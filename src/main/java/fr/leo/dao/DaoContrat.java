package fr.leo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import fr.leo.exceptions.ExceptionChampObligatoire;
import fr.leo.exceptions.ExceptionDao;
import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.metier.Client;
import fr.leo.metier.Contrat;
import fr.leo.utilitaire.ListeContrats;
import fr.leo.utilitaire.ListeSocietes;
import fr.leo.utilitaire.Utilitaire;

/**
 * Classe gestion des Contrats
 * @author Fanny
 */
public class DaoContrat {
	
	// ATTRIBUTS D INSTANCE
	ListeContrats listeContrats;
	ConnexionManager connexionManager;
	
	// CONSTRUCTEURS
	public DaoContrat() {
		this.connexionManager = ConnexionManager.getInstance();
		System.out.println("constructeur DAOContrat "  + connexionManager);
	}	
	/**
	 * @param listeSocietes ListeSocietes
	 * @see ListeSocietes
	 */
	public DaoContrat(ListeContrats listeContrats) {
		this.listeContrats = listeContrats;
		this.connexionManager = ConnexionManager.getInstance();
		System.out.println("constructeur DAOContrat "  + connexionManager);
	}

	
	// METHODE CREATE_CONTRAT() :
	/**
	 * Creer une nouvelle ligne dans la Bdd 'reverso'
	 * Retourne l objet Contrat agrementer d un nouvel identifiant ( creer en Bdd)  si il 
	 *  a ete inserer dans la Bdd, sinon retourne null.
	 * Met a jour la liste des contrats passe en paramatre lors de l'instanciation de la classe.
	 * Met a jour le champs 'idContrat' du contrat passe en parametre, avec l id genere en Bdd.
	 * @param contrat Contrat
	 * @return Contrat
	 * @throws SQLException
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee 
	 */
	public Contrat createContrat(Contrat contrat) 
			throws SQLException, ExceptionDao, ExceptionPersonnalisee {			
		
		if (contrat == null) {
			throw new ExceptionDao("Dao Exception. Methode createContrat(contrat)\n"
									+ "La valeur de l'objet 'contrat' est null.");
		}
		
		// EXTRACTION DE LA VALEURS DE CHACUN DES ATTRIBUTS
		Integer idContrat = contrat.getIdContrat();
		String libelle = contrat.getLibelleContrat();
		Double montant = contrat.getMontantContrat();	
		String dateDebut = contrat.getDateDebutContrat();
		String dateFin = contrat.getDateFinContrat();
	
		// FORMATAGE DES CHAMPS 'DATE', AU FORMAT REQUIS EN BDD
		String dateDebutFormatteeBdd = null;
		int jour = Integer.parseInt(dateDebut.substring(0, 2));
		int mois = Integer.parseInt(dateDebut.substring(3, 5));
		int annee = Integer.parseInt(dateDebut.substring(6, 10)); 
		LocalDate date = LocalDate.of(annee, mois, jour);		
		dateDebutFormatteeBdd =  Utilitaire.FOMATTER_DATE_BDD.format(date);
		
		String dateFinFormatteeBdd = null;
		jour = Integer.parseInt(dateFin.substring(0, 2));
		mois = Integer.parseInt(dateFin.substring(3, 5));
		annee = Integer.parseInt(dateFin.substring(6, 10)); 
		date = LocalDate.of(annee, mois, jour);		
		dateFinFormatteeBdd =  Utilitaire.FOMATTER_DATE_BDD.format(date);	
		
		// VERIFICATION SI CONTRAT EST DEJA PRESENTE EN BDD
		// SI OUI ON ARRETE LE PROCESS
		try {
			if ( (idContrat!= null) && find(idContrat) != null ) {
				return null;
			}
		} catch ( ExceptionPersonnalisee | ExceptionChampObligatoire e2) {
			throw new ExceptionDao("Dao Exception. Methode createSociete.\n" + e2.getMessage());
		}
		
		// SINON ON CREER LE CONTRAT EN BDD
		// CREATION DE LA LIGNE 'CONTRAT' EN BDD 
		Connection connection = connexionManager.getConnection();
		System.out.println("contrat connexion" + connection);
		connection.setAutoCommit(false);
		
		Statement stmtInsert = null;
		ResultSet resulSetAdresse = null;
		ResultSet resulSetContrat = null;
		try {
			stmtInsert = connection.createStatement();
			
			String strRequete = null ;
			// INSERTION DES DONNEES RELATIVES AU CONTRAT, DANS LA TABLE 'CONTRAT'
			strRequete = 	"INSERT INTO contrat VALUES " 
							+ "(" + null + ",\'" + libelle + "\'," + montant
							+ ",\'" + dateDebutFormatteeBdd + "\',\'" 
							+ dateFinFormatteeBdd + "\',"  + null +  ");";	
	
			stmtInsert.execute(strRequete, Statement.RETURN_GENERATED_KEYS);
			resulSetContrat = stmtInsert.getGeneratedKeys();	
			
			if (resulSetContrat.next()) {	
				
				// ON MET A JOUR L'ID_CONTRAT DANS L'OBJET 'CONTRAT' PASSER EN PARAMETRE
				Integer idContratBdd = resulSetContrat.getInt(1);
				contrat.setIdContrat(idContratBdd);
				
				// INSERTION DU NOUVEAU CONTRAT DANS LA LISTE DES CONTRATS
				listeContrats.ajouterContrat(contrat);
		        connection.setAutoCommit(true);
		        
				return contrat;
			}else{
				throw new ExceptionDao("Dao Exception. Methode createContrat.\n"
										+ "Echec creation de la Societe " + libelle +"." );
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}finally {
			try {	
		    	if (resulSetAdresse  != null) { resulSetAdresse.close();}
		    	if (resulSetContrat  != null) { resulSetContrat.close();}
		        if (stmtInsert != null) { stmtInsert.close();}
		        //if (connection != null) { connection.close();}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	// METHODE ASSOCIER_CONTRAT_CLIENT() :
	public Boolean associerContratClient(Client client , Contrat contrat) 
			throws ExceptionDao, ExceptionPersonnalisee, ExceptionChampObligatoire {
		
		Integer idClient = client.getIdSociete();
		Integer idContrat = contrat.getIdContrat();
		
		if ( ( idClient == null )  || idContrat == null) {
			throw new ExceptionDao(	"DaoException. Methode associerContratClient().\n"
									+ "IdContrat ou IdClient valeur egale à 'Null'" );
		}
		if( ( (idClient != null) 
				&&  ( new DaoSociete(new ListeSocietes())).find(idClient) == null) ){
			throw new ExceptionDao(	"DaoException. Methode associerContratClient().\n"
									+ "Le client " + client.getRaisonSociale() + " est inexistant en Bdd." );
		}	
		if( ((idContrat != null) &&   ( this.find(idContrat) == null)) ){
			throw new ExceptionDao(	"DaoException. Methode associerContratClient().\n"
									+ "Le contrat " + contrat.getIdContrat() + " est inexistant en Bdd." );
		}
		
		// SI LE CLIENT ET LE CONTRAT EXISTENT EN BDD, ON LES ASSOCIE		
		Connection connection = connexionManager.getConnection();
		System.out.println("contrat connexion" + connection);
		
		PreparedStatement updateContrat = null;
		String updateString ;
		try {
			connection.setAutoCommit(false);

			updateString = 	"update contrat set "
							+ "contrat.fk_societe = ? "  
							+ " where id_contrat = ? " +  ";";   
			updateContrat = connection.prepareStatement(updateString);   
			updateContrat.setLong(1, idClient); 
			updateContrat.setLong(2, idContrat); 
			updateContrat.executeUpdate();
			
	    	connection.commit(); 
	    	//if (connection != null) { connection.close();}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	// METHODE FIND(CONTRAT) :
	/**
	 * Recherche l existence d un Contrat dans la Bdd 'reverso'
	 *  et retourne le Contrat si il est trouvé sinon retourne 'null'
	 * @param societe
	 * @return Contrat
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws ExceptionChampObligatoire
	 */
	public Contrat find( int idContrat ) 
			throws ExceptionDao, ExceptionPersonnalisee, ExceptionChampObligatoire   {	
		
		if (idContrat <= 0 ) {
			throw new ExceptionDao("Dao Exception. Methode find(idContrat)\n"
									+ "IdContrat valeur negative ou egale à 0");
		}
		
		Connection connection = connexionManager.getConnection();
		System.out.println("contrat connexion" + connection);
		Statement stmt = null;
		ResultSet rs  = null;

		try {
		        // CREATION ET EXECUTION DE LA QUERY DE SELECTION DU CONTRAT
				String query = null;
		        query = "Select * from contrat "
						+ "where id_contrat = "
						+  "'" + idContrat +"'";
		        
				stmt = connection.createStatement();
		        rs = stmt.executeQuery(query);
		        if (rs.next()) {	
		        	
		        	// RECUPERATION DES VALEURS PROVENANT DE LA BDD.
		        	Integer idContratTemp = rs.getInt("id_contrat");
		            String libelle  = rs.getString("libelle_contrat");
		            Double montant  = rs.getDouble("montant_contrat");
		            
		            String dateDebut  = rs.getString("date_debut_contrat");
		            int annee = Integer.parseInt(dateDebut.substring(0, 4));
		            int mois = Integer.parseInt(dateDebut.substring(5, 7));
		            int jour = Integer.parseInt(dateDebut.substring(8, 10)); 
		            LocalDate localDateDebut = LocalDate.of(annee, mois, jour);		            
		            // String dateDebutFormattee =  Utilitaire.FOMATTER_DATE.format(localDateDebut);
		            
		            String dateFin  = rs.getString("date_fin_contrat");
		            annee = Integer.parseInt(dateFin.substring(0, 4));
		            mois = Integer.parseInt(dateFin.substring(5, 7));
		            jour = Integer.parseInt(dateFin.substring(8, 10)); 
		            LocalDate localDateFin = LocalDate.of(annee, mois, jour);
					// String dateFinFormattee =  Utilitaire.FOMATTER_DATE.format(localDateFin);
		            
		            Integer fkSociete = rs.getInt("fk_societe");
		            
		            //INSTANCIATION DU NOUVEAU CONTRAT
		            Contrat contrat = new Contrat(libelle, montant, localDateDebut, localDateFin);
		            contrat.setIdSociete(fkSociete);
		            contrat.setIdContrat(idContratTemp);
		            
		            return contrat;
				}	
	    } catch (SQLException e ) {
			e.printStackTrace();
	    } finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    		//if (connection != null) { connection.close();}
	    	} catch (SQLException e) {
				e.printStackTrace();
	    	}
	    }
	   return null;
	}
	
	
	

	// METHODE FIND_ALL() :
	/**
	 * Recherche la liste des societes presents dans la Bdd 'Reverso'.
	 * Retourne une ArrayList<Societe>.
	 * Retourne une liste vide si la table est vide ou si un pbe a été rencontré. 
	 * @return
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws ExceptionChampObligatoire
	 * @throws SQLException
	 */
	public ArrayList<Contrat> findAll() throws SQLException, ExceptionDao, ExceptionPersonnalisee {

		ArrayList<Contrat> listContrats = new ArrayList<>();
		
		Connection connection = connexionManager.getConnection();
		System.out.println("contrat connexion" + connection);
		Statement stmt = null;
		ResultSet rs  = null;
		
		try {
				// CREATION ET EXECUTION DE LA QUERY DE SELECTION DES CONTRATS
				String query = 	"Select * from contrat";	
				stmt = connection.createStatement();
		        rs = stmt.executeQuery(query);
		        
		        // CREATION DES OBJETS CONTRATS ET ENREGISTREMENT DANS UNE ARRAY_LIST
		        while (rs.next()) {		            
		        	// RECUPERATION DES VALEURS PROVENANT DE LA BDD.
		        	Integer idContrat = rs.getInt("id_contrat");
		            String libelle  = rs.getString("libelle_contrat");
		            Double montant  = rs.getDouble("montant_contrat");
		            
		            String dateDebut  = rs.getString("date_debut_contrat");
		            int annee = Integer.parseInt(dateDebut.substring(0, 4));
		            int mois = Integer.parseInt(dateDebut.substring(5, 7));
		            int jour = Integer.parseInt(dateDebut.substring(8, 10)); 
		            LocalDate localDateDebut = LocalDate.of(annee, mois, jour);		            
		            // String dateDebutFormattee =  Utilitaire.FOMATTER_DATE.format(localDateDebut);
		            
		            String dateFin  = rs.getString("date_fin_contrat");
		            annee = Integer.parseInt(dateFin.substring(0, 4));
		            mois = Integer.parseInt(dateFin.substring(5, 7));
		            jour = Integer.parseInt(dateFin.substring(8, 10)); 
		            LocalDate localDateFin = LocalDate.of(annee, mois, jour);	
					//String dateFinFormattee =  Utilitaire.FOMATTER_DATE.format(localDateFin);
		            
		            Integer fkSociete = rs.getInt("fk_societe");
		            
		            //INSTANCIATION DU NOUVEAU CONTRAT
		            Contrat contrat = new Contrat(libelle, montant, localDateDebut, localDateFin);
		            contrat.setIdSociete(fkSociete);
		            contrat.setIdContrat(idContrat);
		            listContrats.add(contrat);
		        }  
	    } catch (SQLException e ) {
	    	e.printStackTrace();
	    	throw new ExceptionDao(	"ExceptionDaoContrat. Methode findAll().\n"
	    							+ e.getMessage());
	    } finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    		//if (connection != null) { connection.close();}
	    	} catch (SQLException e) {
	    		e.printStackTrace();
	    		throw new ExceptionDao(	"ExceptionDao. Methode findAll().\n"
						+ e.getMessage());
	    	}
	    }
	   return listContrats;
	}	
	
	
	
	
	// METHODE FIND_ALL(ID_CLIENT) :
	/**
	 * Recherche la liste des contrats presents dans la Bdd 'Reverso' pour un Client donner.
	 * Retourne une ArrayList<Societe>.
	 * Retourne une liste vide si la table est vide ou si un pbe a été rencontré. 
	 * @return ArrayList<Contrat>
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws SQLException
	 */
	public ArrayList<Contrat> findAll(Integer IdClient) throws SQLException, ExceptionDao, ExceptionPersonnalisee {

		ArrayList<Contrat> listContrats = new ArrayList<>();
		System.out.println( "findAll contrat" +  connexionManager);
		Connection connection = connexionManager.getConnection();		
		System.out.println("contrat connexion" + connection);
		Statement stmt = null;
		ResultSet rs  = null;
		
		try {
				// CREATION ET EXECUTION DE LA QUERY DE SELECTION DE TOUS LES CONTRATS
				String query = 	"Select * from contrat where fk_societe = " + IdClient;	
				stmt = connection.createStatement();
		        rs = stmt.executeQuery(query);
		        
		        // CREATION DES OBJETS 'CONTRAT' 
		        //    ET ENREGISTREMENT DES OBJETS CREER DANS UNE ARRAY_LIST
		        while (rs.next()) {		            
		        	// RECUPERATION DES VALEURS PROVENANT DE LA BDD.
		        	Integer idContrat = rs.getInt("id_contrat");
		            String libelle  = rs.getString("libelle_contrat");
		            Double montant  = rs.getDouble("montant_contrat");
		            
		            String dateDebut  = rs.getString("date_debut_contrat");
		            int annee = Integer.parseInt(dateDebut.substring(0, 4));
		            int mois = Integer.parseInt(dateDebut.substring(5, 7));
		            int jour = Integer.parseInt(dateDebut.substring(8, 10)); 
		            LocalDate localDateDebut = LocalDate.of(annee, mois, jour);		            
					//String dateDebutFormattee =  Utilitaire.FOMATTER_DATE.format(localDateDebut);
		            
		            String dateFin  = rs.getString("date_fin_contrat");
		            annee = Integer.parseInt(dateFin.substring(0, 4));
		            mois = Integer.parseInt(dateFin.substring(5, 7));
		            jour = Integer.parseInt(dateFin.substring(8, 10)); 
		            LocalDate localDateFin = LocalDate.of(annee, mois, jour);	
					// String dateFinFormattee =  Utilitaire.FOMATTER_DATE.format(localDateFin);
		            
		            Integer fkSociete = rs.getInt("fk_societe");
		            
		            //INSTANCIATION DU NOUVEAU CONTRAT
		            Contrat contrat = new Contrat(libelle, montant, localDateDebut, localDateFin);
		            contrat.setIdSociete(fkSociete);
		            contrat.setIdContrat(idContrat);
		            
		            // AJOUT DU CONTRAT DANS L'ARRAY_LIST<CONTRAT>
		            listContrats.add(contrat);
		        }  
	    } catch (SQLException e ) {
	    	e.printStackTrace();
	    	throw new ExceptionDao(	"ExceptionDaoContrat. Methode findAll().\n"
	    							+ e.getMessage());
	    } finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    		//if (connection != null) { connection.close();}
	    	} catch (SQLException e) {
	    		e.printStackTrace();
	    		throw new ExceptionDao(	"ExceptionDao. Methode findAll().\n"
						+ e.getMessage());
	    	}
	    }
	   return listContrats;
	}
	
	
	
	// METHODE DELETE(ID_CONTRAT) :
	/**
	 * Methode qui supprime une Contrat dans la Bdd 'reverso'.
	 * Retourne :	'false' si la suppression a echouer
	 * 				'true' si la suppression a reussi  
	 * @param  idContrat Integer
	 * @return Boolean
	 * @throws ExceptionDao
	 * @throws ExceptionChampObligatoire
	 */
	public Boolean delete(Integer idContrat) throws ExceptionDao,  ExceptionChampObligatoire  {		

		// SI L ID_CONTRAT EST NULL OU NEGATIF , ON RENVOI UNE EXCEPTION
		if (idContrat == null || idContrat < 0 ) {
			throw new ExceptionDao("Dao Exception.\nMethode delete.\n"
									+ "Impossible de supprimer un contrat "
									+ "avec un idContrat avec une valeur 'Null'"
									+ " ( ou un idContrat avec une valeur negative.)" );
		}
		
		// VERIFIT SI LE CONTRAT EXISTE EN BDD.
		//  SI IL EXISTE PAS, ON RETOURNE 'FALSE'
		try {
			if ( ( idContrat != null && idContrat > 0   ) && (find(idContrat) == null) ) {
				return false;				
			}
		} catch ( ExceptionPersonnalisee e2) {
			e2.printStackTrace();
			throw new ExceptionDao("Dao Exception. Methode save().\n" + e2.getMessage());
		}
		
		// CONNECTION BDD
		Connection connection = connexionManager.getConnection();
		System.out.println("contrat connexion" + connection);
		  
		// SUPPRESSION DU CONTRAT DANS LES TABLE 'CONTRAT'
		String deleteStringContrat = "delete from contrat"
									 + " where id_contrat = '" + idContrat +"'";	
		try {
			connection.setAutoCommit(false);
			PreparedStatement deleteContrat = connection.prepareStatement(deleteStringContrat); 
			deleteContrat.executeUpdate();
			connection.commit();
			// SUPPRESSION DU CONTRAT DANS LA LISTE_DES_CONTRATS
			listeContrats.supprimerContrat(listeContrats.chercherContratParIdentifant(idContrat));			
			return true;
		} catch (SQLException | ExceptionPersonnalisee e1) {
			throw new ExceptionDao("Dao Exception. Erreur SQL. La suppression du contrat " 
									+  idContrat + " a echouée.\n"
									+ e1.getMessage() );
		}	
	}
	
	
	
	// METHODE SAVE(CONTRAT) :
	/**
	 * Methode qui prend un objet de type Contrat en parametre,
	 *  et l'enregistre dans la Bdd. 
	 * Si le contrat existe deja en Bdd, met simplement à jour des attributs.
	 * Si la contrat existe pas encore dans la Bdd, alors rajoute une ligne 
	 * 	dans la Bdd pour ce Contrat, dans la table 'Contrat'.
	 * Retourne l objet Contrat si celui ci a ete enregistrer correctement en Bdd.
	 * Retourne null si un pbe a été rencontré ou si le Contrat n a pas de IdContrat.
	 * @param contrat Contrat
	 * @return Contrat
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws SQLException
	 */
	public Contrat save(Contrat contrat) throws ExceptionDao, ExceptionPersonnalisee, SQLException   {
		
		Connection connection = connexionManager.getConnection();	
		System.out.println("contrat connexion" + connection);
		
		// SI L OBJET CONTRAT VAUT 'NULL', ON RENVOIT UNE EXCEPTION
		if (contrat == null) {
			throw new ExceptionDao("Dao Exception. Methode DaoContrat.save.\n"
									+ "La Contrat ne peut pas avoir une valeur egale à Null." );
		}
		
		// EXTRACTION DE LA VALEURS DE CHACUN DES ATTRIBUTS
				Integer idContrat = contrat.getIdContrat();
				String libelle = contrat.getLibelleContrat();
				Double montant = contrat.getMontantContrat();	
				String dateDebut = contrat.getDateDebutContrat();
				String dateFin = contrat.getDateFinContrat();
				Integer fk_societe = contrat.getIdSociete();
			
				// FORMATAGE DES CHAMPS 'DATE', AU FORMAT REQUIS EN BDD
				String dateDebutFormatteeBdd = null;
				int jour = Integer.parseInt(dateDebut.substring(0, 2));
				int mois = Integer.parseInt(dateDebut.substring(3, 5));
				int annee = Integer.parseInt(dateDebut.substring(6, 10)); 
				LocalDate date = LocalDate.of(annee, mois, jour);		
				dateDebutFormatteeBdd =  Utilitaire.FOMATTER_DATE_BDD.format(date);
				
				String dateFinFormatteeBdd = null;
				jour = Integer.parseInt(dateFin.substring(0, 2));
				mois = Integer.parseInt(dateFin.substring(3, 5));
				annee = Integer.parseInt(dateFin.substring(6, 10)); 
				date = LocalDate.of(annee, mois, jour);		
				dateFinFormatteeBdd =  Utilitaire.FOMATTER_DATE_BDD.format(date);
				
		// SI LE CONTRAT EXISTE PAS DANS LA BDD, ON LA CREER
		//  (ET ON LA RAJOUTE A LA LISTE DES CONTRATS => fait automatiquement dans la methode createContrat)
		try {
			if (idContrat == null) {
				if (this.createContrat(contrat) != null) {
					 return contrat;
				}
			}			
			if (  ( (idContrat != null)  && (idContrat > 0) )  &&    (find(idContrat) == null) ) {				
				 if (this.createContrat(contrat) != null) {
					 return contrat;
				}else {
					throw new ExceptionDao(	"Exception Dao. DaoContrat.save()\n" 
											+ "Erreur creation Contrat." );
				} 
			}
		} catch (ExceptionChampObligatoire e) {
			e.printStackTrace();
			throw new ExceptionDao(	"Exception Dao. DaoContrat.save()\n" 
					+ "Erreur creation Contrat." 
					+ e.getMessage());
		}
		
		// SI LE CONTRAT EXISTE, ON MODIFIE SES VALEURS DANS LA BDD
		PreparedStatement updateClient = null;
		String updateString ;
		connection.setAutoCommit(false);
        
	    // PREPARATION MISE A JOUR DES CHAMPS DE LA TABLE CONTRAT 
			// SI LE CONTRAT N A PAS DE CLIENT ASSOCIE
 			updateString = 	"update contrat set "
 							+ "contrat.libelle_contrat = ? ,"
							+ "contrat.montant_contrat = ? ,"
							+ "contrat.date_debut_contrat = ? ,"
							+ "contrat.date_fin_contrat = ? "
						    + "where contrat.id_contrat = " + "'" + idContrat + "'";  
    		updateClient = connection.prepareStatement(updateString);   
    		updateClient.setString(1,libelle); 
    		updateClient.setDouble(2,montant); 
    		updateClient.setString(3,dateDebutFormatteeBdd); 
    		updateClient.setString(4,dateFinFormatteeBdd);	
    		// SI LE CONTRAT A UN CLIENT ASSOCIER
            if (fk_societe != 0) {         		
         		updateString = 	"update contrat set "
		        				+ "contrat.libelle_contrat = ? ,"
		        				+ "contrat.montant_contrat = ? ,"
		        				+ "contrat.date_debut_contrat = ? ,"
		        				+ "contrat.date_fin_contrat = ? ,"
		        				+ "contrat.fk_societe = ? "
		        			    + "where contrat.id_contrat = " + "'" + idContrat + "'";
        		updateClient = connection.prepareStatement(updateString);   
        		updateClient.setString(1,libelle); 
        		updateClient.setDouble(2,montant); 
        		updateClient.setString(3,dateDebutFormatteeBdd); 
        		updateClient.setString(4,dateFinFormatteeBdd);
         		updateClient.setInt(5,fk_societe);
			}    		
    		updateClient.executeUpdate();
    	
    	// MISE A JOUR DU CONTRAT EN BDD
    	listeContrats.supprimerContrat(listeContrats.chercherContratParIdentifant(idContrat));    
    	listeContrats.ajouterContrat(contrat);
    	connection.commit();    	
    	//if (connection != null) { connection.close();}        	
        return contrat;
	}
}	
