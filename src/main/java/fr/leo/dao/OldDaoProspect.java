package fr.leo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import fr.leo.enums.EnumDomaine;
import fr.leo.enums.EnumInteret;
import fr.leo.exceptions.ExceptionChampObligatoire;
import fr.leo.exceptions.ExceptionDao;
import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.metier.Adresse;
import fr.leo.metier.Prospect;
import fr.leo.utilitaire.ListeSocietes;
import fr.leo.utilitaire.Utilitaire;


/**
 * Classe DAo de gestion des Prospects 
 * 
 * @author Fanny
 *
 */
public class OldDaoProspect {
	
	// ATTRIBUTS D INSTANCE
	ListeSocietes listeSocietes;
	ConnexionManager connexionManager;
		
	// CONSTRUCTEURS
	public OldDaoProspect() {
		@SuppressWarnings("unused")
		ConnexionManager connexionManager = ConnexionManager.getInstance();
	}	
	public OldDaoProspect(ListeSocietes listeSocietes) {
		this.listeSocietes = listeSocietes;
		@SuppressWarnings("unused")
		ConnexionManager connexionManager = ConnexionManager.getInstance();
	}
	
	// METHODE CREATE_PROSPECT(PROSPECT) :
	/**
	 * Creer une nouvelle ligne dans la Bdd 'reverso'
	 * Retourne true si l objet a ete inserer dans la Bdd, sinon retourne false.
	 * Met a jout la listeDesSociete en ajoutant le Prospect nouvellement créé. 	
	 * @param prospect Prospect
	 * @return Boolean
	 * @throws SQLException
	 * @throws ExceptionDao
	 */
	public Boolean createProspect(	Prospect prospect) 
			throws SQLException, ExceptionDao {	
		
		if (prospect == null) {
			throw new ExceptionDao("Dao Exception. Methode createProspect(prospect)\n"
									+ "La valeur de l'objet 'prospect' est null.");
		}
		
		// EXTRACTION DE LA VALEURS DE CHACUN DES ATTRIBUTS
		String nomRue = prospect.getAdresse().getNomRue();
		String numRue = prospect.getAdresse().getNumRue();
		String codePostal = prospect.getAdresse().getCodePostal();
		String ville = prospect.getAdresse().getVille();
		String raisonSociale = prospect.getRaisonSociale();
		EnumDomaine domaine = prospect.getDomaine();
		String telephone = prospect.getTelephone();
		String email = prospect.getEmail();
		String dateProspectionStr = prospect.getDateProspection();
		EnumInteret interet = prospect.getInteret();

		// VERIFICATION SI LE CLIENT EST DEJA PRESENT EN BDD
		// SI OUI ON ARRETE LE PROCESS
		try {
			if (find(raisonSociale) != null) {
				return false;
			}
		} catch ( ExceptionPersonnalisee | ExceptionChampObligatoire e2) {
			throw new ExceptionDao("Dao Exception. Methode createProspect.\n" + e2.getMessage());
		}
	
		// FORMATAGE DU CHAMPS 'DATE_PROSPECTION, AU FORMAT REQUIS EN BDD
		int jour = Integer.parseInt(dateProspectionStr.substring(0, 2));
		int mois = Integer.parseInt(dateProspectionStr.substring(3, 5));
		int annee = Integer.parseInt(dateProspectionStr.substring(6, 10)); 
		LocalDate dateProspection = LocalDate.of(annee, mois, jour);		
		String dateProspectionFormatteeBdd =  Utilitaire.FOMATTER_DATE_BDD.format(dateProspection);	
		
		// CREATION DE LA LIGNE 'CLIENT' EN BDD : ( ET DE LA LIGNE 'ADRESSE' )
		Connection connection = connexionManager.getConnection();
		connection.setAutoCommit(false);
		
		Statement stmtInsert = null;
		ResultSet resulSetAdresse = null;
		ResultSet resulSetProspect = null;
		try {
			stmtInsert = connection.createStatement();
			
			// INSERTION DES DONNEES RELATIVE A L ADRESSE, DANS LA TABLE 'ADRESSE'
			String strRequete = "INSERT INTO adresse VALUES "
								+ "(" + null + ",\'" 
								+ nomRue + "\',\'" + numRue + "\',\'"
								+ codePostal + "\',\' " + ville + "\'" + ")";
			stmtInsert.execute(strRequete, Statement.RETURN_GENERATED_KEYS);
			resulSetAdresse = stmtInsert.getGeneratedKeys();			
			
			// INSERTION DES DONNEES RELATIVES AU PROSPECT, DANS LA TABLE 'PROSPECT'
			if (resulSetAdresse.next()) {
					Integer fkAdresse = resulSetAdresse.getInt(1);				
	
					strRequete = 	"INSERT INTO prospect VALUES " 
									+ "(" + null + ",\'" + raisonSociale + "\',\'" + domaine
									+ "\',\'" + telephone + "\',\'" + email + "\',\'" + "" + "\',\' "
									+ dateProspectionFormatteeBdd + "\',\'"
									+ interet + "\',\' " + fkAdresse + "\'" + ")";					
					stmtInsert.execute(strRequete, Statement.RETURN_GENERATED_KEYS);
					resulSetProspect = stmtInsert.getGeneratedKeys();		
					
					if (resulSetProspect.next()) {
						 connection.setAutoCommit(true);
				        // INSERTION DU NOUVEAU PROSPECT DANS LA LISTE DES SOCIETES
				        listeSocietes.ajouterSociete(prospect);
						return true;
					}else{
						throw new ExceptionDao("Dao Exception. Methode createProspect.\n"
								+ "Echec creation du Prospect " + raisonSociale +"." );
					}
			} else {
				throw new ExceptionDao(	"Dao Exception. Methode createProspect\n."
										+ "Echec creation Pospect " + raisonSociale  +".\n"
										+ "Les valeurs pour l adresse sont incorrectes");
			}
		} catch (SQLException | ExceptionPersonnalisee e1) {
			e1.printStackTrace();
		}finally {
			try {	
		    	if (resulSetAdresse  != null) { resulSetAdresse.close();}
		    	if (resulSetProspect  != null) { resulSetProspect.close();}
		        if (stmtInsert != null) { stmtInsert.close();}
		        if (connection != null) { connection.close();}
			} catch (SQLException e) {				
				e.printStackTrace();
			}
		}
		return false;
	}


	
	// METHODE FIND(STRING RAISON_SOCIALE) :
	/**
	 * Recherche l existence d un Prospect(raisonSociale) dans la Bdd 'reverso'
	 *   et en retourne une instance.
	 * Retourne null si le Prospect(raisonSociale) n a pas  d existence en Bdd
	 *  ou si un pbe a été rencontré.
	 * @param raisonSociale
	 * @return
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws ExceptionChampObligatoire
	 */
	public Prospect find(String raisonSociale) throws ExceptionDao, 
							ExceptionPersonnalisee, ExceptionChampObligatoire   {
		
		Connection connection = connexionManager.getConnection();		
		Statement stmt = null;
		ResultSet rs  = null;		

		try {
		        // CREATION ET EXECUTION DE LA QUERY DE SELECTION DU PROSPECT
				String query = 	"Select * from prospect"
								+ " inner join adresse on prospect.fk_adresse = adresse.id_adresse	"
								+ "where raison_sociale = "
								+  "'"+ raisonSociale +"'";	
				stmt = connection.createStatement();
		        rs = stmt.executeQuery(query);
		        
		        // RECUPERATION DES VALEURS PROVENANT DE LA BDD,
		        //  ET INSTANCIATION D'UN NOUVEL OBJET 'PROSPECT' ( ET DE SON OBJET 'ADRESSE' ) 
		        while (rs.next()) {
		        	// RECUPERATION DES VALEURS PROVENANT DE LA BDD.
		            String nomRueProspect  = rs.getString("nom_rue");
		            String numeroRueProspect  = rs.getString("num_rue");
		            String codePostalProspect  = rs.getString("code_postal");
		            String villeProspect  = rs.getString("ville");
		            
		            Integer idSociete = rs.getInt("id_societe");
		            String raisonSocialeProspect  = rs.getString("raison_sociale");
		            String domaineClientProspect  =  rs.getString("domaine");
		            EnumDomaine  domaineProspect = domaineClientProspect == "PUBLIC"
		            								? EnumDomaine.PUBLIC : EnumDomaine.PRIVE; 
		            String telephoneProspect  = rs.getString("telephone");
		            String emailProspect  = rs.getString("email");
		            String commentairesProspect  = rs.getString("commentaires");
		            String dateProspectionProspect  = rs.getString("date_prospection");
		            int annee = Integer.parseInt(dateProspectionProspect.substring(0, 4));
		            int mois = Integer.parseInt(dateProspectionProspect.substring(5, 7));
		            int jour = Integer.parseInt(dateProspectionProspect.substring(8, 10)); 
		            LocalDate lc = LocalDate.of(annee, mois, jour);		            
		            String dateProspectionProspectFormattee =  Utilitaire.FOMATTER_DATE.format(lc);
		            String interetProspectString  = rs.getString("interet");
		            EnumInteret  interetProspect = interetProspectString == "OUI"
							? EnumInteret.OUI : EnumInteret.NON ;		            
		            
		            //INSTANCIATION D'UN NOUVEL OBJET 'PROSPECT' (ET DE SON OBJET 'ADRESSE'). 
		            Adresse adresseProspect = new Adresse( numeroRueProspect, nomRueProspect, 
		            									codePostalProspect, villeProspect);
		            Prospect prospect = new Prospect(	raisonSocialeProspect, domaineProspect,
														adresseProspect, telephoneProspect, emailProspect,
														dateProspectionProspectFormattee, interetProspect);
		            prospect.setIdSociete(idSociete);
		            
		            // AJOUT COMMENTAIRE
		            if (commentairesProspect != null) {
						prospect.setCommentaires(commentairesProspect);
					}
		            return prospect;
		        }
	    } catch (SQLException e ) {
			e.printStackTrace();
	    } finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    		if (connection != null) { connection.close();}
	    	} catch (SQLException e) {
				e.printStackTrace();
	    	}
	    }
	   return null;
	}
	
	// METHODE FIND(PROSPECT) :
	/**
	 * Recherche l existence d un Prospect dans la Bdd 'reverso'
	 *  et retourne  'true' si le Prospect est trouvé sinon retourne 'false'
	 * @param prospect
	 * @return Boolean
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws ExceptionChampObligatoire
	 */
	public Boolean find(Prospect prospect) 
			throws ExceptionDao, ExceptionPersonnalisee, ExceptionChampObligatoire   {		
		
		if (prospect == null) {
			throw new ExceptionDao("Dao Exception. Methode find(prospect)\n"
					+ "L objet prospect est null.");
		}
		
		Connection connection = connexionManager.getConnection();			
		Statement stmt = null;
		ResultSet rs  = null;
		
		try {
		        // CREATION ET EXECUTION DE LA QUERY DE SELECTION DU PROSPECT
			String query = 	"Select * from prospect"
							+ " inner join adresse on prospect.fk_adresse = adresse.id_adresse	"
							+ "where raison_sociale = "
							+  "'"+ prospect.getRaisonSociale() +"'";	
				stmt = connection.createStatement();
		        rs = stmt.executeQuery(query);
		        if (rs.next()) {
					return true;
				}	
	    } catch (SQLException e ) {
			e.printStackTrace();
	    } finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    		if (connection != null) { connection.close();}
	    	} catch (SQLException e) {
				e.printStackTrace();
	    	}
	    }
	   return false;
	}

	// METHODE FIND_ALL() :
	 /**
 	 * Recherche la liste des prospects presents dans la Bdd 'Reverso'.
	 * Retourne une ArrayList<Client>.
	 * Retourne une liste vide si la table est vide ou si un pbe a été rencontré. 
	 * @return ArrayList<Prospect>
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws ExceptionChampObligatoire
	 * @throws SQLException
	 */
	public ArrayList<Prospect> findAll() throws ExceptionDao, ExceptionPersonnalisee,
								ExceptionChampObligatoire, SQLException {
		
		
		ArrayList<Prospect> listProspects = new ArrayList<>();
		Connection connection = connexionManager.getConnection();	
		
		Statement stmt = null;
		ResultSet rs  = null;
	
		try {
				// CREATION ET EXECUTION DE LA QUERY DE SELECTION DES PROSPECTS
				String query = 	"Select * from prospect"
								+ " inner join adresse on prospect.fk_adresse = adresse.id_adresse";
				stmt = connection.createStatement();
		        rs = stmt.executeQuery(query);
		        
		        // CREATION DES OBJETS PROSPETS ET ENREGISTREMENT DANS UNE ARRAY_LIST
		        while (rs.next()) {
		            String raisonSocialeClient  = rs.getString("raison_sociale");
		            Prospect prospect = this.find(raisonSocialeClient);
		            listProspects.add(prospect);
		        }
	    } catch (SQLException e ) {
			e.printStackTrace();
	    } finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    		if (connection != null) { connection.close();}
	    	} catch (SQLException e) {
				e.printStackTrace();
	    	}
	    }
	   return listProspects;
	}	
	
	
	// METHODE DELETE(PROSPECT) :
	/**
 	 * Methode qui supprime un Prospect dans la Bdd 'reverso'.
	 * Retourne :	'false' si la suppression a echouer
	 * 				'true' si la suppression a reussi  
	 * @param prospect
	 * @return
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws ExceptionChampObligatoire
	 */
	public Boolean delete(Prospect prospect ) throws ExceptionDao, ExceptionPersonnalisee, ExceptionChampObligatoire  {
		
		// SI LE PROSPECT EST NULL, ON RENVOI UNE EXCEPTION
		if (prospect == null) {
			throw new ExceptionDao("Dao Exception.\nMethode delete.\n"
									+ "Impossible de supprimer un prospect avec la valeur 'Null'");
		}
		

		// VERIFIT SI LE PROSPECT EXISTE EN BDD.
		//  SI IL EXISTE PAS, ON RETOURNE 'FALSE'
		String raisonSociale = prospect.getRaisonSociale();
		try {
			if (find(raisonSociale) == null) {
				return false;				
			}
		} catch ( ExceptionPersonnalisee e2) {
			e2.printStackTrace();
			throw new ExceptionDao("Dao Exception. Methode save().\n" + e2.getMessage());
		}
		
		// CONNECTION BDD
		Connection connection = connexionManager.getConnection();
		
		// RECHERCHE DE LA CLE ETRANGERE => 'TABLE_ADRESSE', POUR CE PROSPECT
		String fkAdresse = null;
		String query = 	"Select * from prospect where raison_sociale = "
						+  "'" + raisonSociale +"'" ;	
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				fkAdresse  = rs.getString("fk_adresse");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		  
		// SUPPRESSION DU PROSPECT ET DE SON 'ADRESSE' 
		//  DANS LES TABLES 'PROSPECT' ET 'ADRESSE'
		String deleteStringProspect = "delete from prospect where raison_sociale = "
				+  "'"+ raisonSociale +"'";			
		String deleteStringAdresse = "delete from adresse where  adresse.id_adresse = "
				+ fkAdresse ;	
		try {
			connection.setAutoCommit(false);
			PreparedStatement deleteProspect = connection.prepareStatement(deleteStringProspect);  
			PreparedStatement deleteAdresse = connection.prepareStatement(deleteStringAdresse); 
			int nbr  = deleteProspect.executeUpdate();	
			nbr = nbr + deleteAdresse.executeUpdate();	
			connection.commit();
			// SUPPRESSION DE LA SOCIETE DANS LA LISTE_DES-SOCIETES
			listeSocietes.supprimerSociete(prospect);
			
			return true;
		} catch (SQLException e1) {
			throw new ExceptionDao(	"Dao Exception. Erreur SQL. La suppression du client " 
									+  raisonSociale + " a echouée.\n"
									+ e1.getMessage() );
		}	
	}
	
	// METHODE SAVE(CLIENT) :
	/**
	 * Methode qui prend un objet de type Prospect en parametre,
	 *  et l'enregistre dans la Bdd. (Tables Prospect et Adresse).
	 * Si le prospect existe deja en Bdd, met simplement à jour des attributs.
	 * Si le prospect existe pas encore dans la Bdd, alors rajoute une ligne 
	 * 	dans la Bdd pour ce prospect, dans les tables 'Prospect' et 'Adresse'.
	 * Retourne l objet Prospect si celui ci a ete enregistrer correctement en Bdd.
	 * Retourne null si un pbe a été rencontré.
	 * @param prospect
	 * @return Prospect
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws SQLException
	 */
	public Prospect save2(Prospect prospect) throws ExceptionDao, ExceptionPersonnalisee, SQLException   {
		
		Connection connection = connexionManager.getConnection();	
		
		// SI L OBJET PROSPECT VAUT 'NULL', ON RENVOIT UNE EXCEPTION
		if (prospect == null) {
			throw new ExceptionDao("Dao Exception. Methode DaoProspect.save2.\n"
									+ "Le prospect ne peut pas avoir une valeur egale à Null." );
		}
		
		// EXTRACTION DES VALEURS DE L OBJET 'CLIENT' PASSE EN PARAMETRE
		String nomRue = prospect.getAdresse().getNomRue();
		String numRue = prospect.getAdresse().getNumRue();
		String codePostal = prospect.getAdresse().getCodePostal();
		String ville = prospect.getAdresse().getVille();
		String raisonSociale = prospect.getRaisonSociale();
		String commentaires = prospect.getCommentaires();
		String domaine = prospect.getDomaine().toString() ;
		String telephone = prospect.getTelephone();
		String email = prospect.getEmail();
		String dateProspection = prospect.getDateProspection();
        int jour = Integer.parseInt(dateProspection.substring(0, 2));
        int mois = Integer.parseInt(dateProspection.substring(3, 5));
        int annee = Integer.parseInt(dateProspection.substring(6, 10));
        dateProspection = annee +"-" + mois + "-" + jour;
		String interet = prospect.getInteret().toString();
		
		// SI LE PROSPECT N EXISTE PAS DANS LA BDD, ON LE CREER
		//  ET ON LE RAJOUTE A LA LISTE DES SOCIETES
		try {
			if ( find(raisonSociale) == null ) {			
				 if (this.createProspect(prospect)) {
					 return prospect;
				}else {
					throw new ExceptionDao(	"Exception Dao. DaoClient.save2()\n" 
											+ "Erreur creation Cleint." );
				} 
			}
		} catch (ExceptionChampObligatoire e) {
			e.printStackTrace();
			throw new ExceptionDao(	"Exception Dao. DaoClient.save2()\n" 
					+ "Erreur creation Client." 
					+ e.getMessage());
		}
		
		// SI LE PROSPECT EXISTE, ON MODIFIE SES VALEURS DANS LA BDD
		PreparedStatement updateClient = null;
		String updateString ;
		connection.setAutoCommit(false);
		
	    // PREPARATION MISE A JOUR DES CHAMPS DE LA TABLE ADRESSE
		updateString = 	"update adresse, prospect set "
						+ "adresse.nom_rue = ? ,"
						+ "adresse.num_rue = ? ,"
						+ "adresse.code_postal = ? ,"
						+ "adresse.ville = ? "
						+ "where adresse.id_adresse = prospect.fk_adresse and  "
						+ "prospect.raison_sociale = " + "'" + raisonSociale + "'";   
		updateClient = connection.prepareStatement(updateString);   
		updateClient.setString(1,nomRue); 
		updateClient.setString(2,numRue); 
		updateClient.setString(3,codePostal); 
		updateClient.setString(4,ville);
    	updateClient.executeUpdate();
        
	    // PREPARATION MISE A JOUR DES CHAMPS DE LA TABLE CLIENT    	
		updateString = 	"update prospect set "
						+ "prospect.raison_sociale = ? ,"
						+ "prospect.domaine = ? ,"
						+ "prospect.telephone = ? ,"
						+ "prospect.email = ? ,"
						+ "prospect.commentaires = ? ,"
						+ "prospect.date_prospection = ? ,"
						+ "prospect.interet = ? "
						+ "where prospect.raison_sociale = " + "'" + raisonSociale + "'";	  
		updateClient = connection.prepareStatement(updateString);   
		updateClient.setString(1,raisonSociale); 
		updateClient.setString(2,domaine); 
		updateClient.setString(3,telephone); 
		updateClient.setString(4,email); 
		updateClient.setString(5,commentaires);
		updateClient.setString(6,dateProspection);
		updateClient.setString(7,interet);
    	updateClient.executeUpdate();
    	
    	// MISE A JOUR DU PROSPECT ET DE SON ADRESSE EN BDD
    	listeSocietes.supprimerSociete(listeSocietes.chercherSocieteParRaisonSociale(raisonSociale));
    	listeSocietes.ajouterSociete(prospect);
    	connection.commit();    	
    	if (connection != null) { connection.close();}        	
        return prospect;
	}

	
	
//					// *************** METHODES UTILITAIRES ***************** //
	
//	// METHODE QUI AFFICHE LES MESSAGES A L'UTILISATEUR
//	private void afficheMsgUtilisateur(String messageUtilisateur, String tpyeErreur , int typeIcone) {
//		JOptionPane.showMessageDialog(null,
//				messageUtilisateur,
//				tpyeErreur, 
//				typeIcone);
//		messageUtilisateur = "";
//	}
	
//	// METHODE QUI VERIFIT SI UN PROSPECT EXISTE DEJA DANS LA BDD
//	// Retourne true si le Prospect est present dans la Bdd 'reverso' sinon retourne false
//	private boolean checkIfExistAlreadyInDatabase(String raisonSociale) 
//					throws ExceptionDao, ExceptionPersonnalisee {
//		
//			String messageErreur;				
//			Prospect prospectAlreadyExist = null;	
//			
//			try {
//				prospectAlreadyExist = this.find(raisonSociale);
//			} catch (ExceptionChampObligatoire e2) {
//				messageErreur = "Dao Erreur. Methode checkIfExistAlreadyInDatabase()\n" 
//								+ "Erreur client " + raisonSociale + ".\n"
//								+ e2.getMessage();
//				afficheMsgUtilisateur(messageErreur, "erreur",  JOptionPane.WARNING_MESSAGE);
//			}
//			if (prospectAlreadyExist == null) {
//				return false;
//			}
//			return true;		
//	}	
}


//// Methode save()
///**
// * Methode de mise a jour d un/des champ(s) d un Prospect particulier en Bdd.
// * Selection du Prospect a partir de sa 'raisonSociale'. 
// * La liste des "champs/valeurs" a modifier, sont passes en parametre
// *  dans une HashMap du type 'HashMap<EnumChampsTablePropect, String>'
// * @param listeChampsValeurs  HashMap<EnumChampsTablePropect, String> 
// * @param raisonSociale
// * @return boolean
// * @throws SQLException
// * @throws ExceptionDao
// */
//public Prospect save(HashMap<EnumChampsTableProspect, String> listeChampsValeurs, String raisonSociale)
//								throws  ExceptionDao, SQLException   {
//	
//	String messageErreur;
//	
//	Connection connection = ConnexionManager.getConnection();
//	
//	// CHECK SI LE PROSPECT EXISTE EN BDD
//	//  SI IL N EXISTE PAS ON LE CREER ( a condition que toutes
//	//  					valeurs obligatoires soient renseignees )
//	try {
//		if (! checkIfExistAlreadyInDatabase(raisonSociale) ) {
//			String num_rue = null;	String nom_rue = null ;String code_postal = null;
//			String ville = null ;String raison_sociale =null; EnumDomaine domaineTempEnumDomaine = null;
//			String telephone = null ;String email=null; String commentaires = null ;
//			LocalDate localDate = null; EnumInteret interetTempEnum = null;
//			String date_prospection = null;
//			
//			// ON RECUPERE TOUTES LES VALEURS PASSEES EN PARAMATRES DANS LA HASH_MAP
//			for (Entry<EnumChampsTableProspect, String> champAMettreAJour : listeChampsValeurs.entrySet()) {
//				try {
//					if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.num_rue)){
//						num_rue = champAMettreAJour.getValue(); 
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.nom_rue)){
//						nom_rue = champAMettreAJour.getValue(); 
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.code_postal)){
//						code_postal = champAMettreAJour.getValue(); 
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.ville)){
//						ville = champAMettreAJour.getValue(); 
//					}
//					if(champAMettreAJour.getKey().equals(raisonSociale)){
//						 raison_sociale = champAMettreAJour.getValue(); 
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.domaine)){
//						String domaineTempString =  champAMettreAJour.getValue();
//						domaineTempEnumDomaine = domaineTempString== "PUBLIC"
//																	? EnumDomaine.PUBLIC : EnumDomaine.PRIVE; 								
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.telephone)){
//						telephone = champAMettreAJour.getValue(); 
//					}				
//					if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.email)){
//						email = champAMettreAJour.getValue(); 		
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.commentaires)){
//						commentaires = champAMettreAJour.getValue(); 	
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.date_prospection)){
//						    date_prospection  = champAMettreAJour.getValue();
////				            int jour = Integer.parseInt(date_prospection.substring(0, 2));
////				            int mois = Integer.parseInt(date_prospection.substring(3, 5));
////				            int annee = Integer.parseInt(date_prospection.substring(6, 10)); 
////				            localDate = LocalDate.of(annee, mois, jour);		            
//				            //dateProspectionProspectFormattee =  Utilitaire.FOMATTER_DATE_BDD.format(lc); 
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.interet)){
//						String interet =  champAMettreAJour.getValue();
//						interetTempEnum = interet == "OUI" ? EnumInteret.OUI : EnumInteret.NON; 
//					}	
//				} catch (NumberFormatException e) {						
//					JOptionPane.showMessageDialog(null,
//							"Echec Mise A Jour de l'objet Prospect " 
//							+ raisonSociale + " en Bdd : \n" + e.getMessage() ,
//							"Erreur", JOptionPane.WARNING_MESSAGE);
//							e.printStackTrace();
//					throw new ExceptionDao("Dao Exception. Methode Save.\n" 
//											+ e.getMessage());					}
//			}
//			// INSTANCIATION DU NOUVEAU PROSPECT
//			Adresse adresseProspect = new Adresse(num_rue, nom_rue, code_postal, ville);
//			Prospect newProspect = new Prospect(raisonSociale, domaineTempEnumDomaine,
//													adresseProspect, telephone, email, 
//													date_prospection, interetTempEnum);
//			if (this.createProspect(newProspect)) {
////			Prospect newProspect = this.createProspect(nom_rue, num_rue, code_postal, 
////					ville, raisonSociale, domaineTempEnumDomaine, telephone, 
////					email, localDate, interetTempEnum);
////			String messageUtilisateur = "Dao Success. Le prospect " + raisonSociale
////										+ " a été créer avec succés dans la Bdd";
////			afficheMsgUtilisateur(messageUtilisateur, "Insertion" , JOptionPane.INFORMATION_MESSAGE);
//			return newProspect;
//			}
//		}
//	}  catch (ExceptionPersonnalisee | ExceptionChampObligatoire e2) {
//		e2.printStackTrace();
//		throw new ExceptionDao("Dao Exception. Methode save().\n" + e2.getMessage());
//	}
//	
//	
//		
//	// SI LE PROSPECT EXISTE PAS EN BDD, ON FAIT LA MISE A JOUR 
//	PreparedStatement updateProspect = null;
//	String updateString ;
//	Boolean existingField;
//	connection.setAutoCommit(false);
//	
//	// INSTANCIATION DU PROSPECT A PARTIR DES DONNEES EN BDD.
//	//  CETTE INSTANCE VA SERVIR A VERIFIER LA CORRECTION DES NOUVELLES VALEURS.	
//	Prospect prospectMisAJour = null;
//	//prospectMisAJour = this.find(raisonSociale);
//	System.out.println("@@@@@@@@@@@@@@@@@@@@@");
//	System.out.println("ici" + listeSocietes );
//	prospectMisAJour =  (Prospect) listeSocietes.chercherSocieteParRaisonSociale(raisonSociale);
//	System.out.println("**********************************");
//	// TEST DE LA CORRECTION DES CHAMPS A METTRE A JOUR	
//	for (Entry<EnumChampsTableProspect, String> champAMettreAJour : listeChampsValeurs.entrySet()) {
//		try {
//			if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.nom_rue)){
//				prospectMisAJour.getAdresse().setNomRue(champAMettreAJour.getValue()); 
//			}
//			
//			if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.num_rue)){
//				prospectMisAJour.getAdresse().setNumRue(champAMettreAJour.getValue()); 
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.code_postal)){
//				prospectMisAJour.getAdresse().setCodePostal(champAMettreAJour.getValue()); 		
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.ville)){
//				prospectMisAJour.getAdresse().setVille(champAMettreAJour.getValue()); 
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.raison_sociale)){
//				prospectMisAJour.setRaisonSociale(champAMettreAJour.getValue());			
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableClient.domaine)){
//				String domaineTempString =  champAMettreAJour.getValue();
//				EnumDomaine domaineTempEnumDomaine = domaineTempString== "PUBLIC"
//															? EnumDomaine.PUBLIC : EnumDomaine.PRIVE; 
//				prospectMisAJour.setDomaine(domaineTempEnumDomaine);			
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.telephone)){
//				prospectMisAJour.setTelephone(champAMettreAJour.getValue());	
//			}				
//			if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.email)){
//					prospectMisAJour.setEmail(champAMettreAJour.getValue());			
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.commentaires)){
//				prospectMisAJour.setCommentaires(champAMettreAJour.getValue());			
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.date_prospection)){
//				prospectMisAJour.setDateProspection(champAMettreAJour.getValue());
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.interet)){
//				String interetTempString =  champAMettreAJour.getValue();
//				EnumInteret domaineTempEnumInteret = interetTempString== "OUI"
//															? EnumInteret.OUI : EnumInteret.NON; 
//				prospectMisAJour.setInteret(domaineTempEnumInteret);			
//			}
//		} catch (ExceptionChampObligatoire | ExceptionPersonnalisee | NumberFormatException e) {
//			JOptionPane.showMessageDialog(null,
//					"Erreur Dao. Echec Mise A Jour de l'objet Prospect " 
//					+ raisonSociale + " en Bdd : \n" + e.getMessage() ,
//					"Erreur", JOptionPane.WARNING_MESSAGE);
//					e.printStackTrace();
//			throw new ExceptionDao("Dao Exception. Methode Save.\n" 
//									+ e.getMessage());	
//		}
//	}
//	
//    // PREPARATION MISE A JOUR DES CHAMPS DE LA TABLE ADRESSE
//    for (Entry<EnumChampsTableProspect, String> champAMettreAJour : listeChampsValeurs.entrySet()) {
//    		existingField = false;
//    		
//			updateString = 	"update adresse, prospect set adresse." + champAMettreAJour.getKey() +  " = ?"
//							+ " where adresse.id_adresse = prospect.fk_adresse and  "
//							+ "prospect.raison_sociale = " + "'" + raisonSociale + "'";
//			updateProspect = connection.prepareStatement(updateString);   
//			
//			try {
//				existingField = preparationChamps(updateProspect,  champAMettreAJour, existingField, EnumChampsTableProspect.num_rue);
//				existingField = preparationChamps(updateProspect,  champAMettreAJour, existingField, EnumChampsTableProspect.nom_rue);
//				existingField = preparationChamps(updateProspect,  champAMettreAJour, existingField, EnumChampsTableProspect.code_postal);
//				existingField = preparationChamps(updateProspect,  champAMettreAJour, existingField, EnumChampsTableProspect.ville);    			
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//        	if (existingField) {	        		
//				updateProspect.executeUpdate();
//			}
//    }        
//    
//    // PREPARATION MISE A JOUR DES CHAMPS DE LA TABLE PROSPECT
//    for (Entry<EnumChampsTableProspect, String> champAMettreAJour : listeChampsValeurs.entrySet()) {
//    		existingField = false;
//    		
//			updateString = 	"update prospect set prospect." + champAMettreAJour.getKey() +  "= ? "
//							+ "where prospect.raison_sociale = " + "'" + raisonSociale + "'";	        			
//			updateProspect = connection.prepareStatement(updateString);
//			
//        	existingField = preparationChamps(updateProspect, champAMettreAJour, existingField, EnumChampsTableProspect.raison_sociale);
//        	try {
//				existingField = preparationChamps(updateProspect, champAMettreAJour, existingField, EnumChampsTableProspect.domaine);
//				existingField = preparationChamps(updateProspect, champAMettreAJour, existingField, EnumChampsTableProspect.telephone);
//				existingField = preparationChamps(updateProspect, champAMettreAJour, existingField, EnumChampsTableProspect.email);
//				existingField = preparationChamps(updateProspect, champAMettreAJour, existingField, EnumChampsTableProspect.commentaires);
//				// Gestion de la date de prospection
//				if(champAMettreAJour.getKey().equals(EnumChampsTableProspect.date_prospection)){
//		            String dateProspectionProspect  = champAMettreAJour.getValue();
//		            int jour = Integer.parseInt(dateProspectionProspect.substring(0, 2));
//		            int mois = Integer.parseInt(dateProspectionProspect.substring(3, 5));
//		            int annee = Integer.parseInt(dateProspectionProspect.substring(6, 10)); 
//		            LocalDate lc = LocalDate.of(annee, mois, jour);		            
//		            String dateProspectionProspectFormattee =  Utilitaire.FOMATTER_DATE_BDD.format(lc); 
//					updateProspect.setString(1, dateProspectionProspectFormattee);
//				existingField = true;
//				}
//				existingField = preparationChamps(updateProspect, champAMettreAJour, existingField, EnumChampsTableProspect.interet);
//				existingField = preparationChamps(updateProspect, champAMettreAJour, existingField, EnumChampsTableProspect.raison_sociale);
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}	        
//        	if (existingField) {
//					updateProspect.executeUpdate();
//			}	        	
//    }
//    	// MISE A JOUR DU PROSPECT ET DE SON ADRESSE EN BDD
//    	connection.commit();  
//        messageErreur = "Le prospect " + raisonSociale + " a été mis à jour avec succés dans la Bdd";
//        afficheMsgUtilisateur(messageErreur, "Mise a jour" , JOptionPane.INFORMATION_MESSAGE);
//    	if (connection != null) { connection.close();}        	
//        return prospectMisAJour;
//}


///**
// * Methode qui prepare la mise a jour d un champs particulier
// * Retourne 'true' si le champs passe en parametre a ete preparer
// * @param updateProspect
// * @param champAMettreAJour
// * @param existingField
// * @param champ
// * @return champAMettreAJour
// * @throws SQLException
// */
//private Boolean preparationChamps(PreparedStatement updateProspect, Entry<EnumChampsTableProspect, String> champAMettreAJour,
//		Boolean existingField , EnumChampsTableProspect champ) throws SQLException {
//				if(champAMettreAJour.getKey().equals(champ)){
//					updateProspect.setString(1, champAMettreAJour.getValue()); 
//					existingField = true;
//				}
//				return existingField;
//}





//// METHODE : CREATION L OBJET 'PROSPECT' (ET DE SON OBJET 'ADRESSE')
//// En cas de success, renvoit l'objet 'Prospect', sinon renvoit 'Null' 
//private Prospect creationObjetAdresseEtObjetProspect(String nomRue, String numRue, String codePostal,
//													String ville, String raisonSociale, EnumDomaine domaine, 
//													String telephone, String email, EnumInteret interet,
//													String dateProspectionFormatteeClasseSociete) 
//													throws ExceptionDao {
//	String messageUtilisateur;
//	Prospect nouveauProspect = null;
//	// INSTANCIATION DE L'OBJET 'PROSPECT' ( ET DE SON OBJET 'ADRESSE' )
//	Adresse adresseProspect;
//	try {
//		adresseProspect = new Adresse(numRue, nomRue, codePostal, ville);
//		nouveauProspect = new Prospect(raisonSociale, domaine, adresseProspect, telephone,
//										email, dateProspectionFormatteeClasseSociete , interet );
//	} catch (ExceptionPersonnalisee | ExceptionChampObligatoire e) {			
//				messageUtilisateur = 	"Dao Erreur.\nEchec creation de l objet 'Prospect'"
//									+ " (ou de l objet 'Adresse' associé)" 
//									+ raisonSociale + " en Bdd.\n" 
//									+ e.getMessage();
//				afficheMsgUtilisateur(messageUtilisateur, "erreur" , JOptionPane.WARNING_MESSAGE);
//				throw new ExceptionDao(	"DAO Exception.\nMethode createProspect.\n"
//										+ "Echec creation objet Prospect\n"
//										+ e.getMessage());
//	}
//	return nouveauProspect;
//}


