package fr.leo.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.logging.log4j.*;

import fr.leo.exceptions.ExceptionDao;
import fr.leo.exceptions.ExceptionPersonnalisee;

/**
 * Classe gerant la connexion vers la base de donnée du projet Reverso
 * 
 * @author Fanny
 */
public class ConnexionManager {
	
	private static Connection connection;
	
	private static final Logger LOGGER = LogManager.getLogger(ConnexionManager.class.getName());

	private ConnexionManager() {
	}
	
	private static ConnexionManager uniqueInstance ;
	
	static{
		Runtime.getRuntime().addShutdownHook(new Thread(){
			public void run() {
				if (connection != null) {
					try {
						System.out.println("connexion fermée .");
						System.out.println(LOGGER);
						LOGGER.info("Database : connexion closed");
						connection.close();
					} catch (Exception e) {
						LOGGER.fatal(e.getMessage());
					}
				}
			}
		});
	}
	
    public static synchronized ConnexionManager getInstance()
    {           
        if (uniqueInstance == null)
        {   uniqueInstance = new ConnexionManager(); 
        System.out.println("iciciciciicc et la" + uniqueInstance);
        }
        return uniqueInstance;
    }
	
	/**
	 * Methode permettant d'obtenir une instance de la classe Connection c'est a
	 * dire une connexion vers la base de donnée du projet Reverso
	 * Renvoit une exception si le fichier de  configuration n'est pas accessible
	 * Renvoit une exception en cas de pbe de connection vers la Bdd
	 * 
	 * @return Connection connection
	 * @throws ExceptionPersonnalisee
	 * @throws ExceptionDao 
	 */
	public Connection getConnection() throws ExceptionDao {
		
		System.out.println("dans getConnection " + uniqueInstance);

		String login = null; String myPassword = null; String url = null;
		final Properties dataProperties = new Properties();
		// Chargement et lecture du fichier 'database.properties' 
		try {
			dataProperties.load(ConnexionManager.class.getClassLoader().getResourceAsStream("database.properties"));
			login = dataProperties.getProperty("login");
			myPassword = dataProperties.getProperty("password");
			url = dataProperties.getProperty("url");
		} catch (Exception e2) {
			LOGGER.error("DAO Exception : Impossible d'ouvir le fichier database.properties");
			throw new ExceptionDao("DAO Exception : Impossible d'ouvir le fichier database.properties");
		}
		// Get objet 'connection'
		try {
			if (connection == null) {
				this.connection = DriverManager.getConnection(url, login, myPassword);	
			}
			return connection;
		} catch (SQLException e) {
			LOGGER.error("DAO Exception : Connexion Bdd impossible."
							+ " Url, login ou motDePasse incorrect ou sgbd non accessible");
			throw new ExceptionDao("DAO Exception : Connexion Bdd impossible. Url, "
							+ "login ou motDePasse incorrect ou sgbd non accessible");
		}
	}
}
