package fr.leo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.leo.enums.EnumDomaine;
import fr.leo.enums.EnumInteret;
import fr.leo.enums.EnumTypeSociete;
import fr.leo.exceptions.ExceptionChampObligatoire;
import fr.leo.exceptions.ExceptionDao;
import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.metier.Adresse;
import fr.leo.metier.Client;
import fr.leo.metier.Prospect;
import fr.leo.metier.Societe;
import fr.leo.utilitaire.ListeSocietes;
import fr.leo.utilitaire.Utilitaire;

/**
 * Classe gestion des Societes
 * @author Fanny
 */
public class DaoSociete {
	
	// ATTRIBUT DE CLASSE
	private static final Logger LOGGER = LogManager.getLogger(DaoSociete.class.getName());
	
	// ATTRIBUTS D INSTANCE
	ListeSocietes listeSocietes;
	ConnexionManager connexionManager;
	
	// CONSTRUCTEURS
	public DaoSociete() {
		this.connexionManager = ConnexionManager.getInstance();
		System.out.println("constructeur DAOSociete "  + connexionManager);
	}	
	/**
	 * On passe en parametre une liste de societe, afin de 
	 *  pouvoir la mettre a jour si necessaire et que 
	 *  la methode appelante dispose d une liste a jour.
	 * @param listeSocietes ListeSocietes
	 * @see ListeSocietes
	 */
	public DaoSociete(ListeSocietes listeSocietes) {
		this.listeSocietes = listeSocietes;
		this.connexionManager = ConnexionManager.getInstance();
	}

	
	// METHODE CREATE_SOCIETE(SOCIETE) :
	/**
	 * Creer une nouvelle ligne dans la Bdd 'reverso'
	 * Retourne la Societe si l objet a ete inserer dans la Bdd, sinon retourne null.
	 * Si la Societe a ete inserer, l'id creer en Bdd est ajouter a l instance.
	 * Met a jour la listeDesSociete en ajoutant la societe nouvellement créé.
	 * @param societe Societe
	 * @return Societe
	 * @throws SQLException
	 * @throws ExceptionDao
	 */
	public Societe createSociete(Societe societe) 
			throws SQLException, ExceptionDao {	
		
		if (societe == null) {
			LOGGER.error("Dao Exception. Methode createSociete(societe)."
						  + "La valeur de l'objet 'societe' est null.");
			throw new ExceptionDao("Dao Exception. Methode createSociete(societe)\n"
									+ "La valeur de l'objet 'societe' est null.");
		}
		
		// EXTRACTION DE LA VALEURS DE CHACUN DES ATTRIBUTS
		String nomRue = societe.getAdresse().getNomRue();
		String numRue = societe.getAdresse().getNumRue();
		String codePostal = societe.getAdresse().getCodePostal();
		String ville = societe.getAdresse().getVille();
		String raisonSociale = societe.getRaisonSociale();
		EnumDomaine domaine = societe.getDomaine();
		String telephone = societe.getTelephone();
		String email = societe.getEmail();
		// VALEURS PARTICULIERES AUX CLIENTS :
		double chiffreAfaire = 0;
		int nbreEmployes = 0;
		// VALEURS PARTICULIERES AUX PROSPECTS :
		String dateProspectionStr = null;
		EnumInteret interet = null;		
		if (societe instanceof Client) {
			chiffreAfaire = ((Client) societe).getChiffreAfaire();
			nbreEmployes = ((Client) societe).getNbrEmployes();			
		}else if (societe instanceof Prospect) {
			dateProspectionStr = ((Prospect) societe).getDateProspection();
			interet = ((Prospect) societe).getInteret();
		}
		
		// FORMATAGE DU CHAMPS 'DATE_PROSPECTION' PROSPECT,
		//  AU FORMAT REQUIS EN BDD
		String dateProspectionFormatteeBdd = null;
		if (societe instanceof Prospect) {
			int jour = Integer.parseInt(dateProspectionStr.substring(0, 2));
			int mois = Integer.parseInt(dateProspectionStr.substring(3, 5));
			int annee = Integer.parseInt(dateProspectionStr.substring(6, 10)); 
			LocalDate dateProspection = LocalDate.of(annee, mois, jour);		
			dateProspectionFormatteeBdd =  Utilitaire.FOMATTER_DATE_BDD.format(dateProspection);	
		}

		// VERIFICATION SI LA SOCIETE EST DEJA PRESENTE EN BDD
		// SI OUI ON ARRETE LE PROCESS
		Integer idSocieteTemp = societe.getIdSociete();
		try {
			if ((idSocieteTemp != null )  &&   (find(idSocieteTemp) != null) ) {
				LOGGER.warn("Methode createSociete(societe)."
						  + "La societe " + societe.getIdSociete() 
						  + " est deja presente en Bdd. Creation non effectuer");
				return null;
			}
		} catch ( ExceptionPersonnalisee | ExceptionChampObligatoire e2) {
			LOGGER.error("Methode createSociete(societe)."
					  + "Une erreur s est produite lors de l enregistrement en Bdd "
					  + "de la societe " + societe.getRaisonSociale());
			throw new ExceptionDao("Dao Exception. Methode createSociete.\n" + e2.getMessage());
		}
		
		// SINON ON CREER LA SOCIETE EN BDD
		// CREATION DE LA LIGNE 'CLIENT' EN BDD : ( ET DE SA LIGNE 'ADRESSE' 
		Connection connection = connexionManager.getConnection();
		System.out.println(connection);
		connection.setAutoCommit(false);
		
		Statement stmtInsert = null;
		ResultSet resulSetAdresse = null;
		ResultSet resulSetClient = null;
		try {
			stmtInsert = connection.createStatement();
			
			String strRequete = null ;
			// INSERTION DES DONNEES RELATIVES A LA SOCIETE, DANS LA TABLE 'SOCIETE'
			if ( societe instanceof Client) {
				strRequete = 	"INSERT INTO societe VALUES " 
								+ "(" + null + ",\'" + raisonSociale + "\',\'" + domaine
								+ "\',\'" + telephone + "\',\'" + email + "\',\'" + "" + "\',\'"
								+ chiffreAfaire + "\',\'"
								+ nbreEmployes + "\'," + null + "," +  null + ",\'"
								+ EnumTypeSociete.CLIENT + "\'" + ")";	
			}
			if (societe instanceof Prospect) {
				strRequete = 	"INSERT INTO societe VALUES " 
								+ "(" + null + ",\'" + raisonSociale + "\',\'" + domaine
								+ "\',\'" + telephone + "\',\'" + email + "\',\'" + "" + "\',"
								+ null + "," +  null  + ",\'" + dateProspectionFormatteeBdd + "\',"
								+ "\'" + interet +  "\', " + "\'"  +EnumTypeSociete.PROSPECT + "\')";	
			}
			stmtInsert.execute(strRequete, Statement.RETURN_GENERATED_KEYS);
			resulSetClient = stmtInsert.getGeneratedKeys();
			
			// INSERTION DES DONNEES RELATIVE A L ADRESSE, DANS LA TABLE 'ADRESSE'
			if (resulSetClient.next()) {
					// On recupere l idSociete generer automatiquement par la Bdd
					//  pour servir de cles etrangere dans la table Adresse
					Integer idSociete = resulSetClient.getInt(1);					
					strRequete = "INSERT INTO adresse VALUES "
								 + "(" + null + ",\'" 
								 + nomRue + "\',\'" + numRue + "\',\'"
								 + codePostal + "\',\' " + ville + "\'" + "," + idSociete + ")";					
					stmtInsert.execute(strRequete, Statement.RETURN_GENERATED_KEYS);
					resulSetAdresse = stmtInsert.getGeneratedKeys();	
					
					if (resulSetAdresse.next()) {				        
				        connection.setAutoCommit(true);
				        LOGGER.info("Methode createSociete(societe)."
								  + "La societe " + societe.getRaisonSociale()
								  + " a ete creer avec success en Bdd") ;
				        // ON MET A JOUR L'ID_SOCIETE DANS L'OBJET PASSE EN PARAMETRE A LA FONCTION
				        societe.setIdSociete(idSociete);
				        // INSERTION DE LA NOUVELLE SOCIETE DANS LA LISTE DES SOCIETES
				        listeSocietes.ajouterSociete(societe);
				        LOGGER.info("Methode createSociete(societe)."
								  + "Ajout de la societe " + societe.getIdSociete()
								  + " a la liste des societes") ;
						return societe;
					}else{
						throw new ExceptionDao("Dao Exception. Methode createSociete.\n"
												+ "Echec creation de la Societe " + raisonSociale +"." );
					}
			} else {
		        LOGGER.error("Methode createSociete(societe)."
						  + "Une erreur s'est produite lors de la creation de la societe " 
						  + societe.getRaisonSociale() + " en Bdd.") ;
				throw new ExceptionDao(	"Dao Exception. Methode createSociete\n."
										+ "Echec creation Societe " + raisonSociale  +".\n"
										+ "Les valeurs pour l adresse sont incorrectes");
			}
		} catch (SQLException | ExceptionPersonnalisee e1) {
			e1.printStackTrace();
		}finally {
			try {	
		    	if (resulSetAdresse  != null) { resulSetAdresse.close();}
		    	if (resulSetClient  != null) { resulSetClient.close();}
		        if (stmtInsert != null) { stmtInsert.close();}
		        //if (connection != null) { connection.close();}
			} catch (SQLException e) {
				   LOGGER.error("Methode createSociete(societe)."
							  + " Une erreur s est produite lors de la fermeture d un resultSet"
							  + " ou d'un Statement. ") ;
				e.printStackTrace();
			}
		}
		return null;
	}	
	
	
	
	// METHODE FIND(ID_SOCIETE) :
	/**
	 * Recherche l existence d une Societe dans la Bdd 'reverso'
	 *  et retourne  la societe si la Societe est trouvé sinon retourne 'null'.
	 * Prend en parametre l id de la societe a rechercher en Bdd.
	 * @param  int idSociete
	 * @return Societe societe
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws ExceptionChampObligatoire
	 */
	public Societe find(int idSociete) 
			throws ExceptionDao, ExceptionPersonnalisee, ExceptionChampObligatoire   {	
		
		if (idSociete <=0 ) {
	        LOGGER.error("Methode find(int idSociete)."
					  + "L identifiant " + idSociete + " de la societe est negatif.") ;
			throw new ExceptionDao("Dao Exception. Methode find(societe)\n"
									+ "L idSociete est negatif.");
		}
		
		Connection connection = connexionManager.getConnection();		
		System.out.println(connection);
		Statement stmt = null;
		ResultSet rs  = null;

		try {
		        // CREATION ET EXECUTION DE LA QUERY DE SELECTION DE LA SOCIETE
				String query = null;
		        query = "Select * from societe "
		        		+ " inner join adresse on societe.id_societe = adresse.fk_societe"
						+ " where id_societe = "
						+  "'" + idSociete +"'";

				stmt = connection.createStatement();
		        rs = stmt.executeQuery(query);
		        if (rs.next()) {	
		        	// SI LA SOCIETE EST PRESENTE DANS LA LISTE_DES_SOCIETE
		        	//  ON LA RETOURNE
		        	if (listeSocietes.chercherSocieteParIdentifant(idSociete) != null) {
		    	        LOGGER.info("Methode find(int idSociete)."
		  					  + "La societe avec l identifiant " + idSociete + " a ete trouve") ;
		        		return listeSocietes.chercherSocieteParIdentifant(idSociete);
		        	}else{
		        		return null;
		        	}
		        	
//		        	// SI LA SOCIETE N EST PAS PRESENTE DANS LA LISTE_DES_SOCIETE
//		        	//  ON CREER UNE NOUVELLE INSTANCE ET ON LA REJOUTE A LA LISTE
//		        	if (listeSocietes.chercherSocieteParIdentifant(idSociete) == null){
//				        	// RECUPERATION DES VALEURS PROVENANT DE LA BDD.
//				            String nomRue  = rs.getString("nom_rue");
//				            String numeroRue  = rs.getString("num_rue");
//				            String codePostal  = rs.getString("code_postal");
//				            String ville  = rs.getString("ville");
//				            
//				            String typeSocieteStr  =  rs.getString("type_societe");
//				            EnumTypeSociete  typeSociete = typeSocieteStr.toLowerCase().equalsIgnoreCase("CLIENT")
//				            								? EnumTypeSociete.CLIENT : EnumTypeSociete.PROSPECT; 
//				            
//				            Integer idSocieteTemp = rs.getInt("id_societe");
//				            String raisonSociale  = rs.getString("raison_sociale");
//				            String telephone  = rs.getString("telephone");
//				            String email  = rs.getString("email");
//							String commentaires  = rs.getString("commentaires");
//							            String domaineClientProspect  =  rs.getString("domaine");
//				            EnumDomaine  domaine = domaineClientProspect.toLowerCase().equalsIgnoreCase("PUBLIC")
//				            								? EnumDomaine.PUBLIC : EnumDomaine.PRIVE; 
//				            EnumInteret  interetProspect = null;
//				            String dateProspectionProspectFormattee = null;
//				            if (typeSociete == EnumTypeSociete.PROSPECT) {
//					            String dateProspectionProspect  = rs.getString("date_prospection");
//					            int annee = Integer.parseInt(dateProspectionProspect.substring(0, 4));
//					            int mois = Integer.parseInt(dateProspectionProspect.substring(5, 7));
//					            int jour = Integer.parseInt(dateProspectionProspect.substring(8, 10)); 
//					            LocalDate lc = LocalDate.of(annee, mois, jour);	
//					           
//					            dateProspectionProspectFormattee =  Utilitaire.FOMATTER_DATE.format(lc);
//					            String interetProspectString  = rs.getString("interet");
//					            interetProspect = interetProspectString == "OUI"
//					            		? EnumInteret.OUI : EnumInteret.NON ;
//				            }
//				            Double chiffre_affaireClient = null;
//				            Integer nombre_employesClient = null;
//				            if (typeSociete == EnumTypeSociete.CLIENT) {
//					            chiffre_affaireClient  = rs.getDouble("chiffre_affaire");
//					            nombre_employesClient  = rs.getInt("nombre_employes"); 
//				            }
//				            
//				            //INSTANCIATION DE L 'ADRESSE. 
//				            Adresse adresse = new Adresse(numeroRue, nomRue, codePostal, ville);
//				            
//				            if (typeSociete == EnumTypeSociete.CLIENT) {
//					            //INSTANCIATION D'UN NOUVEL OBJET 'CLIENT' 
//					            Client client = new Client(  raisonSociale,domaine, 
//					            							 adresse, telephone, 
//					            							 email,chiffre_affaireClient,
//					            							 nombre_employesClient);
//					            client.setIdSociete(idSocieteTemp);
//					            if (commentaires != null) {
//									client.setCommentaires(commentaires);
//								}
//					            return client;
//							}else{
//								//INSTANCIATION D'UN NOUVEL OBJET 'PROSPECT' 
//								Prospect prospect = new Prospect(	raisonSociale, domaine,
//										adresse, telephone, email,
//										dateProspectionProspectFormattee, interetProspect);
//								prospect.setIdSociete(idSocieteTemp);	
//					            if (commentaires != null) {
//					            	prospect.setCommentaires(commentaires);
//									}
//								return prospect;
//							}
//					}
				}	
	    } catch (SQLException e ) {
	        LOGGER.info("Methode find(int idSociete)."
					  + "Une erreur SLQ s est produite :" + e.getMessage() ) ;
			e.printStackTrace();
	    } finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    		//if (connection != null) { connection.close();}
	    	} catch (SQLException e) {
			   LOGGER.error("Methode find( idSociete)."
						  + " Une erreur s est produite lors de la fermeture du resultSet"
						  + " ou d'un Statement : " 
						  + e.getMessage()) ;
				e.printStackTrace();
	    	}
	    }
	   return null;
	}
	
	

	// METHODE FIND_ALL() :
	/**
	 * Recherche la liste des societes presents dans la Bdd 'Reverso'.
	 * Retourne une ArrayList<Societe>.
	 * Retourne une liste vide si la table en Bdd est vide ou si un pbe a été rencontré. 
	 * @return ArrayList<Societe> 
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws ExceptionChampObligatoire
	 * @throws SQLException
	 */
	@SuppressWarnings("resource")
	public ArrayList<Societe> findAll() throws ExceptionDao, ExceptionPersonnalisee,
								ExceptionChampObligatoire, SQLException {

		ArrayList<Societe> listSocietes = new ArrayList<>();
		System.out.println( "VOICI LA CONNEXION :" + connexionManager);
		Connection connection = connexionManager.getConnection();
		System.out.println(connection);
		
		Statement stmt = null;
		ResultSet rs  = null;
		
		try {
			// CREATION ET EXECUTION DE LA QUERY DE SELECTION DES PROSPECTS
			//  ET DE LEURS ADRESSES
			String query = 	"Select * from societe"
							+ " inner join adresse on societe.id_societe = adresse.fk_societe"
							+ " and type_societe like 'prospect' ";	
			stmt = connection.createStatement();
	        rs = stmt.executeQuery(query);
	        
	        // CREATION DES OBJETS 'PROSPECTS' ET ENREGISTREMENT DANS UNE ARRAY_LIST
	        while (rs.next()) {		            
	        	// RECUPERATION DES VALEURS PROVENANT DE LA BDD.
	            String nomRueProspect  = rs.getString("nom_rue");
	            String numeroRueProspect  = rs.getString("num_rue");
	            String codePostalProspect  = rs.getString("code_postal");
	            String villeProspect  = rs.getString("ville");
	            
	            Integer idSociete = rs.getInt("id_societe");
	            String raisonSocialeProspect  = rs.getString("raison_sociale");
	            String domaineClientProspect  =  rs.getString("domaine");
	            EnumDomaine  domaineProspect = domaineClientProspect.toLowerCase().equalsIgnoreCase("PUBLIC")
	            								? EnumDomaine.PUBLIC : EnumDomaine.PRIVE; 
	            String telephoneProspect  = rs.getString("telephone");
	            String emailProspect  = rs.getString("email");
	            String commentairesProspect  = rs.getString("commentaires");
	            String dateProspectionProspect  = rs.getString("date_prospection");
	            int annee = Integer.parseInt(dateProspectionProspect.substring(0, 4));
	            int mois = Integer.parseInt(dateProspectionProspect.substring(5, 7));
	            int jour = Integer.parseInt(dateProspectionProspect.substring(8, 10)); 
	            LocalDate lc = LocalDate.of(annee, mois, jour);		            
	            String dateProspectionProspectFormattee =  Utilitaire.FOMATTER_DATE.format(lc);
	            String interetProspectString  = rs.getString("interet");
	            EnumInteret  interetProspect = interetProspectString.toLowerCase().equalsIgnoreCase("OUI")
						? EnumInteret.OUI : EnumInteret.NON ;
	            
	            //INSTANCIATION D'UN NOUVEL OBJET 'PROSPECT' (ET DE SON OBJET 'ADRESSE'). 
	            Adresse adresseProspect = new Adresse(	numeroRueProspect, nomRueProspect, 
	            										codePostalProspect, villeProspect);
	            Prospect prospect = new Prospect(	raisonSocialeProspect, domaineProspect,
	            									adresseProspect, telephoneProspect, emailProspect,
	            									dateProspectionProspectFormattee, interetProspect);
	            prospect.setIdSociete(idSociete);
	            
	            // AJOUT COMMENTAIRE
	            if (commentairesProspect != null) {
					prospect.setCommentaires(commentairesProspect);
				}
	            listSocietes.add(prospect);
	        }
	        
	        // CREATION ET EXECUTION DE LA QUERY DE SELECTION DU CLIENT
	        query = "Select * from societe"
					+ " inner join adresse on societe.id_societe = adresse.fk_societe "
					+ " and type_societe like 'client' ";
			stmt = connection.createStatement();
	        rs = stmt.executeQuery(query);
	        
	        // CREATION DES OBJETS CLIENTS ET ENREGISTREMENT DANS UNE ARRAY_LIST
	        while (rs.next()) {
	        	// RECUPERATION DES VALEURS PROVENANT DE LA BDD.
	            String nomRueClient  = rs.getString("nom_rue");
	            String numeroRueClient  = rs.getString("num_rue");
	            String codePostalClient  = rs.getString("code_postal");
	            String villeClient  = rs.getString("ville");
	            
	            Integer idSociete = rs.getInt("id_societe");
	            String raisonSocialeClient  = rs.getString("raison_sociale");
	            String domaineClientString  =  rs.getString("domaine");
	            EnumDomaine  domaineClient = domaineClientString.equalsIgnoreCase("PUBLIC")
	            								? EnumDomaine.PUBLIC : EnumDomaine.PRIVE; 
	            String telephoneClient  = rs.getString("telephone");
	            String emailClient  = rs.getString("email");
	            String commentairesClient  = rs.getString("commentaires");
	            Double chiffre_affaireClient  = rs.getDouble("chiffre_affaire");
	            Integer nombre_employesClient  = rs.getInt("nombre_employes");
	            
	            //INSTANCIATION D'UN NOUVEL OBJET 'CLIENT' (ET DE SON OBJET 'ADRESSE'). 
	            Adresse adresseClient = new Adresse(numeroRueClient, nomRueClient,
	            									codePostalClient, villeClient);		            	
	
	            Client client = new Client(  raisonSocialeClient,domaineClient, 
	            							adresseClient, telephoneClient, 
	            							emailClient,chiffre_affaireClient,
	            							nombre_employesClient);
	            client.setIdSociete(idSociete);
	            
	            // AJOUT COMMENTAIRE
	            if (commentairesClient != null) {
					client.setCommentaires(commentairesClient);
				}
	            listSocietes.add(client);		            
	        }
	    } catch (SQLException e ) {
	        LOGGER.info("Methode findAll()."
					  + "Une erreur SLQ s est produite :" + e.getMessage() ) ;
	    	e.printStackTrace();
	    	throw new ExceptionDao(	"ExceptionDao. Methode findAll().\n"
	    							+ e.getMessage());
	    } finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    		//if (connection != null) { connection.close();}
	    	} catch (SQLException e) {
	    		 LOGGER.error("Methode findAll()."
						  + " Une erreur s est produite lors de la fermeture du resultSet"
						  + " ou d'un Statement : " 
						  + e.getMessage()) ;
	    		e.printStackTrace();
	    		throw new ExceptionDao(	"ExceptionDao. Methode findAll().\n"
						+ e.getMessage());
	    	}
	    }
        LOGGER.info("Methode findAll()."
				  + "Renvoit liste des societes " + listSocietes) ;
	   return listSocietes;
	}	
	
	
	
	// METHODE DELETE(SOCIETE) :
	/**
	 * Methode qui supprime une Societe dans la Bdd 'reverso'.
	 * Les contrats associes a cette societe sont supprimer egalement.
	 * Retourne :	'false' si la suppression a echouer
	 * 				'true' si la suppression a reussi  
	 * @param  idSociete Integer
	 * @return Boolean
	 * @throws ExceptionDao
	 * @throws ExceptionChampObligatoire
	 */
	public Boolean delete(Integer idSociete) throws ExceptionDao,  ExceptionChampObligatoire  {		

		// SI L ID_SOCIETE EST NULL OU NEGATIF, ON RENVOI UNE EXCEPTION
		if (idSociete == null || idSociete < 0) {
   		 LOGGER.warn("Methode delete(Integer idSociete)."
					  + "L idSociete a une valeur null."
					  +" Impossible de supprimer un societe avec un IdSociete null ") ;
			throw new ExceptionDao("Dao Exception.\nMethode delete.\n"
									+ "Impossible de supprimer une societe "
									+ "avec un IdSociete 'Null' ( ou un idSociete avec une valeur negative)" );
		}
		
		// VERIFIT SI LA SOCIETE EXISTE EN BDD.
		//  SI IL EXISTE PAS, ON RETOURNE 'FALSE'
		try {
			if ( ( idSociete != null && idSociete > 0 ) && (find(idSociete) == null) ) {
		   		 LOGGER.warn("Methode delete(Integer idSociete)."
						  + "La societe a supprimer existe deja en Bdd.") ;
				return false;				
			}
		} catch ( ExceptionPersonnalisee e2) {
	   		 LOGGER.error("Methode delete(Integer idSociete)."
					  + "Erreur " + e2.getMessage()) ;
			e2.printStackTrace();
			throw new ExceptionDao("Dao Exception. Methode save().\n" + e2.getMessage());
		}
		
		// CONNECTION BDD
		Connection connection = connexionManager.getConnection();
		System.out.println(connection);
		  
		// SUPPRESSION DE LA SOCIETE ET DE SES 'CONTRATS' 
		//  DANS LES TABLE 'SOCIETE' ET 'CONTRAT'	
		String deleteStringSociete = "delete from societe"
									 + " where id_societe = '" + idSociete +"'";	
		try {
			connection.setAutoCommit(false);
			PreparedStatement deleteSociete = connection.prepareStatement(deleteStringSociete); 
			deleteSociete.executeUpdate();
			connection.commit();
	   		 LOGGER.info("Methode delete(Integer idSociete)."
					  + "La societe avec l identifant " + idSociete
					  + " a ete supprimer en Bdd") ;
			// SUPPRESSION DE LA SOCIETE DANS LA LISTE_DES-SOCIETES
			listeSocietes.supprimerSociete(listeSocietes.chercherSocieteParIdentifant(idSociete));	
	   		 LOGGER.info("Methode delete(Integer idSociete)."
					  + "La societe avec l identifant " + idSociete
					  + " a ete supprimer de la liste des societes") ;
			return true;
		} catch (SQLException | ExceptionPersonnalisee e1) {
	   		 LOGGER.error("Methode delete(Integer idSociete)."
					  + "Une erreur s est produite " + e1.getMessage()) ;
			throw new ExceptionDao("Dao Exception. Erreur SQL. La suppression de la societe " 
									+  idSociete + " a echouée.\n"
									+ e1.getMessage() );
		}	
	}
	
	
	
	// METHODE SAVE(SOCIETE) :
	/**
	 * Methode qui prend un objet de type Societe en parametre,
	 *  et l'enregistre dans la Bdd. (Tables Societe et Adresse).
	 * Si la societe existe deja en Bdd, met simplement à jour des attributs.
	 * Si la societe existe pas encore dans la Bdd, alors rajoute une ligne 
	 * 	dans la Bdd pour cette Societe, dans la table 'Societe' et 'Adresse'.
	 * Retourne l objet Societe si celui ci a ete enregistrer correctement en Bdd.
	 * Retourne null si un pbe a été rencontré ou si la Societe n a pas de IdSociete.
	 * @param societe Societe
	 * @return Societe
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws SQLException
	 */
	public Societe save(Societe societe) throws ExceptionDao, ExceptionPersonnalisee, SQLException   {
		

		Connection connection = connexionManager.getConnection();
		System.out.println(connection);
		
		// SI L OBJET CLIENT VAUT 'NULL', ON RENVOIT UNE EXCEPTION
		if (societe == null) {
	   		 LOGGER.error("Methode save(Societe societe)."
					  + "La Societe ne peut pas avoir une valeur egale à Null.") ;
			throw new ExceptionDao("Dao Exception. Methode DaoSociete.save.\n"
									+ "La Societe ne peut pas avoir une valeur egale à Null." );
		}
		
		// EXTRACTION DES VALEURS DE L OBJET 'SOCIETE' PASSER EN PARAMETRE
		String nomRue = societe.getAdresse().getNomRue();
		String numRue = societe.getAdresse().getNumRue();
		String codePostal = societe.getAdresse().getCodePostal();
		Integer idSociete = societe.getIdSociete();
		String ville = societe.getAdresse().getVille();
		String raisonSociale = societe.getRaisonSociale();
		String commentaires = societe.getCommentaires();
		String domaine = societe.getDomaine().toString() ;
		String telephone = societe.getTelephone();
		String email = societe.getEmail();
		String chiffreAfaire = null ;
		String nbreEmployes = null;
		String dateProspection = null;
		String interet = null;
		if (societe instanceof Client) {
			chiffreAfaire =  ((Client) societe).getChiffreAfaire().toString();
			nbreEmployes = ((Client) societe).getNbrEmployes().toString();
		}
		if (societe instanceof Prospect) {
			dateProspection = ((Prospect) societe).getDateProspection();
	        int jour = Integer.parseInt(dateProspection.substring(0, 2));
	        int mois = Integer.parseInt(dateProspection.substring(3, 5));
	        int annee = Integer.parseInt(dateProspection.substring(6, 10));
	        dateProspection = annee +"-" + mois + "-" + jour;
			interet = ((Prospect) societe).getInteret().toString();
		}
		
		// SI LA SOCIETE EXISTE PAS DANS LA BDD, ON LA CREER
		//  ET ON LA RAJOUTE A LA LISTE DES SOCIETES
		Integer idSocieteTemp = societe.getIdSociete();
		try {
			if (idSociete == null) {
				if (this.createSociete(societe) != null) {
					 LOGGER.info("Societe " + societe.getRaisonSociale() + " a ete creer en Bdd");
					 return societe;
				}
			}			
			if (  ( idSociete != null)  &&    (find(idSocieteTemp) == null) ) {				
				 if (this.createSociete(societe) != null) {
					 LOGGER.info("Societe " + societe.getRaisonSociale() + " a ete creer en Bdd");
					 return societe;
				}else {
			   		 LOGGER.error("Methode save(Societe societe)."
							  + "Erreur lors de la creation de la Societe"
			   				  +  societe.getRaisonSociale() +" .") ;
					throw new ExceptionDao(	"Exception Dao. DaoSociete.save()\n" 
											+ "Erreur creation Societe." );
				} 
			}
		} catch (ExceptionChampObligatoire e) {
	   		 LOGGER.error("Methode save(Societe societe)."
					  + "Une erreur s est produite lors de la creation de la Societe"
	   				  +  societe.getRaisonSociale() +" : "
	   				  + e.getMessage()) ;
			e.printStackTrace();
			throw new ExceptionDao(	"Exception Dao. DaoSociete.save()\n" 
					+ "Erreur creation Societe." 
					+ e.getMessage());
		}
		
		// SI LA SOCIETE EXISTE, ON MODIFIE SES VALEURS DANS LA BDD
		PreparedStatement updateClient = null;
		String updateString ;
		connection.setAutoCommit(false);
		
	    // PREPARATION MISE A JOUR DES CHAMPS DE LA TABLE ADRESSE
		updateString = 	"update adresse, societe set "
						+ "adresse.nom_rue = ? ,"
						+ "adresse.num_rue = ? ,"
						+ "adresse.code_postal = ? ,"
						+ "adresse.ville = ? "
						+ "where adresse.fk_societe = societe.id_societe and  "
						+ "societe.id_societe = " + "'" + idSociete + "'";   
		updateClient = connection.prepareStatement(updateString);   
		updateClient.setString(1,nomRue); 
		updateClient.setString(2,numRue); 
		updateClient.setString(3,codePostal); 
		updateClient.setString(4,ville);
    	updateClient.executeUpdate();
        
	    // PREPARATION MISE A JOUR DES CHAMPS DE LA TABLE SOCIETE 
    	if (societe instanceof Client) {
    		updateString = 	"update societe set "
    				+ "societe.raison_sociale = ? ,"
    				+ "societe.domaine = ? ,"
    				+ "societe.telephone = ? ,"
    				+ "societe.email = ? ,"
    				+ "societe.commentaires = ? ,"
    				+ "societe.chiffre_affaire = ? ,"
    				+ "societe.nombre_employes = ? "
    				+ "where societe.id_societe = " + "'" + idSociete + "'";	  
    		updateClient = connection.prepareStatement(updateString);   
    		updateClient.setString(1,raisonSociale); 
    		updateClient.setString(2,domaine); 
    		updateClient.setString(3,telephone); 
    		updateClient.setString(4,email); 
    		updateClient.setString(5,commentaires);
    		updateClient.setString(6,chiffreAfaire);
    		updateClient.setString(7,nbreEmployes);
    		updateClient.executeUpdate();
    		LOGGER.info("Societe " + societe.getRaisonSociale() + " modifier en Bdd");
		}
    	if (societe instanceof Prospect) {
    		updateString = 	"update societe set "
					+ "societe.raison_sociale = ? ,"
					+ "societe.domaine = ? ,"
					+ "societe.telephone = ? ,"
					+ "societe.email = ? ,"
					+ "societe.commentaires = ? ,"
					+ "societe.date_prospection = ? ,"
					+ "societe.interet = ? "
					+ "where societe.id_societe = " + "'" + idSociete + "'";	  
			updateClient = connection.prepareStatement(updateString);   
			updateClient.setString(1,raisonSociale); 
			updateClient.setString(2,domaine); 
			updateClient.setString(3,telephone); 
			updateClient.setString(4,email); 
			updateClient.setString(5,commentaires);
			updateClient.setString(6,dateProspection);
			updateClient.setString(7,interet);
			updateClient.executeUpdate();
			LOGGER.info("Societe " + societe.getRaisonSociale() + " modifier en Bdd");
		}
    	
    	// MISE A JOUR DE LA SOCIETE ET DE SON ADRESSE EN BDD
    	listeSocietes.supprimerSociete(listeSocietes.chercherSocieteParRaisonSociale(raisonSociale));    
    	listeSocietes.ajouterSociete(societe);
    	connection.commit();  
 		 LOGGER.info("Methode save(Societe societe)."
				  + "La Societe " +  societe.getRaisonSociale() + " a ete mise a jour") ;
    	//if (connection != null) { connection.close();}        	
        return societe;
	}
}	