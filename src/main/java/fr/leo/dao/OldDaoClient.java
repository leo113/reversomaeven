package fr.leo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import fr.leo.enums.EnumDomaine;
import fr.leo.exceptions.ExceptionChampObligatoire;
import fr.leo.exceptions.ExceptionDao;
import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.metier.Adresse;
import fr.leo.metier.Client;
import fr.leo.utilitaire.ListeSocietes;

/**
 * Classe gestion de  Clients
 * @author Fanny
 */
public class OldDaoClient {
	
	// ATTRIBUTS D INSTANCE
	ListeSocietes listeSocietes;
	ConnexionManager connexionManager;
	
	// CONSTRUCTEURS
	public OldDaoClient() {
		@SuppressWarnings("unused")
		ConnexionManager connexionManager = ConnexionManager.getInstance();
	}	
	/**
	 * @param listeSocietes ListeSocietes
	 * @see ListeSocietes
	 */
	public OldDaoClient(ListeSocietes listeSocietes) {
		this.listeSocietes = listeSocietes;
		@SuppressWarnings("unused")
		ConnexionManager connexionManager = ConnexionManager.getInstance();
	}

	
	// METHODE CREATE_CLIENT(CLIENT) :
	/**
	 * Creer une nouvelle ligne dans la Bdd 'reverso'
	 * Retourne true si l objet a ete inserer dans la Bdd, sinon retourne false.
	 * Met a jout la listeDesSociete en ajoutant le Client nouvellement créé.
	 * @param client
	 * @return Boolean
	 * @throws SQLException
	 * @throws ExceptionDao
	 */
	public Boolean createClient(Client client) 
			throws SQLException, ExceptionDao {	
		
		if (client == null) {
			throw new ExceptionDao("Dao Exception. Methode createClient(client)\n"
									+ "La valeur de l'objet 'client' est null.");
		}
		
		// EXTRACTION DE LA VALEURS DE CHACUN DES ATTRIBUTS
		String nomRue = client.getAdresse().getNomRue();
		String numRue = client.getAdresse().getNumRue();
		String codePostal = client.getAdresse().getCodePostal();
		String ville = client.getAdresse().getVille();
		String raisonSociale = client.getRaisonSociale();
		EnumDomaine domaine = client.getDomaine();
		String telephone = client.getTelephone();
		String email = client.getEmail();
		double chiffreAfaire = client.getChiffreAfaire();
		int nbreEmployes = client.getNbrEmployes();

		// VERIFICATION SI LE PROSPECT EST DEJA PRESENT EN BDD
		// SI OUI ON ARRETE LE PROCESS
		try {
			if ( find(raisonSociale) != null ) {
				return false;
			}
		} catch ( ExceptionPersonnalisee | ExceptionChampObligatoire e2) {
			throw new ExceptionDao("Dao Exception. Methode createClient.\n" + e2.getMessage());
		}
		
		// CREATION DE LA LIGNE 'CLIENT' EN BDD : ( ET DE SA LIGNE 'ADRESSE' )
		Connection connection = connexionManager.getConnection();
		connection.setAutoCommit(false);
		
		Statement stmtInsert = null;
		ResultSet resulSetAdresse = null;
		ResultSet resulSetClient = null;
		try {
			stmtInsert = connection.createStatement();
			
			// INSERTION DES DONNEES RELATIVE A L ADRESSE, DANS LA TABLE 'ADRESSE'
			String strRequete = "INSERT INTO adresse VALUES "
								+ "(" + null + ",\'" 
								+ nomRue + "\',\'" + numRue + "\',\'"
								+ codePostal + "\',\' " + ville + "\'" + ")";
			stmtInsert.execute(strRequete, Statement.RETURN_GENERATED_KEYS);
			resulSetAdresse = stmtInsert.getGeneratedKeys();			
			
			// INSERTION DES DONNEES RELATIVES AU CLIENT, DANS LA TABLE 'CLIENT'
			if (resulSetAdresse.next()) {
					Integer fkAdresse = resulSetAdresse.getInt(1);				
	
					strRequete = 	"INSERT INTO client VALUES " 
									+ "(" + null + ",\'" + raisonSociale + "\',\'" + domaine
									+ "\',\'" + telephone + "\',\'" + email + "\',\'" + "" + "\',\'"
									+ chiffreAfaire + "\',\'"
									+ nbreEmployes + "\',\'" + fkAdresse + "\'" + ")";
					
					stmtInsert.execute(strRequete, Statement.RETURN_GENERATED_KEYS);
					resulSetClient = stmtInsert.getGeneratedKeys();		
					
					if (resulSetClient.next()) {				        
				        connection.setAutoCommit(true);
				        // INSERTION DU NOUVEAU CLIENT DANS LA LISTE DES SOCIETES
				        listeSocietes.ajouterSociete(client);
						return true;
					}else{
						throw new ExceptionDao("Dao Exception. Methode createClient.\n"
												+ "Echec creation du Client " + raisonSociale +"." );
					}
			} else {
				throw new ExceptionDao(	"Dao Exception. Methode createClient\n."
										+ "Echec creation Client " + raisonSociale  +".\n"
										+ "Les valeurs pour l adresse sont incorrectes");
			}
		} catch (SQLException | ExceptionPersonnalisee e1) {
			e1.printStackTrace();
		}finally {
			try {	
		    	if (resulSetAdresse  != null) { resulSetAdresse.close();}
		    	if (resulSetClient  != null) { resulSetClient.close();}
		        if (stmtInsert != null) { stmtInsert.close();}
		        if (connection != null) { connection.close();}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}	

	// METHODE FIND(STRING RAISON_SOCIALE) :
	/**
	 * Recherche l existence d un Client(raisonSociale) dans la Bdd 'reverso'
	 *   et en retourne une instance.
	 * Retourne null si le Client(raisonSociale) n a pas  d existence en Bdd
	 *  ou si un pbe a été rencontré.
	 * @param raisonSociale
	 * @return
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws ExceptionChampObligatoire
	 */
	public Client find(String raisonSociale) 
			throws ExceptionDao, ExceptionPersonnalisee, ExceptionChampObligatoire   {		
		
		Connection connection = connexionManager.getConnection();			
		Statement stmt = null;
		ResultSet rs  = null;

		try {
		        // CREATION ET EXECUTION DE LA QUERY DE SELECTION DU CLIENT
				String query = 	"Select * from client"
					    		+ " inner join adresse on client.fk_adresse = adresse.id_adresse	"
					    		+ "where raison_sociale = "
					    		+  "'"+ raisonSociale +"'";	
				stmt = connection.createStatement();
		        rs = stmt.executeQuery(query);
		        
		        // RECUPERATION DES VALEURS PROVENANT DE LA BDD,
		        //  ET INSTANCIATION D'UN NOUVEL OBJET 'CLIENT' ( ET DE SON OBJET 'ADRESSE' ) 
		        while (rs.next()) {
		        	// RECUPERATION DES VALEURS PROVENANT DE LA BDD.
		        	Integer idSocieteClient = rs.getInt("id_societe");
		            String nomRueClient  = rs.getString("nom_rue");
		            String numeroRueClient  = rs.getString("num_rue");
		            String codePostalClient  = rs.getString("code_postal");
		            String villeClient  = rs.getString("ville");
		            
		            String raisonSocialeClient  = rs.getString("raison_sociale");
		            String domaineClientString  =  rs.getString("domaine");
		            EnumDomaine  domaineClient = domaineClientString == "PUBLIC"
		            								? EnumDomaine.PUBLIC : EnumDomaine.PRIVE; 
		            String telephoneClient  = rs.getString("telephone");
		            String emailClient  = rs.getString("email");
		            String commentairesClient  = rs.getString("commentaires");
		            Double chiffre_affaireClient  = rs.getDouble("chiffre_affaire");
		            Integer nombre_employesClient  = rs.getInt("nombre_employes");
		            
		            //INSTANCIATION D'UN NOUVEL OBJET 'CLIENT' (ET DE SON OBJET 'ADRESSE'). 
		            Adresse adresseClient = new Adresse(numeroRueClient, nomRueClient,
		            									codePostalClient, villeClient);		          
		            
		            Client client = new Client( raisonSocialeClient, domaineClient, 
												adresseClient, telephoneClient, 
												emailClient,chiffre_affaireClient,
												nombre_employesClient);
		            client.setIdSociete(idSocieteClient);
		            
		            // AJOUT COMMENTAIRE
		            if (commentairesClient != null) {
						client.setCommentaires(commentairesClient);
					}
		            return client;
		        }
	    } catch (SQLException e ) {
			e.printStackTrace();
	    } finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    		if (connection != null) { connection.close();}
	    	} catch (SQLException e) {
				e.printStackTrace();
	    	}
	    }
	   return null;
	}
	
	// METHODE FIND(CLIENT) :
	/**
	 * Recherche l existence d un Client dans la Bdd 'reverso'
	 *  et retourne  'true' si le CLient est trouvé sinon retourne 'false'
	 * @param client
	 * @return Boolean
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws ExceptionChampObligatoire
	 */
	public Boolean find(Client client) 
			throws ExceptionDao, ExceptionPersonnalisee, ExceptionChampObligatoire   {	
		
		if (client == null) {
			throw new ExceptionDao("Dao Exception. Methode find(client)\n"
									+ "L objet client est null.");
		}
		
		Connection connection = connexionManager.getConnection();			
		Statement stmt = null;
		ResultSet rs  = null;

		try {
		        // CREATION ET EXECUTION DE LA QUERY DE SELECTION DU CLIENT
				String query = 	"Select * from client"
					    		+ " inner join adresse on client.fk_adresse = adresse.id_adresse	"
					    		+ "where raison_sociale = "
					    		+  "'"+ client.getRaisonSociale() +"'";	
				stmt = connection.createStatement();
		        rs = stmt.executeQuery(query);
		        if (rs.next()) {
					return true;
				}	
	    } catch (SQLException e ) {
			e.printStackTrace();
	    } finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    		if (connection != null) { connection.close();}
	    	} catch (SQLException e) {
				e.printStackTrace();
	    	}
	    }
	   return false;
	}
	

	// METHODE FIND_ALL() :
	/**
	 * Recherche la liste des clients presents dans la Bdd 'Reverso'.
	 * Retourne une ArrayList<Client>.
	 * Retourne une liste vide si la table est vide ou si un pbe a été rencontré. 
	 * @return ArrayList<Client>
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws ExceptionChampObligatoire
	 * @throws SQLException
	 */
	public ArrayList<Client> findAll() throws ExceptionDao, ExceptionPersonnalisee,
								ExceptionChampObligatoire, SQLException {

		ArrayList<Client> listClients = new ArrayList<>();
		Connection connection = connexionManager.getConnection();
		
		Statement stmt = null;
		ResultSet rs  = null;
		
		try {
				// CREATION ET EXECUTION DE LA QUERY DE SELECTION DES CLIENTS
				String query = 	"Select * from client"
								+ " inner join adresse on client.fk_adresse = adresse.id_adresse";	
				stmt = connection.createStatement();
		        rs = stmt.executeQuery(query);
		        
		        // CREATION DES OBJETS CLIENTS ET ENREGISTREMENT DANS UNE ARRAY_LIST
		        while (rs.next()) {		            
		            String raisonSocialeClient  = rs.getString("raison_sociale");
		            Client client = this.find(raisonSocialeClient);
		            listClients.add(client);
		        }
	    } catch (SQLException e ) {
	    	e.printStackTrace();
	    	throw new ExceptionDao(	"ExceptionDao. Methode findAll().\n"
	    							+ e.getMessage());
	    } finally {
	    	try{
	    		if (rs != null) { rs.close();}
	    		if (stmt != null) { stmt.close();}
	    		if (connection != null) { connection.close();}
	    	} catch (SQLException e) {
	    		e.printStackTrace();
	    		throw new ExceptionDao(	"ExceptionDao. Methode findAll().\n"
						+ e.getMessage());
	    	}
	    }
	   return listClients;
	}	
	
	// METHODE DELETE(CLIENT) :
	/**
	 * Methode qui supprime un Client dans la Bdd 'reverso'.
	 * Retourne :	'false' si la suppression a echouer
	 * 				'true' si la suppression a reussi  
	 * @param client Client
	 * @return Boolean
	 * @throws ExceptionDao
	 * @throws ExceptionChampObligatoire
	 */
	public Boolean delete(Client client) throws ExceptionDao,  ExceptionChampObligatoire  {		

		// SI LE CLIENT EST NULL, ON RENVOI UNE EXCEPTION
		if (client == null) {
			throw new ExceptionDao("Dao Exception.\nMethode delete.\n"
									+ "Impossible de supprimer un client avec la valeur 'Null'");
		}
		
		// VERIFIT SI LE CLIENT EXISTE EN BDD.
		//  SI IL EXISTE PAS, ON RETOURNE 'FALSE'
		String raisonSociale = client.getRaisonSociale();		
		try {
			if (find(raisonSociale) == null ) {
				return false;				
			}
		} catch ( ExceptionPersonnalisee e2) {
			e2.printStackTrace();
			throw new ExceptionDao("Dao Exception. Methode save().\n" + e2.getMessage());
		}
		
		// CONNECTION BDD
		Connection connection = connexionManager.getConnection();
		
		// RECHERCHE DE LA CLE ETRANGERE => 'TABLE_ADRESSE', POUR CE CLIENT
		String fkAdresse = null;
		String query = 	"Select * from client where raison_sociale = "
						+  "'" + raisonSociale +"'" ;	
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				fkAdresse  = rs.getString("fk_adresse");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		  
		// SUPPRESSION DU CLIENT ET DE SON 'ADRESSE' 
		//  DANS LES TABLES 'CLIENT' ET 'ADRESSE'
		String deleteStringClient = "delete from client where raison_sociale = "
				+  "'"+ raisonSociale +"'";			
		String deleteStringAdresse = "delete from adresse where  adresse.id_adresse = "
				+ fkAdresse ;	
		try {
			connection.setAutoCommit(false);
			PreparedStatement deleteClient = connection.prepareStatement(deleteStringClient);  
			PreparedStatement deleteAdresse = connection.prepareStatement(deleteStringAdresse); 
			deleteClient.executeUpdate();	
			deleteAdresse.executeUpdate();	
			connection.commit();
			// SUPPRESSION DE LA SOCIETE DANS LA LISTE_DES-SOCIETES
			listeSocietes.supprimerSociete(client);
			return true;
		} catch (SQLException | ExceptionPersonnalisee e1) {
			throw new ExceptionDao("Dao Exception. Erreur SQL. La suppression du client " 
									+  raisonSociale + " a echouée.\n"
									+ e1.getMessage() );
		}	
	}
	
	
	// METHODE SAVE(CLIENT) :
	/**
	 * Methode qui prend un objet de type Client en parametre,
	 *  et l'enregistre dans la Bdd. (Tables Client et Adresse).
	 * Si le client existe deja en Bdd, met simplement à jour des attributs.
	 * Si le client existe pas encore dans la Bdd, alors rajoute une ligne 
	 * 	dans la Bdd pour ce client, dans les tables 'Clients' et 'Adresse'.
	 * Retourne l objet Client si celui ci a ete enregistrer correctement en Bdd.
	 * Retourne null si un pbe a été rencontré.
	 * @param client Client
	 * @return Client
	 * @throws ExceptionDao
	 * @throws ExceptionPersonnalisee
	 * @throws SQLException
	 */
	public Client save2(Client client) throws ExceptionDao, ExceptionPersonnalisee, SQLException   {
		
		Connection connection = connexionManager.getConnection();	
		
		// SI L OBJET CLIENT VAUT 'NULL', ON RENVOIT UNE EXCEPTION
		if (client == null) {
			throw new ExceptionDao("Dao Exception. Methode DaoClient.save2.\n"
									+ "Le client ne peut pas avoir une valeur egale à Null." );
		}
		
		// EXTRACTION DES VALEURS DE L OBJET 'CLIENT' PASSE EN PARAMETRE
		String nomRue = client.getAdresse().getNomRue();
		String numRue = client.getAdresse().getNumRue();
		String codePostal = client.getAdresse().getCodePostal();
		String ville = client.getAdresse().getVille();
		String raisonSociale = client.getRaisonSociale();
		String commentaires = client.getCommentaires();
		String domaine = client.getDomaine().toString() ;
		String telephone = client.getTelephone();
		String email = client.getEmail();
		String chiffreAfaire = client.getChiffreAfaire().toString();
		String nbreEmployes = client.getNbrEmployes().toString();
		
		// SI LE CLIENT EXISTE PAS DANS LA BDD, ON LE CREER
		//  ET ON LE RAJOUTE A LA LISTE DES SOCIETES
		try {
			if ( find(raisonSociale) == null ) {				
				 if (this.createClient(client)) {
					 return client;
				}else {
					throw new ExceptionDao(	"Exception Dao. DaoClient.save2()\n" 
											+ "Erreur creation Client." );
				} 
			}
		} catch (ExceptionChampObligatoire e) {
			e.printStackTrace();
			throw new ExceptionDao(	"Exception Dao. DaoClient.save2()\n" 
					+ "Erreur creation Client." 
					+ e.getMessage());
		}
		
		// SI LE CLIENT EXISTE, ON MODIFIE SES VALEURS DANS LA BDD
		PreparedStatement updateClient = null;
		String updateString ;
		connection.setAutoCommit(false);
		
	    // PREPARATION MISE A JOUR DES CHAMPS DE LA TABLE ADRESSE
		updateString = 	"update adresse, client set "
						+ "adresse.nom_rue = ? ,"
						+ "adresse.num_rue = ? ,"
						+ "adresse.code_postal = ? ,"
						+ "adresse.ville = ? "
						+ "where adresse.id_adresse = client.fk_adresse and  "
						+ "client.raison_sociale = " + "'" + raisonSociale + "'";   
		updateClient = connection.prepareStatement(updateString);   
		updateClient.setString(1,nomRue); 
		updateClient.setString(2,numRue); 
		updateClient.setString(3,codePostal); 
		updateClient.setString(4,ville);
    	updateClient.executeUpdate();
        
	    // PREPARATION MISE A JOUR DES CHAMPS DE LA TABLE CLIENT    	
		updateString = 	"update client set "
						+ "client.raison_sociale = ? ,"
						+ "client.domaine = ? ,"
						+ "client.telephone = ? ,"
						+ "client.email = ? ,"
						+ "client.commentaires = ? ,"
						+ "client.chiffre_affaire = ? ,"
						+ "client.nombre_employes = ? "
						+ "where client.raison_sociale = " + "'" + raisonSociale + "'";	  
		updateClient = connection.prepareStatement(updateString);   
		updateClient.setString(1,raisonSociale); 
		updateClient.setString(2,domaine); 
		updateClient.setString(3,telephone); 
		updateClient.setString(4,email); 
		updateClient.setString(5,commentaires);
		updateClient.setString(6,chiffreAfaire);
		updateClient.setString(7,nbreEmployes);
    	updateClient.executeUpdate();
    	
    	// MISE A JOUR DU CLIENT ET DE SON ADRESSE EN BDD
    	listeSocietes.supprimerSociete(listeSocietes.chercherSocieteParRaisonSociale(raisonSociale));
    	listeSocietes.ajouterSociete(client);
    	connection.commit();    	
    	if (connection != null) { connection.close();}        	
        return client;
	}
}	


//// METHODE QUI VERIFIT SI UN  CLIENT EXISTE DEJA DANS LA BDD
//// Retourne true si le Client est present dans la Bdd 'reverso' sinon retourne false
//private boolean checkIfClientExistAlreadyInDatabase(String raisonSociale) 
//				throws ExceptionDao, ExceptionPersonnalisee {
//	
//		Client clientAlreadyExist = null;
//		try {
//			clientAlreadyExist = this.find(raisonSociale);
//		} catch (ExceptionChampObligatoire e2) {
//			throw new ExceptionDao(	"Dao Erreur. Methode checkIfExistAlreadyInDatabase()\n" 
//									+ "Erreur client " + raisonSociale + ".\n"
//									+ e2.getMessage());
//		}
//		if (clientAlreadyExist == null) {
//			return false;
//		}
//		return true;
//	}

//// Methode save()
///**
// * Methode de mise a jour d un/des champ(s) d un Client particulier dans la Bdd 'reverso'.
// * Selection du Client a partir de sa 'raisonSociale'. 
// * La liste des "champs/valeurs" a modifier, sont passes en parametre
// *  dans une HashMap du type 'HashMap<EnumChampsTableClient, String>'
// *  Retourne un booleen a 'true' si la mise a jour a ete effective.
// *  Retourne false si la mise a jour a rencontre un pbe. 
// * Si le client n existe pas en Bdd, il est creer en bdd et on 
// *  en retourne une instance ( ceci a condition que tous les 
// *  champs obligatoires pour un Client aient ete renseignes) 
// * @param listeChampsValeurs  HashMap<EnumChampsTableClient, String> 
// * @param raisonSociale
// * @return Client 
// * @throws SQLException
// * @throws ExceptionDao
// */
//public Client save(HashMap<EnumChampsTableClient, String> listeChampsValeurs, String raisonSociale)
//								throws SQLException, ExceptionDao   {
//	
//	Connection connection = ConnexionManager.getConnection();		
//	
//	// CHECK SI LE CLIENT EXISTE EN BDD
//	//  SI IL N EXISTE PAS ON LE CREER ( a condition que toutes
//	//  					valeurs obligatoires soient renseignees )
//	try {
//		if (! checkIfClientExistAlreadyInDatabase(raisonSociale) ) {
//			String num_rue = null;	String nom_rue = null ;String code_postal = null;
//			String ville = null ;String raison_sociale =null; EnumDomaine domaineTempEnumDomaine = null;
//			String telephone = null ;String email=null; String commentaires = null ;
//			Double chiffre_affaire = null ;	Integer nombre_employes = null;	
//			
//			// ON RECUPERE TOUTES LES VALEURS PASSEES EN PARAMATRES DANS LA HASH_MAP
//			for (Entry<EnumChampsTableClient, String> champAMettreAJour : listeChampsValeurs.entrySet()) {
//				try {
//					if(champAMettreAJour.getKey().equals(EnumChampsTableClient.num_rue)){
//						num_rue = champAMettreAJour.getValue(); 
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableClient.nom_rue)){
//						nom_rue = champAMettreAJour.getValue(); 
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableClient.code_postal)){
//						code_postal = champAMettreAJour.getValue(); 
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableClient.ville)){
//						ville = champAMettreAJour.getValue(); 
//					}
//					if(champAMettreAJour.getKey().equals(raisonSociale)){
//						 raison_sociale = champAMettreAJour.getValue(); 
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableClient.domaine)){
//						String domaineTempString =  champAMettreAJour.getValue();
//						domaineTempEnumDomaine = domaineTempString== "PUBLIC"
//																	? EnumDomaine.PUBLIC : EnumDomaine.PRIVE; 								
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableClient.telephone)){
//						telephone = champAMettreAJour.getValue(); 
//					}				
//					if(champAMettreAJour.getKey().equals(EnumChampsTableClient.email)){
//						email = champAMettreAJour.getValue(); 		
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableClient.commentaires)){
//						commentaires = champAMettreAJour.getValue(); 	
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableClient.chiffre_affaire)){
//						chiffre_affaire = Double.parseDouble(champAMettreAJour.getValue());			
//					}
//					if(champAMettreAJour.getKey().equals(EnumChampsTableClient.nombre_employes)){
//						nombre_employes = Integer.parseInt(champAMettreAJour.getValue());			
//					}	
//				} catch (NumberFormatException e) {						
//					JOptionPane.showMessageDialog(null,
//							"Echec Mise A Jour de l'objet Client " 
//							+ raisonSociale + " en Bdd : \n" + e.getMessage() ,
//							"Erreur", JOptionPane.WARNING_MESSAGE);
//							e.printStackTrace();
//					throw new ExceptionDao("Dao Exception. Methode Save.\n" 
//											+ e.getMessage());					}
//			}
//			// INSTANCIATION DU NOUVEAU CLIENT
//			Adresse newAdresseTemp = new Adresse(num_rue, nom_rue, code_postal, ville);
//			Client newCLient = new Client(	raisonSociale, domaineTempEnumDomaine,
//											newAdresseTemp, telephone, email, 
//											chiffre_affaire, nombre_employes);
//			 if (this.createClient(newCLient)) {
//				 return newCLient;
//			} 
//		}
//	}  catch (ExceptionPersonnalisee | ExceptionChampObligatoire e2) {
//		e2.printStackTrace();
//		throw new ExceptionDao("Dao Exception. Methode save().\n" + e2.getMessage());
//	}
//		
//	// SI LE CLIENT EXISTE PAS EN BDD, ON FAIT LA MISE A JOUR 
//	PreparedStatement updateClient = null;
//	String updateString ;
//	Boolean existingField;
//	connection.setAutoCommit(false);
//	
//	// INSTANCIATION DU CLIENT A PARTIR DES DONNEES EN BDD.
//	// CETTE INSTANCE VA SERVIR A VERIFIER LA CORRECTION DES NOUVELLES VALEURS.
//	Client clientAMettreAJour = null;
//	//clientAMettreAJour = this.find(raisonSociale);
//	clientAMettreAJour =  (Client) listeSocietes.chercherSocieteParRaisonSociale(raisonSociale);
//	// TEST DE LA CORRECTION DES CHAMPS A METTRE A JOUR		
//	for (Entry<EnumChampsTableClient, String> champAMettreAJour : listeChampsValeurs.entrySet()) {
//		try {
//			if(champAMettreAJour.getKey().equals(EnumChampsTableClient.num_rue)){
//				clientAMettreAJour.getAdresse().setNumRue(champAMettreAJour.getValue()); 
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableClient.nom_rue)){
//				clientAMettreAJour.getAdresse().setNomRue(champAMettreAJour.getValue()); 
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableClient.code_postal)){
//				clientAMettreAJour.getAdresse().setCodePostal(champAMettreAJour.getValue()); 
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableClient.ville)){
//				clientAMettreAJour.getAdresse().setVille(champAMettreAJour.getValue()); 
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableClient.raison_sociale)){
//				clientAMettreAJour.setRaisonSociale(champAMettreAJour.getValue());			
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableClient.domaine)){
//				String domaineTempString =  champAMettreAJour.getValue();
//				EnumDomaine domaineTempEnumDomaine = domaineTempString== "PUBLIC"
//															? EnumDomaine.PUBLIC : EnumDomaine.PRIVE; 
//				clientAMettreAJour.setDomaine(domaineTempEnumDomaine);			
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableClient.telephone)){
//				clientAMettreAJour.setTelephone(champAMettreAJour.getValue());	
//			}				
//			if(champAMettreAJour.getKey().equals(EnumChampsTableClient.email)){
//					clientAMettreAJour.setEmail(champAMettreAJour.getValue());			
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableClient.commentaires)){
//				clientAMettreAJour.setCommentaires(champAMettreAJour.getValue());			
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableClient.chiffre_affaire)){
//				clientAMettreAJour.setChiffreAfaire( Double.parseDouble(champAMettreAJour.getValue()));			
//			}
//			if(champAMettreAJour.getKey().equals(EnumChampsTableClient.nombre_employes)){
//				clientAMettreAJour.setNbrEmployes(Integer.parseInt(champAMettreAJour.getValue()));			
//			}	
//		} catch (ExceptionChampObligatoire | ExceptionPersonnalisee | NumberFormatException e) {
//			JOptionPane.showMessageDialog(null,
//					"Erreur Dao. Echec Mise A Jour de l'objet Client " 
//					+ raisonSociale + " en Bdd : \n" + e.getMessage() ,
//					"Erreur", JOptionPane.WARNING_MESSAGE);
//					e.printStackTrace();
//			throw new ExceptionDao("Dao Exception. Methode Save.\n" 
//									+ e.getMessage());
//		}
//	}
//	
//    // PREPARATION MISE A JOUR DES CHAMPS DE LA TABLE ADRESSE
//    for (Entry<EnumChampsTableClient, String> champAMettreAJour : listeChampsValeurs.entrySet()) {
//    		existingField = false;
//    		
//			updateString = 	"update adresse, client set adresse." + champAMettreAJour.getKey() +  "= ? "
//							+ "where adresse.id_adresse = client.fk_adresse and  "
//							+ "client.raison_sociale = " + "'" + raisonSociale + "'";
//			
//			updateClient = connection.prepareStatement(updateString);   
//			
//			existingField = preparationChamps(updateClient,  champAMettreAJour, existingField, EnumChampsTableClient.num_rue);
//			existingField = preparationChamps(updateClient,  champAMettreAJour, existingField, EnumChampsTableClient.nom_rue);
//			existingField = preparationChamps(updateClient,  champAMettreAJour, existingField, EnumChampsTableClient.code_postal);
//			existingField = preparationChamps(updateClient,  champAMettreAJour, existingField, EnumChampsTableClient.ville);    			
//        	if (existingField) {
//        		updateClient.executeUpdate();	        			
//			}
//    }        
//    
//    // PREPARATION MISE A JOUR DES CHAMPS DE LA TABLE CLIENT
//    for (Entry<EnumChampsTableClient, String> champAMettreAJour : listeChampsValeurs.entrySet()) {
//    		existingField = false;
//    		
//			updateString = 	"update client set client." + champAMettreAJour.getKey() +  "= ? "
//							+ "where client.raison_sociale = " + "'" + raisonSociale + "'";	        			
//			updateClient = connection.prepareStatement(updateString);
//			
//        	existingField = preparationChamps(updateClient, champAMettreAJour, existingField, EnumChampsTableClient.raison_sociale);
//        	existingField = preparationChamps(updateClient, champAMettreAJour, existingField, EnumChampsTableClient.domaine);
//        	existingField = preparationChamps(updateClient, champAMettreAJour, existingField, EnumChampsTableClient.telephone);
//        	existingField = preparationChamps(updateClient, champAMettreAJour, existingField, EnumChampsTableClient.email);
//        	existingField = preparationChamps(updateClient, champAMettreAJour, existingField, EnumChampsTableClient.commentaires);
//        	existingField = preparationChamps(updateClient, champAMettreAJour, existingField, EnumChampsTableClient.chiffre_affaire);
//        	existingField = preparationChamps(updateClient, champAMettreAJour, existingField, EnumChampsTableClient.nombre_employes);   	
//        
//        	if (existingField) {
//        		updateClient.executeUpdate();	        			
//			}	        	
//    }
//    	// MISE A JOUR DU CLIENT ET DE SON ADRESSE EN BDD
//    	connection.commit();        	
//		JOptionPane.showMessageDialog(null,
//				"Le client " + raisonSociale + " a été mis à jour avec succés dans la Bdd",
//				"Mise a jour", JOptionPane.INFORMATION_MESSAGE);
//    	if (connection != null) { connection.close();}        	
//        return clientAMettreAJour;
//}
//
///**
// * Methode qui prepare la mise a jour d un champs particulier.
// * Retourne 'true' si le champs passe en parametre a ete preparer.
// * @param updateClient
// * @param champAMettreAJour
// * @param existingField
// * @param champ
// * @return champAMettreAJour
// * @throws SQLException
// */
//private Boolean preparationChamps(	PreparedStatement updateClient, 
//									Entry<EnumChampsTableClient,String> champAMettreAJour,
//									Boolean existingField , EnumChampsTableClient champ) 
//											throws SQLException {
//	if(champAMettreAJour.getKey().equals(champ)){
//		updateClient.setString(1, champAMettreAJour.getValue()); 
//		existingField = true;
//	}
//	return existingField;
//}








//// METHODE QUI AFFICHE LES MESSAGE L'UTILISATEUR
//private void afficheMsgUtilisateur(String messageUtilisateur, String tpyeErreur , int typeIcone) {
//	JOptionPane.showMessageDialog(null,
//			messageUtilisateur,
//			tpyeErreur, 
//			typeIcone);
//	messageUtilisateur = "";
//}