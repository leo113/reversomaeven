package fr.leo.utilitaire;

import java.sql.SQLException;
import java.util.ArrayList;

import fr.leo.dao.DaoContrat;
import fr.leo.exceptions.ExceptionDao;
import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.metier.Contrat;

public class ListeContrats {
	
	/**
	 * Classe permettant de gerer un liste de Contrats
	 * @see Contrat
	 */
	
	// ATTRIBUTS D'INSTANCE
	private ArrayList<Contrat> listContrats = new ArrayList<>();
				
	// CONSTRUCTEUR
	public ListeContrats() {
		try {
			this.listContrats = new DaoContrat().findAll();
		} catch (ExceptionDao | ExceptionPersonnalisee | SQLException e) {
		}
	}
	
	// GETTERS ET SETTERS	
	
	/**
	 * Retourne une liste triee selon la RaisonSociale
	 * @return listSocietes ArrayList
	 */
	public ArrayList<Contrat> getListContrats() {
		// Collections.sort(this.listContrats, new ComparatorSociete());		
		return listContrats;
	}

	
					// *********** METHODES ***********	
	// SUPPRIMER_CONTRAT
	/**
	 * Supprime un contrat de la liste.	
	 * Retourne une exception de type ExceptionPersonnalisee si la liste 
	 *  passe en parametre a pour valeur 'null'
	 * @param contrat  Contrat
	 * @return listeContrat ArrayList 
	 * @throws ExceptionPersonnalisee  exceptionPersonnalisee
	 * @see ExceptionPersonnalisee
	 */
	public ArrayList<Contrat> supprimerContrat(Contrat contrat) throws ExceptionPersonnalisee {
		if (contrat != null) {
			if (listContrats.contains(contrat)) {
				listContrats.remove(contrat);
				return listContrats;
			}			
		}else{
			throw new ExceptionPersonnalisee("Impossible de supprimer une societe null");
		}
		return null;
	}
	
	// CHERCHER_CONTRAT
	/**
	 * Cherche un contrat dans la liste.
	 * Retourne l'index du contrat passe en parametre dans la liste, si il est presente dans la liste.
	 * Retourne -1 si le contrat ne figure pas dans la liste.
	 * @param contrat Contrat 
	 * @return int index de la Contrat 
	 */
	public int chercherContrat(Contrat _contrat){
		int indexListeContrat = listContrats.indexOf(_contrat);		
		return indexListeContrat;
	}
	
	// CHERCHER_CONTRAT PAR IDENTIFIANT
	/**
	 * Cherche un contrat dans la liste.
	 * Retourne la reference au contrat dans la liste, si il est present dans la liste.
	 * Retourne null si le contrat ne figure pas dans la liste.
	 * @param  IdContrat int 
	 * @return Contrat contratAChercher
	 */
	public Contrat chercherContratParIdentifant(int idContrat){
		for (Contrat contrat : listContrats) {
			int idContratTemp = (int) contrat.getIdContrat() ;
			if ( idContratTemp == idContrat ) {
				return contrat;
			}
		}		
		return null;
	}
	
	// AJOUTER_CONTRAT
	/**
	 * Ajoute un contrat a la liste des contrats.
	 * Envoit une exception de type ExceptionPersonnalisee si la
	 *  liste passee en parametre a pour valeur 'null' 
	 *  ou si la liste passee en parametre est deja presente dans la liste
	 * @param contrat  Contrat
	 * @return listeContrat  ArrayList 
	 * @throws ExceptionPersonnalisee  exceptionPersonnalisee
	 * @see ExceptionPersonnalisee
	 */
	public ArrayList<Contrat> ajouterContrat(Contrat contrat) throws  ExceptionPersonnalisee {
		if (contrat != null) {
			if (this.listContrats.contains(contrat)) {
				throw new ExceptionPersonnalisee("Impossible d'ajouter dans la liste un contrat deja present dans la liste");
			}else{
				listContrats.add(contrat);
			}
		}else{
			throw new ExceptionPersonnalisee("Impossible d'ajouter dans la liste un contrat avec la valeur Null");
		}
		return listContrats;
	}	
}
