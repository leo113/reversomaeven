package fr.leo.utilitaire;

import java.util.Comparator;

import fr.leo.metier.Societe;

public class ComparatorSociete implements Comparator<Societe> {
	
	/**
	 * Classe permettant de comparer 2 Societes
	 */

	@Override
	public int compare(Societe s1, Societe s2) {
		return s1.getRaisonSociale().compareTo(s2.getRaisonSociale()) ;
	}
	
}