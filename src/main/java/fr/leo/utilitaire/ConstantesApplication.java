package fr.leo.utilitaire;

public abstract class ConstantesApplication {
	
	// LISTE DES CONSTANTES DE L'APPLICATION
	public static final String TYPE_CLIENT = "client";
	public static final String TYPE_PROSPECT = "prospect";
	
	public static final String CREATION_SOCIETE = "creationSociete";
	public static final String MODIFICATION_SOCIETE = "modificationSociete";
	public static final String SUPPRESSION_SOCIETE = "suppressionSociete";
	
	public static final String CREATION_CONTRAT = "creationContrat";
	public static final String MODIFICATION_CONTRAT = "modificationContrat";
	public static final String SUPPRESSION_CONTRAT = "suppressionContrat";

}
