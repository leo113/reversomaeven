package fr.leo.utilitaire;

import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

public abstract class Utilitaire {
	
	public static final DateTimeFormatter FOMATTER_DATE = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	public static final DateTimeFormatter FOMATTER_DATE_BDD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	public static final Pattern PATTERN_MAIL = Pattern.compile("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b");
	
	public static final Pattern PATTERN_TEL = Pattern.compile("(0|\\\\+33|0033)[1-9][0-9]{10}");
	
	// private static Logger logger = LogManager.getLogger(Client.class);
}
