package fr.leo.utilitaire;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import fr.leo.dao.DaoSociete;
import fr.leo.exceptions.ExceptionChampObligatoire;
import fr.leo.exceptions.ExceptionDao;
import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.metier.Societe;

public class ListeSocietes {
	
	/**
	 * Classe permettant de gerer un liste de Societe
	 * @see Societe
	 */

	// ATTRIBUTS DE CLASSE
	private static int nombreDeSocietes = 0;
	
	// ATTRIBUTS D'INSTANCE
	private ArrayList<Societe> listSocietes = new ArrayList<>();
				
	// CONSTRUCTEUR
	public ListeSocietes() {
		try {
			this.listSocietes = new DaoSociete().findAll();
		} catch (ExceptionDao | ExceptionPersonnalisee | ExceptionChampObligatoire | SQLException e) {
		}
	}
	
	// GETTERS ET SETTERS		
	/**
	 * Retourne une liste triee selon la RaisonSociale
	 * @return listSocietes ArrayList
	 */
	public ArrayList<Societe> getListSocietes() {
		Collections.sort(this.listSocietes, new ComparatorSociete());		
		return listSocietes;
	}	
	public static int getNombreDeSocietes() {
		return nombreDeSocietes;
	}
	private static void setNombreDeSocietes(int nombreDeSocietes) {
		ListeSocietes.nombreDeSocietes = nombreDeSocietes;
	}
	
					// *********** METHODES ***********	
	// SUPPRIMER_SOCIETE
	/**
	 * Supprime une societe de la liste.	
	 * Retourne une exception de type ExceptionPersonnalisee si la liste 
	 *  passe en parametre a pour valeur 'null'
	 * @param societe  Societe
	 * @return listeSociete ArrayList 
	 * @throws ExceptionPersonnalisee  exceptionPersonnalisee
	 * @see ExceptionPersonnalisee
	 */
	public ArrayList<Societe> supprimerSociete(Societe societe) throws ExceptionPersonnalisee {
		if (societe != null) {
			if (listSocietes.contains(societe)) {
				listSocietes.remove(societe);
				ListeSocietes.setNombreDeSocietes(--ListeSocietes.nombreDeSocietes);
				return listSocietes;
			}			
		}else{
			throw new ExceptionPersonnalisee("Impossible de supprimer une societe null");
		}
		return null;
	}
	
	// CHERCHER_SOCIETE
	/**
	 * Cherche une societe dans la liste.
	 * Retourne l'index de la societe passe en parametre dans la liste, si il est presente dans la liste.
	 * Retourne -1 si la societe ne figure pas dans la liste.
	 * @param _societe Societe 
	 * @return int index de la Societe 
	 */
	public int chercherSociete(Societe _societe){
		int indexListeSociete = listSocietes.indexOf(_societe);		
		return indexListeSociete;
	}
	
	// CHERCHER_SOCIETE PAR IDENTIFIANT
	/**
	 * Cherche une societe dans la liste.
	 * Retourne la reference a la societe dans la liste, si il est presente dans la liste.
	 * Retourne null si la societe ne figure pas dans la liste.
	 * @param  Idsociete int 
	 * @return Societe societeAChercher
	 */
	public Societe chercherSocieteParIdentifant(int Idsociete){
		for (Societe societe : listSocietes) {
			int idSocieteTemp = (int) societe.getIdSociete() ;
			if ( idSocieteTemp == Idsociete ) {
				return societe;
			}
		}		
		return null;
	}
	
	// CHERCHER_SOCIETE PAR RAISON SOCIALE
	/**
	 * Cherche une societe dans la liste.
	 * Retourne la reference a la societe dans la liste, si il est presente dans la liste.
	 * Retourne null si la societe ne figure pas dans la liste.
	 * @param  raisonSociale String 
	 * @return Societe societeAChercher
	 */
	public Societe chercherSocieteParRaisonSociale(String raisonSociale){
		for (Societe societe : listSocietes) {
			if (societe.getRaisonSociale().equalsIgnoreCase(raisonSociale)) {
				return societe;
			}
		}		
		return null;
	}
	

	// AJOUTER_SOCIETE
	/**
	 * Ajoute une societe a la liste des societe.
	 * Envoit une exception de type ExceptionPersonnalisee si la
	 *  liste passee en parametre a pour valeur 'null' 
	 *  ou si la liste passee en parametre est deja presente dans la liste
	 * @param societe  Societe
	 * @return listeSociete  ArrayList 
	 * @throws ExceptionPersonnalisee  exceptionPersonnalisee
	 * @see ExceptionPersonnalisee
	 */
	public ArrayList<Societe> ajouterSociete(Societe societe) throws  ExceptionPersonnalisee {
		if (societe != null) {
			if (this.listSocietes.contains(societe)) {
				throw new ExceptionPersonnalisee("Impossible d'ajouter dans la liste une societe deja presente dans la liste");
			}else{
			listSocietes.add(societe);
			ListeSocietes.setNombreDeSocietes(++ListeSocietes.nombreDeSocietes);
			}
		}else{
			throw new ExceptionPersonnalisee("Impossible d'ajouter dans la liste une societe avec la valeur Null");
		}
		return listSocietes;
	}
	
	// AJOUTER PLUSIEURS SOCIETES
	/**
	 * Transfert une les objets de type 'Societes' d'une ArrayList<Societe>,  dans un objet de type ListeSocietes 
	 * @param listeSocieteAAjouter ArrayList<Societe>
	 * @param listeSocietes ListeSocietes
	 * @return ListeSocietes
	 * @throws ExceptionPersonnalisee
	 * Renvoit une ExceptionPersonnalisee si une des 2 listes est vide.
	 */
	public ListeSocietes transfertLesSocietesInBddToListeSocietes(ArrayList<? extends Societe> listeSocieteAAjouter ) throws  ExceptionPersonnalisee {
		if (listeSocieteAAjouter != null && this != null ) {
			for (Societe societe : listeSocieteAAjouter) {
				this.ajouterSociete(societe);
			}
		}else{
			throw new ExceptionPersonnalisee("Impossible d'ajouter la liste de societes avec la valeur Null");
		}
		return this;
	}
	
}
