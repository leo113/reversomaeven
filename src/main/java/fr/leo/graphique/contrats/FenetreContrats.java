package fr.leo.graphique.contrats;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;

import fr.leo.controllers.ControllerFormulaire;
import fr.leo.dao.DaoContrat;
import fr.leo.exceptions.ExceptionDao;
import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.graphique.FenetreAccueil;
import fr.leo.metier.Client;
import fr.leo.metier.Contrat;
import fr.leo.metier.Societe;
import fr.leo.utilitaire.ListeSocietes;

public class FenetreContrats extends JFrame {
	
	/**
	 * Classe qui afiche les Contrats d'un Client
	 */

	// CONSTANTES
	private static final long serialVersionUID = 1L;

	// ATTRIBUTS D'INSTANCE
	private DefaultTableModel listData; 	// Modele pour la JTable listant les Clients/Prospects
	private ListeSocietes listeSocietes;
	private JLabel lblTitre;
	private FenetreAccueil fenetreAccueil;
	private JComboBox<Societe> comboBxSocietes; 
	private JScrollPane jScrollPaneSocietes = new JScrollPane(); 
	private JPanel pnlContrats = new JPanel();
	private JTable jtbl_ListeDesContrats;
	private JTextField txtFldMontantTotal;
	private JLabel lblListeContrats ;

				// 	***********************  CONSTRUCTEUR	************************
	/**
	 * 
	 * @param  _listeSocietes ListeSocietes
	 * @param  _fenetreAccueil FenetreAccueil
	 * @param  _typeCrud FenetreAccueil	type d operation Crud
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public FenetreContrats(FenetreAccueil _fenetreAccueil) {

		// LOOK AND FEEL : NIMBUS
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

		// AFFECTATION VALEURS ATTRIBUTS D'INSTANCE
		this.fenetreAccueil = _fenetreAccueil;	 						 // reference vers la fenetre Accueil
		this.listeSocietes = ControllerFormulaire.getListeSociete();	 // reference vers la liste des societes
		

		// CARACTERISTIQUES GENERALES DE LA JFRAME
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 190, 1200, 570);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		this.setVisible(true);

		// JPANEL DE LA FRAME		
		pnlContrats.setBounds(0, 0, 1184, 531);
		getContentPane().add(pnlContrats);
		pnlContrats.setLayout(null);
		
						// LES COMPOSANTS DU PANEL: 
		// LABEL "TITRE"
		lblTitre = new JLabel("CONTRATS :");
		lblTitre.setFont(new Font("Dark Tales", Font.BOLD, 30));
		lblTitre.setBounds(435, 31, 469, 31);
		pnlContrats.add(lblTitre);
		// LABEL "MONTANT TOTAL"
		JLabel lblMontantTotal = new JLabel("Montant Total :");
		lblMontantTotal.setBounds(39, 94, 84, 24);
		pnlContrats.add(lblMontantTotal);
		// TEXT "MONTANT TOTAL"
		txtFldMontantTotal = new JTextField();
		txtFldMontantTotal.setBounds(133, 91, 86, 24);
		pnlContrats.add(txtFldMontantTotal);
		txtFldMontantTotal.setColumns(10);
		// LABEL "LISTE DES CONTRATS"
		lblListeContrats = new JLabel("");
		lblListeContrats.setFont(new Font("Tahoma", Font.BOLD, 19));
		lblListeContrats.setBounds(39, 329, 518, 24);
		pnlContrats.add(lblListeContrats);
		// BOUTON "QUITTER"
		JButton btnQuitter = new JButton("QUITTER APP");
		btnQuitter.setBounds(924, 227, 222, 24);
		pnlContrats.add(btnQuitter);
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		// BOUTON "RETOUR_MENU"
		JButton btnRetourMenu = new JButton("RETOUR MENU");
		btnRetourMenu.setBounds(924, 173, 222, 24);
		pnlContrats.add(btnRetourMenu);
		FenetreContrats _this = this;
		btnRetourMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_this.dispose();
				fenetreAccueil.setVisible(true);
			}
		});
		
		// BOUTON "GESTION DES CONTRATS"
		JButton btnGestionContrats = new JButton("GESTION DES CONTRATS");
		btnGestionContrats.setBackground(new Color(102, 204, 153));
		btnGestionContrats.setBounds(924, 112, 222, 23);
		pnlContrats.add(btnGestionContrats);
		btnGestionContrats.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_this.dispose();
				new FenetreGestionContrats(_this).setVisible(true);
			}
		});
		
						// FIN - LES COMPOSANTS DU PANEL: 

		// INSTANCIATION ET ALIMENTATION DE LA COMBO_BOX LISTANT LES CLIENTS : 
		String[] data = remplirComboBox(listeSocietes); 
		comboBxSocietes = new JComboBox(data);
		comboBxSocietes.setBounds(450, 94, 153, 157);
		pnlContrats.add(comboBxSocietes);
		comboBxSocietes.addItemListener(new ItemListenerComboBoxSociete());			
	}
				// 	***********************  FIN CONSTRUCTEUR	************************
	
	

				// 	***********************  GETTERS ET SETTERS	************************
	protected JLabel getLblTitre() {
		return lblTitre;
	}
	protected void setLblTitre(JLabel lblTitre) {
		this.lblTitre = lblTitre;
	}
	public JComboBox<Societe> getComboBxSocietes() {
		return comboBxSocietes;
	}
	public void setComboBxSocietes(JComboBox<Societe> comboBxSocietes) {
		this.comboBxSocietes = comboBxSocietes;
	}
				// 	*********************** FIN GETTERS ET SETTERS	************************
	
		
	
				// 	*********************** TRAITEMENT COMBO_BOX CLIENTS	************************	

	/*
	 * Methode qui prepare les donnees pour alimenter la ComBoBoxSociete.
	 * Retourne un tableau de String : {"5 Chloris", "2 Eoles", "1 FleurAuDent", ...}
	 */
	@SuppressWarnings("unused")
	private String[] remplirComboBox(ListeSocietes listeSocietes) {
		ArrayList<Societe> listeTemp = listeSocietes.getListSocietes();
		String stringTemp = "";
		for (Societe societe : listeTemp) {
			if (societe instanceof Client) {
				//  Selection champs "idSociete" et "RaisonSociale"
				stringTemp = stringTemp + "::" + societe.getIdSociete() + " " + societe.getRaisonSociale();
			}
		}
		String[] tabTempStrings = { "" };
		return tabTempStrings = stringTemp.split("::");
	}

	/*
	* Listener de la ComboBox Societe
	* Alimente la JTable avec les Contrats du CLient qui a ete selectionne
	*/
	class ItemListenerComboBoxSociete implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent e) {
			String societeSelectionnee = ((JComboBox<?>) e.getSource()).getSelectedItem().toString();
			String[] tabTemp = societeSelectionnee.split(" ");
			String strIdClient = tabTemp[0]; // Recupere l'IdSociete du Client
			Integer intIdClient = Integer.parseInt(strIdClient);
			
			// LA JTABLE EST INSEREE DANS UN JSCROLLPANE
			jScrollPaneSocietes = remplirJTable(listeSocietes, intIdClient);	
			// AJOUT DU JSCROLLPANE AU CONTENT_PANE
			pnlContrats.add(jScrollPaneSocietes);
		}
	}
				// 	*********************** FIN METHODES COMBO_BOX CLIENTS	************************	
	
	
	
	
				// 	*********************** TRAITEMENT JTABLE CONTRATS       ************************	
	
	/**
	 * Methode qui remplit une JTable avec des Contrats
	 * Retourne un JSrcollPane englobant la JTable.
	 * @param Integer intIdClient 
	 * @param ListeSocietes  listeSocietes
	 * @return JScrollPane jScrollPane
	 */
	private JScrollPane remplirJTable(ListeSocietes listeSocietes , Integer intIdClient) {

		// CREATION DU MODELE ET AJOUT DE LA LIGNE DES ENTETES DE LA JTABLE
		String[] cols = Contrat.getListeDesDonneesGerees();
		listData = new DefaultTableModel(cols, 0);

		// INSTANCIATION DE LA JTABLE ET INSERTION DANS UN J_SCROLL_PANE
		jtbl_ListeDesContrats = new JTable(listData);
		jtbl_ListeDesContrats.setEnabled(false);
		JScrollPane jscpContrats = new JScrollPane();
		jscpContrats.setLocation(0, 379);
		jscpContrats.setSize(1184, 262);
		jscpContrats.setViewportView(jtbl_ListeDesContrats);
		
		// AJOUT DES ENTREES AU MODELE :
		// On recherche le client dans la listeDesSocietes
		Client c1 =   (Client) listeSocietes.chercherSocieteParIdentifant(intIdClient);	
		
		// Recherche de tous les contrats pour ce client
		ArrayList<Contrat> listeContrats = null;
		try {
			listeContrats = new DaoContrat().findAll(c1.getIdSociete());
		} catch (SQLException | ExceptionDao | ExceptionPersonnalisee e) {
			e.printStackTrace();
		}
		
		// On recupere les valeurs de chaque contrats du client
		Double montantTotalContrats = 0.0;
		DecimalFormat df = new DecimalFormat("0.00");	
		for (Contrat contrat : listeContrats) {
			String stringChamps =    contrat.getIdContrat() + "::"
									 + contrat.getLibelleContrat() + "::" + contrat.getMontantContrat() + "::"
									 + contrat.getDateDebutContrat() + "::" + contrat.getDateFinContrat()
									 + "::" + contrat.getIdSociete() ;			
			//String stringCopieChamps = stringChamps ;
			String[] tab = stringChamps.split("::");
			listData.addRow(tab);
			
			// On met a jour le champ : "Montant Total"
			montantTotalContrats = montantTotalContrats + contrat.getMontantContrat();
			txtFldMontantTotal.setText( df.format(montantTotalContrats) + "" );	
		}
		
		// On met a jour le libelle de la fenetre
		lblListeContrats.setText("LISTE DES CONTRATS POUR LE CLIENT : "  
									+ listeSocietes.chercherSocieteParIdentifant(intIdClient).getRaisonSociale());
		
		return jscpContrats; // retourne un JscrollPane contenant la Jtable qui liste les Contrats
	}
}
