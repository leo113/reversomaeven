package fr.leo.graphique.contrats;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;

import fr.leo.metier.Contrat;
import fr.leo.utilitaire.ListeContrats;

public class FenetreAffichageContrats extends JFrame {
	
	/**
	 * Classe qui gere l'affichage de tous les Contrats
	 */

	// CONSTANTES D'INSTANCE
	private static final long serialVersionUID = 1L;

	// ATTRIBUTS D'INSTANCE
	private DefaultTableModel listData; // Modele pour la JTable listant les Contrats
	@SuppressWarnings("unused")
	private FenetreGestionContrats fenetreGestionContrats;  //Reference vers la FenetreGestionContrats
    @SuppressWarnings("unused")
	private ListeContrats listeContrats;
	
	// CONSTRUCTEUR
	public FenetreAffichageContrats(ListeContrats listeContrats, FenetreGestionContrats fenetreGestionContrats) {

		// LookAndFell : Nimbus
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

		// INITIALISATION DES ATTRIBUTS D'INSTANCE
		this.fenetreGestionContrats = fenetreGestionContrats;
		this.listeContrats = listeContrats;

		// INTIALISATION DES CARACTERISTIQUES GENERALES DE LA JFRAME
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 190, 1200, 570);
		this.setLocationRelativeTo(null);
		setTitle("LISTE ");
		getContentPane().setLayout(null);

		// INITIALISATION DU JPANEL
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1184, 1);
		getContentPane().add(panel);
		panel.setLayout(null);

		// BOUTON RETOUR ACCUEIL : retour vers la fenetre 'Accueil'
		JButton btnRetourMenu = new JButton("RETOUR ACCUEIL");
		btnRetourMenu.setBounds(375, 425, 152, 24);
		this.getContentPane().add(btnRetourMenu);
		FenetreAffichageContrats _this = this;
		btnRetourMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_this.dispose();
				fenetreGestionContrats.setVisible(true);
			}
		});

		// BOUTON QUITTER APPLICATION : permet de quitter l'application
		JButton btnQuitterApplication = new JButton("QUITTER APPLICATION");
		btnQuitterApplication.setBounds(603, 425, 208, 24);
		getContentPane().add(btnQuitterApplication);
		btnQuitterApplication.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});

		// JLABEL TITRE DE LA FENETRE : 
		JLabel lblTitreAffichage = new JLabel();
		getContentPane().add(lblTitreAffichage);
		lblTitreAffichage.setFont(new Font("Dark Tales", Font.BOLD, 30));
		lblTitreAffichage.setBounds(541, 21, 423, 32);

		// INSTANCIATION ET ALIMENTATION DE LA JTABLE 'CONTRATS'
		// LA JTABLE EST INSEREE DANS UN JSCROLLPANE
		JScrollPane jScrollPaneSocietes = remplirJTable(listeContrats);

		// AJOUT DU JSCROLLPANE AU CONTENT_PANE
		getContentPane().add(jScrollPaneSocietes);
		
	} // FIN CONSTRUCTEUR

	// METHODE REMPLIR_JTABLE :
	/**
	 * Methode qui remplit une JTable soit avec Contrats.
	 * Retourne un JSrcollPane englobant la JTable.
	 * @param ListeContrats  listeContrats
	 * @return JScrollPane jScrollPane
	 */
	private JScrollPane remplirJTable(ListeContrats listeContrats) {

		// CREATION DU MODELE ET AJOUT DE LA LIGNE DES ENTETES DE LA JTABLE
		String[] cols = null;
			cols = Contrat.getListeDesDonneesGerees();
		listData = new DefaultTableModel(cols, 0);

		// INSTANCIATION DE LA JTABLE ET INSERTION DANS UN J_SCROLL_PANE
		JTable jtbl_ListeDesContrats = new JTable(listData);
		jtbl_ListeDesContrats.setEnabled(false);
		JScrollPane panTable = new JScrollPane();
		panTable.setLocation(20, 65);
		panTable.setViewportView(jtbl_ListeDesContrats);
		panTable.setSize(1141, 262);

		// AJOUT DES ENTREES AU MODELE :
		ArrayList<Contrat> listeTemp = listeContrats.getListContrats();		
		for (Contrat contrat : listeTemp) {
			String stringChampsCommuns =	contrat.getIdContrat()+ "::" 
											+ contrat.getLibelleContrat() + "::"
											+ contrat.getMontantContrat()+ "::"
											+ contrat.getDateDebutContrat() + "::"
											+ contrat.getDateFinContrat() + "::"
											+ contrat.getIdSociete();
					String[] tabTempStrings = stringChampsCommuns.split("::");
					listData.addRow(tabTempStrings);
		}
		return panTable; // retourne un JscrollPane contenant la Jtable qui liste les Contrats
	}
}
