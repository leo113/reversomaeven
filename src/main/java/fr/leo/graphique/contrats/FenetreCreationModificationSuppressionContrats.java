package fr.leo.graphique.contrats;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.time.LocalDate;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fr.leo.dao.DaoContrat;
import fr.leo.dao.DaoSociete;
import fr.leo.exceptions.ExceptionChampObligatoire;
import fr.leo.exceptions.ExceptionDao;
import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.metier.Client;
import fr.leo.metier.Contrat;
import fr.leo.metier.Societe;
import fr.leo.utilitaire.ConstantesApplication;
import fr.leo.utilitaire.ListeContrats;
import fr.leo.utilitaire.ListeSocietes;

public class FenetreCreationModificationSuppressionContrats extends JFrame {

	/**
	 * Classe qui gere la Modification, la Suppression et la MiseAJour d'un Contrat
	 */

	// CONSTANTES
	private static final long serialVersionUID = 1L;
	private ListeContrats listeContrats;
	private JTextField txtMontantContrat;
	private JTextField txtLibelle;
	private JTextField txtDateDebutContrat;
	private JTextField txtDateFinContrat;
	private JTextField txtClient;
	private String typeSociete;
	private JLabel lblTitre;
	private JLabel lblDateFinContrat;
	private JLabel lblClient;
	@SuppressWarnings("unused")
	private FenetreGestionContrats fenetreGestionContrats;
	private JComboBox<Societe> comboBxSocietes; // ComboBox listant les Clients/Prospects
	private JButton btnValiderCreation;
	private JButton btnSuppression;
	private JButton btnModification;
	private String typeCrud;
	private int idClient;

	// 	*****       		         CONSTRUCTEUR			 	     *******

	/**
	 * 
	 * @param _listeContrats
	 * @param _fenetreGestonContrats
	 */
	public FenetreCreationModificationSuppressionContrats(
									ListeContrats _listeContrats,
									FenetreGestionContrats _fenetreGestionContrats,
									String _typeCrud) {

		// Look and feel : Nimbus
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}
		
		// AFFECTATION VALEURS ATTRIBUTS D'INSTANCE
		this.fenetreGestionContrats = _fenetreGestionContrats;	 			 // reference vers la fenetre Accueil
		this.listeContrats = _listeContrats; 				 // reference vers la liste des societes
		this.typeCrud = _typeCrud;	
		
		// CARACTERISTIQUES GENERALES DE LA JFRAME
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 190, 1200, 570);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		// JPANEL DE LA FRAME
		JPanel pnlCreationContrat = new JPanel();
		pnlCreationContrat.setBounds(0, 0, 1184, 531);
		getContentPane().add(pnlCreationContrat);
		pnlCreationContrat.setLayout(null);
		
		// *****        	 	  LES COMPOSANTS DU PANEL           	   *****

		lblTitre = new JLabel("TITRE : ");
		lblTitre.setFont(new Font("Dark Tales", Font.BOLD, 30));
		lblTitre.setBounds(435, 31, 469, 31);
		pnlCreationContrat.add(lblTitre);

		txtLibelle = new JTextField();
		txtLibelle.setColumns(10);
		txtLibelle.setBounds(295, 99, 152, 24);
		pnlCreationContrat.add(txtLibelle);	
		JLabel lblLibelle = new JLabel("Libelle :");
		lblLibelle.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblLibelle.setBounds(167, 99, 99, 24);
		pnlCreationContrat.add(lblLibelle);
		
		txtMontantContrat = new JTextField();
		txtMontantContrat.setColumns(10);
		txtMontantContrat.setBounds(295, 145, 152, 24);
		pnlCreationContrat.add(txtMontantContrat);	
		JLabel lblMontantContrat = new JLabel("Montant :");
		lblMontantContrat.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblMontantContrat.setBounds(167, 145, 99, 24);
		pnlCreationContrat.add(lblMontantContrat);

		JLabel lblDateDebutContrat = new JLabel("Date Début :");
		lblDateDebutContrat.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDateDebutContrat.setBounds(167, 191, 99, 24);
		pnlCreationContrat.add(lblDateDebutContrat);
		txtDateDebutContrat = new JTextField();
		txtDateDebutContrat.setColumns(10);
		txtDateDebutContrat.setBounds(295, 191, 152, 24);
		pnlCreationContrat.add(txtDateDebutContrat);		

		lblDateFinContrat = new JLabel("Date Fin :");
		lblDateFinContrat.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDateFinContrat.setBounds(167, 240, 114, 24);
		pnlCreationContrat.add(lblDateFinContrat);
		txtDateFinContrat = new JTextField();
		txtDateFinContrat.setColumns(10);
		txtDateFinContrat.setBounds(295, 240, 152, 24);
		pnlCreationContrat.add(txtDateFinContrat);

		lblClient = new JLabel("Client :");
		lblClient.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblClient.setBounds(167, 287, 99, 24);
		pnlCreationContrat.add(lblClient);
		txtClient = new JTextField();
		txtClient.setColumns(10);
		txtClient.setBounds(295, 287, 152, 24);
		pnlCreationContrat.add(txtClient);

		// BOUTON "CREER" : appel a la methode creerSociete()
		btnValiderCreation = new JButton("CREER");
		btnValiderCreation.setBackground(new Color(95, 158, 160));
		btnValiderCreation.setBounds(939, 99, 152, 24);
		pnlCreationContrat.add(btnValiderCreation);
		lblTitre.setText("CREATION CONTRAT :");
		btnValiderCreation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					creerContrat();
				} catch (ExceptionPersonnalisee e) {
					JOptionPane.showMessageDialog(null, e.getMessage(), "Creation", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		// BOUTON "SUPPRIMER" : appel a la methode supprimerContrat()
		btnSuppression = new JButton("SUPPRIMER");
		btnSuppression.setBackground(new Color(95, 158, 160));
		btnSuppression.setBounds(939, 99, 152, 24);
		pnlCreationContrat.add(btnSuppression);
		btnSuppression.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//supprimerContrat();
			}
		});

		// BOUTON "MODIFER" : appel a la methode modiferSociete()
		btnModification = new JButton("MODIFER");
		btnModification.setBackground(new Color(95, 158, 160));
		btnModification.setBounds(939, 99, 152, 24);
		pnlCreationContrat.add(btnModification);
		btnModification.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
//				try {
//					//modiferSociete();
//				} catch (ExceptionPersonnalisee e) {
//					JOptionPane.showMessageDialog(null, e.getMessage(), "Modifcation", JOptionPane.WARNING_MESSAGE);
//				}
			}
		});

		// BOUTON "QUITTER"
		JButton btnQuitter = new JButton("QUITTER APP");
		btnQuitter.setBounds(939, 191, 152, 24);
		pnlCreationContrat.add(btnQuitter);
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});

		// BOUTON "RETOUR_MENU"
		JButton btnRetourMenu = new JButton("RETOUR MENU");
		btnRetourMenu.setBounds(939, 145, 152, 24);
		pnlCreationContrat.add(btnRetourMenu);
		FenetreCreationModificationSuppressionContrats _this = this;
		btnRetourMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_this.dispose();
				_fenetreGestionContrats.setVisible(true);
			}
		});

		// INSTANCIATION ET ALIMENTATION DE LA COMBO_BOX LISTANT LES "PROSPECT/CLIENT" : 
		//String[] data = remplirComboBox(listeContrats, this.typeSociete);
		//comboBxSocietes = new JComboBox(data);
		//comboBxSocietes.setBounds(938, 237, 153, 206);
		//pnlCreationContrat.add(comboBxSocietes);
		//comboBxSocietes.addItemListener(new ItemListenerComboBoxSociete());

		// SETTINGS DE LA JFRAME EN FONCTION DE L'OPERATION CRUD REQUISE :
		//  permet de personaliser la fenetre en fonction de l operation CRUD requise,
		//  les labels sont personnalises, certains composants affiches ou non.
		if (typeCrud.equals(ConstantesApplication.CREATION_SOCIETE)) {
			initialisationSettingsCreationSociete(_fenetreGestionContrats);
		}
		if (typeCrud.equals(ConstantesApplication.SUPPRESSION_SOCIETE)) {
			initialisationSettingsSuppressionSociete(_fenetreGestionContrats);
		}
		if (typeCrud.equals(ConstantesApplication.MODIFICATION_SOCIETE)) {
			initialisationSettingsModificationSociete(_fenetreGestionContrats);
		}
	}
	
	

	// 	*****        		  	   		   GETTERS ET SETTERS      						 *******
	
	
	protected JLabel getLblTitre() {
		return lblTitre;
	}
	public JTextField getTxtMontantContrat() {
		return txtMontantContrat;
	}
	public void setTxtMontantContrat(JTextField txtMontantContrat) {
		this.txtMontantContrat = txtMontantContrat;
	}
	public JTextField getTxtLibelle() {
		return txtLibelle;
	}

	public void setTxtLibelle(JTextField txtLibelle) {
		this.txtLibelle = txtLibelle;
	}

	public JTextField getTxtDateDebutContrat() {
		return txtDateDebutContrat;
	}

	public void setTxtDateDebutContrat(JTextField txtDateDebutContrat) {
		this.txtDateDebutContrat = txtDateDebutContrat;
	}
	public JTextField getTxtDateFinContrat() {
		return txtDateFinContrat;
	}

	public void setTxtDateFinContrat(JTextField txtDateFinContrat) {
		this.txtDateFinContrat = txtDateFinContrat;
	}

	public JTextField getTxtClient() {
		return txtClient;
	}

	public void setTxtClient(JTextField txtClient) {
		this.txtClient = txtClient;
	}
	protected void setLblTitre(JLabel lblTitre) {
		this.lblTitre = lblTitre;
	}
	protected String getTypeSociete() {
		return typeSociete;
	}
	protected void setTypeSociete(String typeSociete) {
		this.typeSociete = typeSociete;
	}
	public JComboBox<Societe> getComboBxSocietes() {
		return comboBxSocietes;
	}
	public void setComboBxSocietes(JComboBox<Societe> comboBxSocietes) {
		this.comboBxSocietes = comboBxSocietes;
	}

	

	
	// 	*****              		  SETTINGS DE LA FENETRE EN FONCTION DU MODE CRUD  REQUIS     				  *******
	
	// MODE CRUD : CREATION
	private void initialisationSettingsCreationSociete(FenetreGestionContrats _fenetreGestionContrats) {
		
		setTitle("CREATION");
		this.getLblTitre().setText("CREER " + typeSociete.toUpperCase() + " : ");
		this.getComboBxSocietes().setVisible(false);
		this.setVisible(true);
	}

	// MODE CRUD : SUPPRESSION
	private void initialisationSettingsSuppressionSociete(FenetreGestionContrats _fenetreGestionContrats) {
		setTitle("SUPPRESSION");
		btnValiderCreation.setVisible(false);
		this.getLblTitre().setText("SUPPRIMER " + typeSociete.toUpperCase() + " : ");
		
		// Rendre les champs du formulaire non modifiables
		lblTitre.setEnabled(false);
		txtLibelle.setEnabled(false);
		txtMontantContrat.setEnabled(false);
		txtDateDebutContrat.setEnabled(false);
		txtDateFinContrat.setEnabled(false);
		txtClient.setEnabled(false);
		
		this.setVisible(true);
	}

	// MODE CRUD : MODIFICATION
	private void initialisationSettingsModificationSociete(FenetreGestionContrats _fenetreGestionContrats) {		
		setTitle("MODIFICATION");
		btnValiderCreation.setVisible(false);
		btnSuppression.setVisible(false);
		btnModification.setVisible(true);
		this.getLblTitre().setText("MODIFIER " + typeSociete.toUpperCase() + " : ");
		this.setVisible(true);
	}
	

	// 	*****         				     METHODES MODIFIER(), SUPPRIMER() ET CREER() 						   *******
	

//	// METHODE MODIFIER_SOCIETE :
//	/*
//	 * Methode permettant la modification d'une Societe ( un Client ou un Prospect )
//	 */
//	private void modiferSociete() throws ExceptionPersonnalisee {
//
//		// VARIABLES LOCALES
//		String strRaisonSociale;
//		EnumDomaine enumDomaine;
//		String strNumRue;
//		String strNomRue;
//		String strCodePostal;
//		String strVille;
//		String strTelephone;
//		String strEmail;
//		double dblChiffreAffaire;
//		int intNbrEmployes;
//		String strCommentaire = "";
//		int intIdSociete;
//		String strDateProspection;
//
//		// RECUPERATION DES VALEURS COMMUNES AUX CLIENTS ET AUX PROSPECTS, PROVENANT DU FORMULAIRE :
//		strRaisonSociale = txtRaisonSociale.getText();
//		int intDomaine = comboBxDomaine.getSelectedIndex();
//		enumDomaine = intDomaine == 1 ? EnumDomaine.PRIVE : EnumDomaine.PUBLIC;
//		strNumRue = txtNumeroRue.getText();
//		strNomRue = txtNomRue.getText();
//		strCodePostal = txtCodePostal.getText();
//		strVille = txtVille.getText();
//		strTelephone = txtTelephone.getText();
//		strEmail = txtEmail.getText();
//		strCommentaire = txtAreaCommentaires.getText();
//		intIdSociete = Integer.parseInt(lblId.getText());
//
//		// ******             TRAITEMENT POUR LES  CLIENTS        *******
//		if (fenetreAccueil.getTypeSociete().equals(ConstantesApplication.TYPE_CLIENT)) {
//
//			int reponse = JOptionPane.showConfirmDialog(null,
//					"Voulez vous vraiment mettre à jour le client " + txtRaisonSociale.getText(), "MISE A JOUR",
//					JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
//			if (reponse == 0) {
//
//				// RECUPERATION DES VALEURS PROPRES AUX CLIENTS
//				try {
//					dblChiffreAffaire = Double.parseDouble(txtChiffreAffaire.getText());
//					intNbrEmployes = Integer.parseInt(txtNbrEmplyes.getText());
//				} catch (NumberFormatException e) {
//					throw new ExceptionPersonnalisee("Erreur ChiffreAffaire ET/OU NombreEmployes : " + e.getMessage());
//				}
//
//				// MODIFICATION DES ATTRIBUTS DU CLIENT DANS LA BDD ET DANS L'OBJET
//				Societe societeMatch = null;
//				for (Societe societe : listeSocietes.getListSocietes()) {
//					if (societe.getIdSociete() == intIdSociete) {
//						societeMatch = societe;
//						// MODIFICATION DES ATTRIBUTS DE L'OBJET
//						Adresse adrSocieteAdresse = new Adresse(strNumRue, strNomRue, strCodePostal, strVille);
//						try {
//							societe.setAdresse(adrSocieteAdresse);
//							societe.setCommentaires(strCommentaire);
//							societe.setDomaine(enumDomaine);
//							societe.setEmail(strEmail);
//							societe.setRaisonSociale(strRaisonSociale);
//							societe.setTelephone(strTelephone);
//							((Client) societe).setChiffreAfaire(dblChiffreAffaire);
//							((Client) societe).setNbrEmployes(intNbrEmployes);
//							((Client) societe).calculRatioChiffreAfaireNombreEmployes();
//							JOptionPane.showMessageDialog(null,
//									"Le client " + txtRaisonSociale.getText() + " a été mise a jour avec succés",
//									"Modification", JOptionPane.INFORMATION_MESSAGE);
//						} catch (ExceptionChampObligatoire | ExceptionPersonnalisee  e) {
//							JOptionPane.showMessageDialog(null,
//									e.getMessage(),
//									"Erreur", JOptionPane.WARNING_MESSAGE);
//							return;
//						}
//						reinitialiserChampsFormulaires();
//					}
//				}
//				// MODIFICATION DES ATTRIBUTS DU CLIENT DANS LA BDD 
//				if (societeMatch != null) {
//					try {
//						//new DaoClient(listeSocietes).save2((Client) societeMatch);
//						new DaoSociete(listeSocietes).save(societeMatch);
//					} catch (ExceptionDao | SQLException e) {
//						e.printStackTrace();
//					}					
//				}
//			}
//		}
//		
//		// ******             TRAITEMENT DES PROSPECTS        *******
//		if (fenetreAccueil.getTypeSociete().equals(ConstantesApplication.TYPE_PROSPECT)) {
//
//			// RECUPERATION DES VALEURS PROPRES AUX PROSPECTS
//			int intInteret = comboBxInteret.getSelectedIndex();
//			EnumInteret enumInteret = intInteret == 1 ? EnumInteret.NON : EnumInteret.OUI;
//			strDateProspection = txtDateProspection.getText();
//
//			int reponse = JOptionPane.showConfirmDialog(null,
//					"Voulez vous vraiment mettre à jour le prospect " + txtRaisonSociale.getText(), "MISE A JOUR",
//					JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
//			if (reponse == 0) {
//
//				// MODIFICATION DES ATTRIBUTS DU PROSPECT
//				Societe societeMatch = null;
//				for (Societe societe : listeSocietes.getListSocietes()) {
//					if (societe.getIdSociete() == intIdSociete) {
//						societeMatch = societe;
//						// MODIFICATION DES ATTRIBUTS DANS L'OBJET
//						Adresse adrSocieteAdresse = new Adresse(strNumRue, strNomRue, strCodePostal, strVille);
//						try {
//							societe.setAdresse(adrSocieteAdresse);
//							societe.setCommentaires(strCommentaire);
//							societe.setDomaine(enumDomaine);
//							societe.setEmail(strEmail);
//							societe.setRaisonSociale(strRaisonSociale);
//							societe.setTelephone(strTelephone);
//							((Prospect) societe).setDateProspection(strDateProspection);
//							((Prospect) societe).setInteret(enumInteret);
//							strDateProspection = txtDateProspection.getText();
//							JOptionPane.showMessageDialog(null,
//									"Le prospect " + txtRaisonSociale.getText() + " a été mise a jour avec succés",
//									"Modification", JOptionPane.INFORMATION_MESSAGE);
//						} catch (ExceptionChampObligatoire e) {
//							JOptionPane.showMessageDialog(null,
//									e.getMessage(),
//									"Erreur", JOptionPane.WARNING_MESSAGE);
//						}
//						reinitialiserChampsFormulaires();
//					}
//				}
//				// MODIFICATION DES ATTRIBUTS DU PROSPECT DANS LA BDD 
//				if (societeMatch != null) {
//					try {
//						//new DaoProspect(listeSocietes).save2((Prospect) societeMatch);
//						new DaoSociete(listeSocietes).save(societeMatch);
//					} catch (ExceptionDao | SQLException e) {
//						e.printStackTrace();
//					}					
//				}
//			}
//		}
//	}	

//	// METHODE SUPPRESSION CONTRAT
//	/*
//	 * Methode qui supprime une Contrat
//	 */
//	private void supprimerContrat() {
//
//			Contrat contratASupprimer = null;
//			int reponse = JOptionPane.showConfirmDialog(null,
//					"Voulez vous vraiment supprimer le contrat " + txtRaisonSociale.getText(), "SUPPRESSION",
//					JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
//			if (reponse == 0) {
//
//	
//				try {
//					// Suppression du Prospect dans la Bdd
//					//new DaoProspect(listeSocietes).delete((Prospect) societeASupprimer);
//					new DaoSociete(listeSocietes).delete(contratASupprimer.getIdSociete());
//					try {
//						// Suppression du Prospect dans la liste des Societe
//						listeSocietes.supprimerSociete(contratASupprimer);
//						JOptionPane.showMessageDialog(null,
//								"Le propect " + txtRaisonSociale.getText() + " a été supprimé avec succés", "Suppression",
//								JOptionPane.INFORMATION_MESSAGE);
//					} catch (ExceptionPersonnalisee e) {
//						JOptionPane.showMessageDialog(null, e.getMessage(), "Suppression", JOptionPane.INFORMATION_MESSAGE);
//					}
//				} catch (ExceptionDao | ExceptionChampObligatoire e1) {
//					JOptionPane.showMessageDialog(null, e1.getMessage(), "Suppression", JOptionPane.INFORMATION_MESSAGE);
//				}
//
//				// Reinitialisation des champs du formulaire
//				reinitialiserChampsFormulaires();
//			}
//	}
		
	// METHODE CREATION_SOCIETE
	/*
	 * Permet la creation d'un Contrat a partir des valeurs renseignees dans le
	 * formulaire de Creation
	 * Renvoit une/des exeptions de type ExceptionPersonnalisee, si un ou plusieur champs
	 *  ont été incorrectement renseignes
	 * Retourne une reference vers la societe si elle a ete cree
	 * Retourne null si la societe n a pas ete cree
	 */
	private Contrat creerContrat() throws ExceptionPersonnalisee {

		// VARIABLES LOCALES
		String strLibelle;
		String strMontant;
		String strDateDebut;
		String strDateFin;
		String strIdClient;
		Contrat contratAcreer = null;

		// RECUPERATION DES VALEURS PROVENANT DU FORMULAIRE :
		strLibelle = txtLibelle.getText();
		
		strMontant = txtMontantContrat.getText();
		double montant = Double.parseDouble(strMontant);		
		
		strDateDebut = txtDateDebutContrat.getText();
		 if (!(strDateDebut.matches("[0-9]{2}\\-[0-9]{2}\\-[0-9]{4}"))){
		    	throw new ExceptionPersonnalisee("Date de debut contrat incorrecte");
		 }
        int jour = Integer.parseInt(strDateDebut.substring(0, 2));
        int mois = Integer.parseInt(strDateDebut.substring(3, 5));
        int annee = Integer.parseInt(strDateDebut.substring(6, 10));
        LocalDate lcDebut = LocalDate.of(annee, mois, jour);        
        
		strDateFin = txtDateFinContrat.getText();
		 if (!(strDateFin.matches("[0-9]{2}\\-[0-9]{2}\\-[0-9]{4}"))){
		    	throw new ExceptionPersonnalisee("Date de fin contrat incorrecte");
		 }
        jour = Integer.parseInt(strDateFin.substring(0, 2));
        mois = Integer.parseInt(strDateFin.substring(3, 5));
        annee = Integer.parseInt(strDateFin.substring(6, 10));
	    LocalDate lcFin = LocalDate.of(annee, mois, jour);
	    
		strIdClient = txtClient.getText();
		idClient =  Integer.parseInt(strIdClient);
		
		int reponse = JOptionPane.showConfirmDialog(null,
				"Voulez vous vraiment creer le Contrat " + strLibelle, "CREATION",
				JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (reponse == 0) {
			try {
				// CREATION DU NOUVEAU CONTRAT EN BDD
				contratAcreer = new Contrat(strLibelle, montant, lcDebut, lcFin);
				
				try {
					Client clientToAddContrat = null;
					if (  (clientToAddContrat = (Client) new DaoSociete(new ListeSocietes()).find(idClient)) != null  ) {
						contratAcreer.setIdSociete(idClient);
						Contrat newContrat = new DaoContrat(listeContrats).createContrat(contratAcreer);
						new DaoContrat(listeContrats).associerContratClient(clientToAddContrat, newContrat);
						
					}else
					{
						JOptionPane.showConfirmDialog(null,
								"LE CLIENT " + idClient + " N EXISTE PAS ! ", "CREATION",
								JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
						return null;
					}
				} catch (ExceptionChampObligatoire e) {
					e.printStackTrace();
				}
				


			} catch (SQLException | ExceptionDao e1) {
				throw new ExceptionPersonnalisee("Creation du Contrat impossible car " + e1.getMessage());
			}

			// REINITIALISATION DU CHAMPS DU FORMULAIRE
			if (contratAcreer != null) {
				reinitialiserChampsFormulaires();
			}

			//listeSocietes.ajouterSociete(societeAcreer);
			JOptionPane.showMessageDialog(null,
					"Le contrat " + strLibelle + " a été créé avec succés", "Creation",
					JOptionPane.INFORMATION_MESSAGE);
		}
		return contratAcreer;
	}	
	// Methode permettant de reinitialiser les champs du formulaire
	private void reinitialiserChampsFormulaires() {
		txtLibelle.setText("");
		txtMontantContrat.setText("");
		txtDateDebutContrat.setText("");
		txtDateFinContrat.setText("");
		txtClient.setText("");
	}

//	
//
//	// 	*****            	  METHODES POUR LA GESTION DE LA COMBO_BOX DES SOCIETES 			   *******
//
//	// METHODE QUI PREPARE LES DONNEES POUR LA COMBO_BOX_SOCIETES
//	/*
//	 * Methode qui prepare les donnees pour alimenter la ComBoBoxSociete.
//	 * Retourne un tableau de String avec les donnees pour alimenter la ComBoBoxSociete.
//	 */
//	private String[] remplirComboBox(ListeContrats listeContrats2, String typeSociete) {
//		
//		// PREPARATION DES ENTREES POUR LA COMBO_BOX :
//		//  Selection des valeurs des champs "idSociete" et "RaisonSociale
//		ArrayList<Societe> listeTemp = listeContrats2.getListSocietes();
//		String stringTemp = "";
//
//		if (typeSociete.equals(ConstantesApplication.TYPE_CLIENT)) {
//			for (Societe societe : listeTemp) {
//				if (societe instanceof Client) {
//					stringTemp = stringTemp + "::" + societe.getIdSociete() + " " + societe.getRaisonSociale();
//				}
//			}
//		}
//		if (typeSociete.equals(ConstantesApplication.TYPE_PROSPECT)) {
//			for (Societe societe : listeTemp) {
//				if (societe instanceof Prospect) {
//					stringTemp = stringTemp + "::" + societe.getIdSociete() + " " + societe.getRaisonSociale();
//				}
//			}
//		}
//		String[] tabTempStrings = { "" };
//		return tabTempStrings = stringTemp.split("::");
//	}
	

//	
//	// LISTENER ASSOCIE A LA COMBO_BOX_SOCIETE
//	/*
//	 * Remplit les champs du formulaire avec les valeurs associees a la 
//	 *  societe selectionnee dans la ComboBoxSociete
//	 */
//	class ItemListenerComboBoxSociete implements ItemListener {
//
//		@Override
//		public void itemStateChanged(ItemEvent e) {
//
//			// Recupere l'IdSociete associé a la Societe selectionnee dans la ComboBox
//			int idx;
//			String societeSelectionnee = ((JComboBox<?>) e.getSource()).getSelectedItem().toString();
//			String[] tabTemp = societeSelectionnee.split(" ");
//			String idSocieteASupprimer = tabTemp[0];
//			int id = Integer.parseInt(idSocieteASupprimer);
//
//			ArrayList<Societe> listeTemp = listeSocietes.getListSocietes();
//			
//			// GESTION D'UN PROSPECT :
//			if (typeSociete.equals(ConstantesApplication.TYPE_PROSPECT)) {
//				for (Societe societe : listeTemp) {
//					//System.out.println(societe.getRaisonSociale());
//					if (societe instanceof Prospect) {
//						int valTemp = societe.getIdSociete();
//						if (valTemp == id) {
//							remplirChampsCommunsSociete(societe);
//							txtDateProspection.setText(((Prospect) societe).getDateProspection() );
//							idx = ((Prospect) societe).getInteret().equals(EnumInteret.OUI.toString()) ? 0 : 1;
//							comboBxInteret.setSelectedIndex(idx);
//						}
//					}
//				}
//			}
//			// GESTION D'UN CLIENT :
//			if (typeSociete.equals(ConstantesApplication.TYPE_CLIENT)) {
//				for (Societe societe : listeTemp) {
//					if (societe instanceof Client) {
//						if (societe.getIdSociete() == id) {
//							remplirChampsCommunsSociete(societe);
//							txtChiffreAffaire.setText( ((Client) societe).getChiffreAfaire().toString() );
//							txtNbrEmplyes.setText(((Client) societe).getNbrEmployes().toString());
//						}
//					}
//				}
//			}
//		}
//
//		// Remplissage des champs communs aux Prospects et aux Clients
//		private void remplirChampsCommunsSociete(Societe societe) {
//			int idx;
//			lblId.setText(societe.getIdSociete().toString());
//			txtRaisonSociale.setText(societe.getRaisonSociale());
//			idx = societe.getDomaine().equals(EnumDomaine.PRIVE.toString()) ? 1 : 0;
//			comboBxDomaine.setSelectedIndex(idx);
//			txtNumeroRue.setText(societe.getAdresse().getNumRue());
//			txtNomRue.setText(societe.getAdresse().getNomRue());
//			txtCodePostal.setText(societe.getAdresse().getCodePostal());
//			txtVille.setText(societe.getAdresse().getVille());
//			txtTelephone.setText(societe.getTelephone());
//			txtEmail.setText(societe.getEmail());
//			// societe.getCommentaires() = societe.getCommentaires() == null ? "" : societe.getCommentaires();
//			txtAreaCommentaires.setText(societe.getCommentaires());
//		}
//	}
}

		
		

