package fr.leo.graphique.contrats;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import fr.leo.utilitaire.ConstantesApplication;
import fr.leo.utilitaire.ListeContrats;
import java.awt.Color;

public class FenetreGestionContrats extends JFrame {

	/**
	 * Fenetre de gestion des Contrats de l'application REVERSO
	 */
	
	// CONSTANTES DE CLASSE
	private static final long serialVersionUID = 1L;
	
	// ATTRIBUTS D'INSTANCE
	private ListeContrats listeContrats;
	private JLabel lblTitrePanelCrud;
	//private JButton btnContrats = new JButton();
	JPanel jplCrud = new JPanel();
	private JLabel lblTitre;
	
	// CONSTRUCTEUR
	public FenetreGestionContrats(FenetreContrats fenetreContrats) {	
		
		// LookAndFeel "Nimbus"
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

		// AFFECTATION DES ATTRIBUTS D'INSTANCE
		this.listeContrats = new ListeContrats();
		
		// CARACTERISTIQUES GENERALES DE LA JFRAME
		setTitle("GESTION DES CONTRATS");	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 190, 1200, 570);		
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		// PANEL "CRUD" : Creation, Suppression, MiseAJour
		jplCrud.setVisible(true);
		jplCrud.setBounds(0, 0, 1184, 300);
		getContentPane().add(jplCrud);
		jplCrud.setLayout(null);

		
		// *****                  COMPOSANTS DU PANNEL "CRUD"                 *****
		
		// LABEL TITRE
		lblTitrePanelCrud = new JLabel();
		lblTitrePanelCrud.setBounds(500, 38, 250, 42);
		lblTitrePanelCrud.setFont(new Font("Tekton Pro", Font.BOLD, 30));
		jplCrud.add(lblTitrePanelCrud);
		
		lblTitre = new JLabel("GESTION DES CONTRATS :");
		lblTitre.setFont(new Font("Dark Tales", Font.BOLD, 30));
		lblTitre.setBounds(435, 31, 469, 31);
		jplCrud.add(lblTitre);
		
		// BOUTON AFICHAGE : appel a la methode 'afficherSocietes()'
		JButton btnAffichage = new JButton("AFFICHAGE");
		btnAffichage.setToolTipText("Affiche la liste");
		btnAffichage.setBounds(265, 95, 140, 42);
		jplCrud.add(btnAffichage);
		btnAffichage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afficherSocietes();
			}
		});
		
		// BOUTON MODIFICATION : appel a la methode 'modiferContrat()'
		JButton btnModification = new JButton("MODIFICATION");
		btnModification.setBackground(Color.RED);
		btnModification.setToolTipText("Modifier une societe");
		btnModification.setBounds(436, 95, 140, 42);
		jplCrud.add(btnModification);
//		btnModification.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				modiferContrat();
//			}
//		});
		
		// BOUTON SUPPRESSION : appel a la methode 'supprimerSociete()'
		JButton btnSuppression = new JButton("SUPPRESSION");
		btnSuppression.setBackground(Color.RED);
		btnSuppression.setToolTipText("Supprimer une societe");
		btnSuppression.setBounds(608, 95, 140, 42);
		jplCrud.add(btnSuppression);
//		btnSuppression.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				supprimerContrat();
//			}
//		});
		
		// BOUTON CREATION : appel a la methode 'creerContrat()'
		JButton btnCreation = new JButton("CREATION");
		btnCreation.setToolTipText("Creer une societe");
		btnCreation.setBounds(792, 95, 140, 42);
		jplCrud.add(btnCreation);
		btnCreation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				creerContrat();
			}
		});

		// BOUTON RETOUR_ACCUEIL :  panel CRUD invisible, panel ACCUEIL_APPLICATION visible
		JButton btnRetourAccueil = new JButton("RETOUR");
		btnRetourAccueil.setToolTipText("Retour");
		btnRetourAccueil.setBounds(192, 185, 140, 42);
		jplCrud.add(btnRetourAccueil);
		FenetreGestionContrats _this = this;
		btnRetourAccueil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_this.dispose();
				fenetreContrats.setVisible(true);
			}
		});
		
		// BOUTON QUITTER
		JButton btnQuitter = new JButton("QUITTER APP");
		btnQuitter.setToolTipText("Quitter l'application");
		btnQuitter.setBounds(869, 185, 140, 42);
		jplCrud.add(btnQuitter);
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});	
	} // FIN CONSTRUCTEUR
	
	
	 
	// *****           		      GETTERS ET SETTERS            	      *****
//	protected String getTypeSociete() {
//		return typeSociete;
//	}
	
	
	// *****           		           METHODES                    	      *****
	// AFFICHER_SOCIETE
	private void afficherSocietes() {	
		this.setVisible(false);
		new FenetreAffichageContrats(listeContrats, this ).setVisible(true);
	}
	// MODIFIER_SOCIETE 
	//	private void modiferContrat( ) {
	//		new FenetreCreationModificationSuppressionContrats(
	//				listeContrats, 
	//				this, 
	//				ConstantesApplication.MODIFICATION_CONTRAT ).setVisible(true);	
	//	}
	// CREER_SOCIETE
	private void creerContrat() {
		this.setVisible(false);
		new FenetreCreationModificationSuppressionContrats(
				listeContrats, 
				this, 
				ConstantesApplication.CREATION_CONTRAT ).setVisible(true);;
	}
	// SUPPRIMER_CONTRAT
//	private void supprimerContrat() {	
//		this.setVisible(false);
//		new FenetreCreationModificationSuppressionContrats(
//						listeContrats, 
//						this, 
//						ConstantesApplication.CREATION_CONTRAT ).setVisible(true);;
//	}
	// CONTRATS
//	private void voirContrats() {	
//		this.setVisible(false);
//		new FenetreContrats(listeSocietes , this);
//	}	
}
