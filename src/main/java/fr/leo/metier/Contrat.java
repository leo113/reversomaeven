package fr.leo.metier;

import java.time.LocalDate;

import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.utilitaire.Utilitaire;

public class Contrat {
	
	/**
	 * Classe de gestion des Contrats
	 * Un Client a 0 ou n contrats
	 */
	
	// ATTRIBUTS DE CLASSE
	private static String[] listeDesDonneesGerees = {"idContrat", "libelleContrat", "montantContrat", "dateDebutContrat" , "dateFinContrat" , "Client" } ;

	// ATTRIBUTS D'INSTANCE
	private Integer idContrat;
	private String libelleContrat;
	private Double montantContrat;
	private String dateDebutContrat;
	private String dateFinContrat;
	private Integer idSociete;
	
	// CONSTRUCTEURS
	public Contrat() {
	}

	public Contrat(String libelleContrat, Double montantContrat, LocalDate dateDebutContrat,
			LocalDate dateFinContrat) throws ExceptionPersonnalisee {
		super();
		this.setIdContrat(null);
		this.setLibelleContrat(libelleContrat);
		this.setMontantContrat(montantContrat);
		this.setDateDebutContrat(dateDebutContrat);
		this.setDateFinContrat(dateFinContrat);
		this.setIdSociete(null);
	}

	// GETTERS ET SETTERS
	public Integer getIdContrat() {
		return idContrat;
	}
	public void setIdContrat(Integer idContrat) {
		this.idContrat = idContrat;
	}
	public String getLibelleContrat() {
		return libelleContrat;
	}
	public void setLibelleContrat(String libelleContrat) {
		this.libelleContrat = libelleContrat;
	}
	public Double getMontantContrat() {
		return montantContrat;
	}
	public void setMontantContrat(Double montantContrat) {
		this.montantContrat = montantContrat;
	}
	public String getDateDebutContrat() {
		return dateDebutContrat;
	}	
	public int getIdSociete() {
		return  this.idSociete.intValue();
	}
	public void setIdSociete(Integer idSociete) {
		this.idSociete = idSociete;
	}
	
	/**
	 * Retourne une execption de type ExceptionPersonnalisee si la date de debut de contrat est null
	 * @param dateProspection LocalDate 
	 * @throws ExceptionPersonnalisee exceptionPersonnalisee
	 * @see ExceptionPersonnalisee
	 */
	public void setDateDebutContrat(LocalDate dateDebutContratEnter) throws ExceptionPersonnalisee {
		 if ( (dateDebutContratEnter==null )) {
		        throw new ExceptionPersonnalisee("La date de debut de Contrat doit etre renseignee");
		    }else{
		    	dateDebutContrat =  Utilitaire.FOMATTER_DATE.format(dateDebutContratEnter);	
		    }	
	}	
	public String getDateFinContrat() {
		return dateFinContrat;
	}
	/**
	 * Retourne une execption de type ExceptionPersonnalisee si la date de fin de contrat est null
	 * @param dateProspection LocalDate 
	 * @throws ExceptionPersonnalisee exceptionPersonnalisee
	 * @see ExceptionPersonnalisee
	 */
	public void setDateFinContrat(LocalDate dateFinContratEnter) throws ExceptionPersonnalisee {
		 if ( (dateFinContratEnter==null)) {
		        throw new ExceptionPersonnalisee("La date de fin de  Contrat doit etre renseignee");
		    }else{
		    	dateFinContrat =  Utilitaire.FOMATTER_DATE.format(dateFinContratEnter);	
		    }		
	}	
	public static String[] getListeDesDonneesGerees() {
		return listeDesDonneesGerees;
	}

	
	@Override
	public String toString() {
		String idSociete =  this.idSociete == null ?  ""  :  this.getIdSociete() +"" ;
		return "Contrat: " 	+ this.getIdContrat() 
							+ " " + this.libelleContrat
							+ " " + this.getMontantContrat() 
							+ " Date de début : " + this.getDateDebutContrat()
							+ " Date de fin : " + this.getDateFinContrat()
							+ idSociete ;
	}
	
	

}
