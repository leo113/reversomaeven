package fr.leo.enums;

import fr.leo.metier.Prospect;

public enum EnumInteret {
	
	/**
	 * Interet exprimé par un Prospect
	 * @see Prospect
	 */
	
	OUI, NON;

}
