package fr.leo.enums;

import fr.leo.metier.Societe;

public enum EnumTypeSociete {
	
	/**
	 * Types possibles pour une Societe
	 * @see Societe
	 */
	
	CLIENT, PROSPECT
}
