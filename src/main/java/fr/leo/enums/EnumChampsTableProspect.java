package fr.leo.enums;

public enum EnumChampsTableProspect {
	
	/**
	 * Champs possibles dans la table Prospect
	 * @see Prospect
	 */
	num_rue,nom_rue,code_postal,ville,
	raison_sociale,domaine,telephone,
	email,commentaires,date_prospection, interet
	
}
