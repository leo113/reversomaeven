package fr.leo.enums;

public enum EnumChampsTableAdresse {
	
	/**
	 * Champs possibles dans la table Adresse
	 * @see Adresse
	 */
	num_rue,nom_rue,code_postal,ville,nombre_employes	
}
