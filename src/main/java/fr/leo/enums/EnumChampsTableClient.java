package fr.leo.enums;

public enum EnumChampsTableClient {
	
	/**
	 * Champs possibles dans la table Prospect
	 * @see Prospect
	 */
	num_rue,nom_rue,code_postal,ville,
	raison_sociale,domaine,telephone,
	email,commentaires,chiffre_affaire,nombre_employes
	
}
