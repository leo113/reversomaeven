package fr.leo.controllers;

import java.sql.SQLException;
import java.util.ArrayList;

import fr.leo.dao.DaoSociete;
import fr.leo.exceptions.ExceptionChampObligatoire;
import fr.leo.exceptions.ExceptionDao;
import fr.leo.exceptions.ExceptionPersonnalisee;
import fr.leo.metier.Societe;
import fr.leo.utilitaire.ListeSocietes;

public class ControllerFormulaire {
	
	// ATTRIBUTS D'INSTANCE
	private static ListeSocietes listeSociete = new ListeSocietes();
	
	// GETTERS ET SETTERS
	public static ListeSocietes getListeSociete() {
		return listeSociete;
	}

	// CONSTRUCTEUR
	public ControllerFormulaire() {
	}
	
	// METHODES
	public void create(Societe societe) throws ExceptionDao, ExceptionPersonnalisee, SQLException {		
		new DaoSociete(listeSociete).save(societe);
	}
	public void delete(Societe societe) throws ExceptionDao, ExceptionChampObligatoire {		
		new DaoSociete(listeSociete).delete(societe.getIdSociete());
	}
	public ArrayList<Societe> findAll() throws ExceptionDao, ExceptionPersonnalisee, ExceptionChampObligatoire, SQLException  {
		return new DaoSociete(listeSociete).findAll();
	}
	public Societe find(Societe societe) throws Exception {
		return new DaoSociete(listeSociete).find(societe.getIdSociete());
	}
	public Societe save(Societe societe) throws ExceptionDao, ExceptionPersonnalisee, SQLException   {
		return new DaoSociete(listeSociete).save(societe);
	}
}
