package AFPA.CDA03.demo;

import java.awt.EventQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.leo.graphique.FenetreAccueil;

public class App {
	
	private static final Logger logger = LogManager.getLogger(App.class);

	public static void main(String[] args)  {	
		
		logger.info("demarrage application !");

		// 			********  1.ON INSTANCIE DES PROSPECTS ET DES CLIENTS :   ************
		//  			  POUR L'INSTANT LEUR IDENTIFIANT A LA VALEUR  'NULL'.
		
			//		// CREATION DE PLUSIEURS PROSPECTS
//					Prospect prospect1 = null;Prospect prospect2 = null;
//					Prospect prospect3 = null;Prospect prospect4 = null;Prospect prospect5 = null;
//					try {
//						prospect1 = new Prospect("Demeter", EnumDomaine.PRIVE,
//												new Adresse("15", "impasse de la ville", "25417", "Avignon"), "0332154784523",
//												"demeter.ese@free.fr", "12-12-2000", EnumInteret.NON);			
//						prospect2 = new Prospect("Themis", EnumDomaine.PUBLIC,
//												new Adresse("16", "rue des mouettes", "37400", "Amboise"),	"00333255145587",
//												"themis@free.fr", "14-11-2001", EnumInteret.OUI);			
//						prospect3 = new Prospect("Hygie", EnumDomaine.PUBLIC,
//												new Adresse("17", "rue du phare", "87100", "Limoges"),"00335421569854",
//												"hygie@free.fr", "23-07-2014", EnumInteret.OUI);			
//						prospect4 = new Prospect("Boree", EnumDomaine.PRIVE,
//												new Adresse("18", "rue des champs de ble", "17200", "Royan"), "00335422126985", "boree@free.fr",
//												"30-01-2010", EnumInteret.NON);			
//						prospect5 = new Prospect("Vesta", EnumDomaine.PUBLIC,
//												new Adresse("19", "rue du mais transgenique", "74100", "Chamonix"), "00335215488746",
//												"vesta@free.fr", "12-12-2012", EnumInteret.OUI);
//					} catch (ExceptionChampObligatoire e1) {
//						System.out.println(e1.getMessage());
//					} catch (ExceptionPersonnalisee e) {
//						System.out.println(e.getMessage());
//					}

//					Societe client1 = null,client2 = null,client3 = null,client4 = null,client5 = null;
//					
//					try {
//						client1 = new Client(	"FleurAuDent", EnumDomaine.PRIVE,
//												new Adresse("10", "rue des jardins", "57120", "Metz"), 
//												"00331254236587", "bartabac@free.fr", 12145121, 100);
//						client2 = new Client(	"Eoles", EnumDomaine.PUBLIC,
//												new Adresse("11", "rue des alouettes", "75002", "Paris"),
//												"00332545658745", "eole.la@free.fr", 1112201.3, 1247);
//						client3 = new Client("Oceanos", EnumDomaine.PRIVE,
//												new Adresse("12", "rue des prairies en fleur", "29800", "Landernau"),
//												"00333625148974","oceanos.lia@free.fr", 11110, 1110);
//						client4 = new Client(	"Maia", EnumDomaine.PRIVE,
//												new Adresse("13", "rue des vertes collines", "59000", "Lille"), 
//												"00335554736524", "maia.lea@free.fr", 11241110, 14);
//						client5 = new Client(	"Chloris", EnumDomaine.PUBLIC,
//												new Adresse("14", "rue des coquelicots", "64200", "Biarritz"),
//												"00333521654782","chloris.li@free.it", 11110, 1110);
//					} catch (ExceptionChampObligatoire | ExceptionPersonnalisee | SQLException | ExceptionDao e1) {
//						e1.printStackTrace();
//					}
					
		
		
		//  		************** 2. ON INSERE LES INSTANCES DE CLIENTS ET PROSPECTS EN BDD :   ******************
		//									ILS ACQUIERENT UN IDENTIFIANT EN BDD
		//						LEURS INSTANCES SONT RETOURNéS AVEC L IDENTIFIANT CRéé EN BDD AJOUTé
		//
					// On a besoin d'une DaoSociete 
					// La DaoSociete requiere comme parametre une ListeSociete afin de mettre à jour 
					//   cette liste !
					// Si des societes sont ajoutées, modifiées ou supprimées en Bdd, cette liste sera 
					//  automatiquement mis a jour.
//					ListeSocietes listeSocietes = new ListeSocietes();
//					DaoSociete daoSociete = new DaoSociete(listeSocietes);						
//					
//					try {
//					daoSociete.createSociete(prospect1);
//					daoSociete.createSociete(prospect2);		
//					daoSociete.createSociete(prospect3);
//					daoSociete.createSociete(prospect4);
//					daoSociete.createSociete(prospect5);	
//					daoSociete.createSociete(client1);
//					daoSociete.createSociete(client2);		
//					daoSociete.createSociete(client3);
//					daoSociete.createSociete(client4);
//					daoSociete.createSociete(client5);	
//				} catch (SQLException | ExceptionDao e1) {
//					e1.printStackTrace();
//				}
		


		
		//  		************** 3. ON INSTANCIE DES CONTRATS  :   ******************
		//					 POUR L'INSTANT LEUR IDENTIFIANT A LA VALEUR  'NULL'.
		//
		// CREATION DE CONTRATS POUR CERTAINS CLIENTS
//		Contrat c1 = null;Contrat c2 = null;
//		Contrat c3 = null;Contrat c4 = null;Contrat c5 = null;
//		try {
//			c1 = new Contrat( "Contrat21", 100.2, LocalDate.of(2015, Month.JANUARY, 14),LocalDate.of(2019, Month.JANUARY, 14));
//			c2 = new Contrat( "Contrat22", 200.2, LocalDate.of(2015, 02, 14), LocalDate.of(2019, 02, 14));
//			c3 = new Contrat( "Contrat23", 300.2, LocalDate.of(2015, 03, 14), LocalDate.of(2019, 03, 14));
//			c4 = new Contrat( "Contrat24", 400.2, LocalDate.of(2015, 04, 14), LocalDate.of(2019, 04, 14));
//			c5 = new Contrat( "Contrat25", 500.2, LocalDate.of(2015, 05, 14), LocalDate.of(2019, 05, 14));
//		} catch (ExceptionPersonnalisee e3) {
//			System.out.println(e3.getMessage());
//		}	
		
		
		//  		************** 4. ON INSERE LES INSTANCES DES CONTRATS EN BDD :   ******************
		//							ILS ACQUIERENT UN IDENTIFIANT EN BDD
		//
		// On a besoin d'une DaoContrat 
		// La DaoContrat requiere comme parametre une ListeContrat afin de mettre à jour 
		//   cette liste !
		// Si des contrats sont ajoutés, modifiés ou supprimés en Bdd, cette liste sera 
		//  automatiquement mis a jour.
		
//		ListeContrats listeContrats = new ListeContrats();
//		DaoContrat daoContrat = new DaoContrat(listeContrats);
//		try {
//			daoContrat.createContrat(c1);
//			daoContrat.createContrat(c2);
//			daoContrat.createContrat(c3);
//			daoContrat.createContrat(c4);
//			daoContrat.createContrat(c5);
//		} catch (SQLException | ExceptionDao | ExceptionPersonnalisee e1) {
//			e1.printStackTrace();
//		}
	
		
		// 			********* 5.  	AJOUT DE CONTRATS AUX CLIENTS  *******	
		
//		ListeContrats listeContrats = new ListeContrats();
//		DaoContrat daoContrat = new DaoContrat(listeContrats);			
		
//		try {
//			//Contrat c1 = new  Contrat( "Contrat1", 100.2, LocalDate.of(2019, 05, 14), LocalDate.of(2019, 05, 30));
//			//daoContrat.createContrat(c1);
//			//daoContrat.associerContratClient(358, 57);
//			
//			Client c11 = new Client(	"ChlorisPlus", EnumDomaine.PUBLIC,
//										new Adresse("14", "rue des coquelicots", "64200", "Biarritz"),
//										"00333521654782","chloris.li@free.it", 11110, 1110);
//
//			
//			
//		} catch (ExceptionPersonnalisee | ExceptionChampObligatoire | SQLException | ExceptionDao e1) {
//			e1.printStackTrace();
//		}//		

		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FenetreAccueil frame = new FenetreAccueil();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}






























				//  ******* OLD CODE ******* //

//// CREATION DE PLUSIEURS CLIENTS
//Client client1 = null;
//Client client2 = null;
//Client client3 = null;
//Client client4 = null;
//Client client5 = null;
//try {
//	client1 = new Client("FleurAuDent", EnumDomaine.PRIVE,
//			new Adresse("10", "rue des jardins", "57120", "Metz"), "00331254236587", "bartabac@free.fr",
//			12145121, 100);
//	client2 = new Client("Eoles", EnumDomaine.PUBLIC, new Adresse("11", "rue des alouettes", "75002", "Paris"),
//			"00332545658745", "eole.la@free.fr", 1112201.3, 1247);
//	client3 = new Client("Oceanos", EnumDomaine.PRIVE,
//			new Adresse("12", "rue des prairies en fleur", "29800", "Landernau"), "00333625148974",
//			"oceanos.lia@free.fr", 11110, 1110);
//	client4 = new Client("Maia", EnumDomaine.PRIVE,
//			new Adresse("13", "rue des vertes collines", "59000", "Lille"), "00335554736524",
//			"maia.lea@free.fr", 11241110, 14);
//	client5 = new Client("Chloris", EnumDomaine.PUBLIC,
//			new Adresse("14", "rue des coquelicots", "64200", "Biarritz"), "00333521654782",
//			"chloris.li@free.it", 11110, 1110);
//} catch (ExceptionChampObligatoire e) {
//	System.out.println(e.getMessage());
//} catch (ExceptionPersonnalisee e) {
//	System.out.println(e.getMessage());
//}

// CREATION DE PLUSIEURS CLIENTS
//DaoClient daoClient = new DaoClient();
//ArrayList<Client> listClients = null;
//try {
//	listClients = daoClient.findAll();
//} catch (ExceptionDao | ExceptionPersonnalisee | ExceptionChampObligatoire | SQLException e3) {
//	e3.printStackTrace();
//}		




//// CREATION DE PLUSIEURS PROSPECTS
//Prospect prospect1 = null;
//Prospect prospect2 = null;
//Prospect prospect3 = null;
//Prospect prospect4 = null;
//Prospect prospect5 = null;
//try {
//	prospect1 = new Prospect("Demeter", EnumDomaine.PRIVE,
//			new Adresse("15", "impasse de la ville", "Avignon", "Avignon"), "0332154784523",
//			"demeter.ese@free.fr", "12-12-2000", EnumInteret.NON);
//	prospect2 = new Prospect("Themis", EnumDomaine.PUBLIC,
//			new Adresse("16", "rue des mouettes", "37400", "Amboise"), "00333255145587", "themis@free.fr",
//			"14-11-2001", EnumInteret.OUI);
//	prospect3 = new Prospect("Hygie", EnumDomaine.PUBLIC, new Adresse("17", "rue du phare", "87100", "Limoges"),
//			"00335421569854", "hygie@free.fr", "23-07-2014", EnumInteret.OUI);
//	prospect4 = new Prospect("Boree", EnumDomaine.PRIVE,
//			new Adresse("18", "rue des champs de ble", "17200", "Royan"), "00335422126985", "boree@free.fr",
//			"30-01-2010", EnumInteret.NON);
//	prospect5 = new Prospect("Vesta", EnumDomaine.PUBLIC,
//			new Adresse("19", "rue du mais transgenique", "74100", "Chamonix"), "00335215488746",
//			"vesta@free.fr", "12-12-2012", EnumInteret.OUI);
//} catch (ExceptionChampObligatoire e1) {
//	System.out.println(e1.getMessage());
//} catch (ExceptionPersonnalisee e) {
//	System.out.println(e.getMessage());
//}

// CREATION DE PLUSIEURS PROSPECTS
//DaoProspect daoProspect = new DaoProspect();
//ArrayList<Prospect> listProspects = null;
//try {
//	listProspects = daoProspect.findAll();
//} catch (ExceptionDao | ExceptionPersonnalisee | ExceptionChampObligatoire | SQLException e2) {
//	e2.printStackTrace();
//}
//for (Prospect prospect : listProspects) {
//	try {
//		listeSocietes.ajouterSociete(prospect);
//		prospect.setCommentaires("Ceci est le commentaire Demeter");	
//	} catch (ExceptionPersonnalisee e) {
//		e.printStackTrace();
//	}
//}



// AJOUT A LA LISTE DES SOCIETES DES PROSPECTS ET DES CLIENTS
//try {
//	listeSocietes.ajouterSociete(client1);
//	listeSocietes.ajouterSociete(client2);
//	listeSocietes.ajouterSociete(client3);
//	listeSocietes.ajouterSociete(client4);
//	listeSocietes.ajouterSociete(client5);
	
	
//	listeSocietes.ajouterSociete(prospect1);
//	listeSocietes.ajouterSociete(prospect2);
//	listeSocietes.ajouterSociete(prospect3);
//	listeSocietes.ajouterSociete(prospect4);
//	listeSocietes.ajouterSociete(prospect5);
//} catch (ExceptionPersonnalisee e1) {
//	System.out.println(e1.getMessage());
//}

// AJOUT DE COMMENTAIRES POUR 1 PROSPECT ET 1 CLIENT
//prospect1.setCommentaires("Ceci est le commentaire Demeter");	





















//// DAO_CLIENT : MISE A JOUR Client : save()		
//HashMap<EnumChampsTableClient, String> listeChampsValeurs = new HashMap<>();		
//listeChampsValeurs.put(EnumChampsTableClient.num_rue, "333");
//listeChampsValeurs.put(EnumChampsTableClient.nom_rue, "rue des prairie");
//listeChampsValeurs.put(EnumChampsTableClient.code_postal, "22222");
//listeChampsValeurs.put(EnumChampsTableClient.ville, "LaBauLePlus");
////listeChampsValeurs.put(EnumChampsTableClient.raison_sociale, "rerePlus");
//listeChampsValeurs.put(EnumChampsTableClient.domaine, EnumDomaine.PRIVE.toString());
//listeChampsValeurs.put(EnumChampsTableClient.email, "rere@free.fr");
//listeChampsValeurs.put(EnumChampsTableClient.commentaires, "des commentaires plus ...");
//listeChampsValeurs.put(EnumChampsTableClient.chiffre_affaire, "100777");
//listeChampsValeurs.put(EnumChampsTableClient.nombre_employes, "52");
//listeChampsValeurs.put(EnumChampsTableClient.telephone, "00331111111333");		
//try {
//Client clientToUpdate = daoClient.save(listeChampsValeurs, "FleurAuDent145");
////if (( clientToUpdate != null) && (listeSocietes.chercherSociete(clientToUpdate) == -1 )  ) {
////	try {
////		listeSocietes.ajouterSociete(clientToUpdate);
////	} catch (ExceptionPersonnalisee e) {
////		e.printStackTrace();
////	}
////}
//} catch (SQLException | ExceptionDao e1) {
//e1.printStackTrace();
//}

//// DAO_PROSPECT : MISE A JOUR Prospect : save()
//HashMap<EnumChampsTableProspect, String> listeChampsValeursProspect = new HashMap<>();
//listeChampsValeursProspect.put(EnumChampsTableProspect.num_rue, "734");
//listeChampsValeursProspect.put(EnumChampsTableProspect.nom_rue, "rue des prairies");
//listeChampsValeursProspect.put(EnumChampsTableProspect.code_postal, "22222");
//listeChampsValeursProspect.put(EnumChampsTableProspect.ville, "LaBauLe");
////listeChampsValeursProspect.put(EnumChampsTableProspect.raison_sociale, "rerePlus");
//listeChampsValeursProspect.put(EnumChampsTableProspect.domaine, EnumDomaine.PRIVE.toString());
//listeChampsValeursProspect.put(EnumChampsTableProspect.telephone, "00334444444778");
//listeChampsValeursProspect.put(EnumChampsTableProspect.email, "rere@free.fr");
//listeChampsValeursProspect.put(EnumChampsTableProspect.commentaires, "");
//listeChampsValeursProspect.put(EnumChampsTableProspect.interet, EnumInteret.OUI.toString());
//listeChampsValeursProspect.put(EnumChampsTableProspect.date_prospection, "21-04-2028");	
//try {
//Prospect prospectToUpdate = daoProspect.save(listeChampsValeursProspect, "LaGrandeMarree34");
////if (( prospectToUpdate != null) && (listeSocietes.chercherSociete(prospectToUpdate) == -1 )  ) {
////try {
////	listeSocietes.ajouterSociete(prospectToUpdate);
////} catch (ExceptionPersonnalisee e) {
////	e.printStackTrace();
////}
////}
//} catch (SQLException | ExceptionDao e1) {
//e1.printStackTrace();
//}




























//int a = -2147483648; // 32 bit
//short b =  32767;
//long d = 9223372036854775807L;  // il faut mettre le L !
//byte e = 127;
//
//float g = 10.2f / 10f;  //32 bit
//double h = 10.2d / 3d;  // 64 bit
//
//float zz = 10.2f;
//
//int p = 9/2; // =>  4
//
//System.out.println(10d/2.1d); // => 4.761904761904762  double precision
//System.out.println(10f/2.1f); // => 4.761905  
//
//int marker = 512;
//double percentage =  marker * 0.46f;
//System.out.println( percentage);
//
//char var = 'A'; // n'importe quoi entre des simples quotes
//char var2 = '\u10E6';
//System.out.println(var2);
//
//// Il vaut mieux utiliser les doubles et les integer !
//// Cela accelere le code
//
//
//
//BigDecimal d1 = new BigDecimal("1.05");
//BigDecimal d2 = new BigDecimal("2.55");
//System.out.println( d1.add(d2));
//
//String aaa = new String("AZE");
//String bcd = "ADV";
//
//
//short a1 = 5;
//int a2 = a1;  // ok, cast implicite
//
//
//int a3 = 10;
//short a4 = (short) a3; // on est obligé de faire un cast explicite, sinon le compilateur rale !
//
//
//double v1 = 10.2222222222222d;
//float v2 = (float) v1; // Le cast est accepté, mais il y aura une perte de précision
//System.out.println(v1);  // => 10.2222222222222
//System.out.println(v2);  // => 10.222222
//
//int aze = 65000;
//short azs = (short) aze;
//System.out.println(azs); //  => -536
//
//// Il est possible de caster un long dans un float malgres que le long soit de 64bits et le float de 32 bits !
//
//// Il est possible de caster un float dans un long
//float azer= 10.2f;
//long cvb = (long) azer;
//System.out.println("ici " + cvb);  // => 10












// 			********* 5.  	 ON CREE DES SOCIETE ET DES CONTRATS ET ON LES ASSOCIE *******
//ListeSocietes listeSocietes = new ListeSocietes();
//DaoSociete daoSociete = new DaoSociete(listeSocietes);	
//try {
//	Client client115 = new Client(	"115", EnumDomaine.PRIVE,
//									new Adresse("10", "rue des jardins", "57120", "Metz"), 
//									"00331254236587", "bartabac@free.fr", 12145121, 100);
//	daoSociete.createSociete(client115);
//	
//	Contrat c7 = new Contrat( "c7", 500.2, LocalDate.of(2015, 05, 14), LocalDate.of(2019, 05, 14));
//	daoContrat.createContrat(c7);
//	
//	client115.addContratClient(c7);
//	daoContrat.associerContratClient(client115, c7);
//} catch (ExceptionChampObligatoire | ExceptionPersonnalisee | SQLException | ExceptionDao e1) {
//
//	e1.printStackTrace();
//}

//try {
//	Contrat c11111 = daoContrat.find(13);
//	c11111.setDateDebutContrat(LocalDate.of(2019, 02, 19));
//	daoContrat.save(c11111);
//} catch (ExceptionDao | ExceptionPersonnalisee | ExceptionChampObligatoire | SQLException e2) {
//	e2.printStackTrace();
//}

//try {
//	daoContrat.findAll(457);
//	for (Contrat contrat : daoContrat.findAll(458)) {
//		System.out.println(contrat);
//	}
//} catch (SQLException | ExceptionDao | ExceptionPersonnalisee e1) {
//	// TODO Auto-generated catch block
//	e1.printStackTrace();
//}

//ListeSocietes listeSocietes = new ListeSocietes();
//DaoSociete daoSociete = new DaoSociete(listeSocietes);		
//
//
//
//try {
//	Prospect prospect1 = new Prospect("LesNouvellesVagues", EnumDomaine.PRIVE,
//							new Adresse("15", "impasse de la ville", "25417", "Avignon"), "0332154784523",
//								"demeter.ese@free.fr", "12-12-2000", EnumInteret.NON);		
//	Societe sc1 =  daoSociete.find(421);
//	sc1.setEmail("a@a.fr");
//	System.out.println(daoSociete.save(prospect1));
//} catch (ExceptionChampObligatoire | ExceptionPersonnalisee | ExceptionDao | SQLException e1) {
//	e1.printStackTrace();
//}	






























//// CREATION DE PLUSIEURS CLIENTS EN BDD			
//Societe client1 = null,client2 = null,client3 = null,client4 = null,client5 = null;
//
//try {
//	client1 = new Client(	"FleurAuDent", EnumDomaine.PRIVE,
//							new Adresse("10", "rue des jardins", "57120", "Metz"), 
//							"00331254236587", "bartabac@free.fr", 12145121, 100);
//	client2 = new Client(	"Eoles", EnumDomaine.PUBLIC,
//							new Adresse("11", "rue des alouettes", "75002", "Paris"),
//							"00332545658745", "eole.la@free.fr", 1112201.3, 1247);
//	client3 = new Client("Oceanos", EnumDomaine.PRIVE,
//							new Adresse("12", "rue des prairies en fleur", "29800", "Landernau"),
//							"00333625148974","oceanos.lia@free.fr", 11110, 1110);
//	client4 = new Client(	"Maia", EnumDomaine.PRIVE,
//							new Adresse("13", "rue des vertes collines", "59000", "Lille"), 
//							"00335554736524", "maia.lea@free.fr", 11241110, 14);
//	client5 = new Client(	"Chloris", EnumDomaine.PUBLIC,
//							new Adresse("14", "rue des coquelicots", "64200", "Biarritz"),
//							"00333521654782","chloris.li@free.it", 11110, 1110);
//} catch (ExceptionChampObligatoire | ExceptionPersonnalisee e1) {
//	e1.printStackTrace();
//}
//try {
//	daoSociete.createSociete(client1);
//	daoSociete.createSociete(client2);		
//	daoSociete.createSociete(client3);
//	daoSociete.createSociete(client4);
//	daoSociete.createSociete(client5);	
//} catch (SQLException | ExceptionDao e4) {
//	e4.printStackTrace();
//}
//
//// CREATION DE CONTRATS POUR CERTAINS CLIENTS
//Contrat c1 = null;
//Contrat c2 = null;
//Contrat c3 = null;
//Contrat c4 = null;
//Contrat c5 = null;
//try {
//	c1 = new Contrat( "Contrat1", 100.2, LocalDate.of(2015, Month.JANUARY, 14),LocalDate.of(2019, Month.JANUARY, 14));
//	c2 = new Contrat( "Contrat2", 200.2, LocalDate.of(2015, 02, 14), LocalDate.of(2019, 02, 14));
//	c3 = new Contrat( "Contrat3", 300.2, LocalDate.of(2015, 03, 14), LocalDate.of(2019, 03, 14));
//	c4 = new Contrat( "Contrat4", 400.2, LocalDate.of(2015, 04, 14), LocalDate.of(2019, 04, 14));
//	c5 = new Contrat( "Contrat5", 500.2, LocalDate.of(2015, 05, 14), LocalDate.of(2019, 05, 14));
//} catch (ExceptionPersonnalisee e3) {
//	System.out.println(e3.getMessage());
//}
//
//// AJOUT DE CONTRATS AUX CLIENTS
//for (Societe client : listeSocietes.getListSocietes()) {
//	if (client instanceof Client) {	
//			try {
//			((Client) client).addContratClient(c1);
//			((Client) client).addContratClient(c2);
//			((Client) client).addContratClient(c3);
//			((Client) client).addContratClient(c4);
//			((Client) client).addContratClient(c5);
//			} catch (ExceptionPersonnalisee e2) {
//				System.out.println(e2.getMessage());
//			}
//	}
//}


//// CREATION DE PLUSIEURS PROSPECTS EN BDD
//try {
//	daoSociete.createSociete(prospect1);
//	daoSociete.createSociete(prospect2);			
//	daoSociete.createSociete(prospect3);
//	daoSociete.createSociete(prospect4);
//	daoSociete.createSociete(prospect5);
//} catch (SQLException | ExceptionDao e4) {
//	e4.printStackTrace();
//}
















//
////// CREATION Client Dao : createClient()
////try {
////	Client client7 = new Client("Chloris", EnumDomaine.PUBLIC,
////								new Adresse("14", "rue des coquelicots", "64200", "Biarritz"),
////								"00333521654782","chloris.li@free.it", 11110, 1110);
////	daoClient.createClient(client7);
////} catch (ExceptionDao | SQLException | ExceptionChampObligatoire | ExceptionPersonnalisee e1) {
////	//System.out.println(e1.getMessage());
////}	
////// CREATION Prospect Dao : createProspect()
////try {
////	Prospect prospect7 = new Prospect("LaGrandeMarreePlus", EnumDomaine.PUBLIC,
////									new Adresse("19", "rue des vallées valonnées", "74100", "Lorient"),
////									"00335215488746","vesta@free.fr", "12-12-2012", EnumInteret.OUI);
////	 daoProspect.createProspect(prospect7);
////} catch (ExceptionDao | SQLException | ExceptionChampObligatoire | ExceptionPersonnalisee e1) {
////	//System.out.println(e1.getMessage());
////}
//		
//
////// DAO_CLIENT : SUPPRESSION Client : 	
////Client client8 = null;
////try {
////	client8 = new Client("Carilya", EnumDomaine.PUBLIC,
////						new Adresse("14", "rue des coquelicots", "64200", "Biarritz"),
////						"00333521654782","chloris.li@free.it", 11110, 1110);
////	daoClient.createClient(client8);
////	//daoClient.delete(client8);
////	new DaoSociete(listeSocietes).delete(client8);
////} catch (ExceptionChampObligatoire | ExceptionPersonnalisee | ExceptionDao | SQLException e1) {
////	e1.printStackTrace();
////}
//
////// DAO_PROSPECT : SUPPRESSION Prospect : delete()
////try {
////	Prospect prospect8 = new Prospect("LesValleesFertiles", EnumDomaine.PUBLIC,
////										new Adresse("19", "rue des vallées valonnées", "74100", "Lorient"),
////										"00335215488746","vesta@free.fr", "12-12-2012", EnumInteret.OUI);
////	daoProspect.createProspect(prospect8);
////	//daoProspect.delete(prospect8);
////	//new DaoSociete(listeSocietes).delete(prospect8);
////} catch (ExceptionDao | ExceptionPersonnalisee | ExceptionChampObligatoire | SQLException e1) {
////	e1.printStackTrace();
////}
////
////	// DAO_CLIENT : RECHERCHE Client : find()
////try {
////	 daoClient.find("FleurAuDent");
//////	if (clt1 != null) {
//////		System.out.println(clt1.getRaisonSociale());				
//////	}else{
//////		System.out.println(clt1);
//////	}
////} catch (ExceptionPersonnalisee | ExceptionChampObligatoire | ExceptionDao e1) {
////	System.out.println(e1.getMessage());
////}
//
//
////// DAO_PROSPECT : RECHERCHE Prospect : find()
////try {
////	Prospect clt1 = daoProspect.find("LaGrandeMarreePlus");
////	if (clt1 != null) {
////		 System.out.println(clt1);				
////	}else{
////		System.out.println(clt1);
////	}
////} catch (ExceptionPersonnalisee | ExceptionChampObligatoire | ExceptionDao e1) {
////	System.out.println(e1.getMessage());        
////}	
//
////// DAO_PROSPECT : RECHERCHE DE TOUS les Prospects : findAll()
////try {
////	ArrayList<Prospect> listProspect = daoProspect.findAll();
////	for (Prospect client : listProspects) {
////		System.out.println(client);
////	}
////} catch (ExceptionDao | ExceptionPersonnalisee | ExceptionChampObligatoire | SQLException e1) {
////	System.out.println(e1.getMessage());
////}
//
////// DaoClient save2
////try {
////	client2.setChiffreAfaire(111107.2);
////	daoClient.save2(client2);
////	//daoSociete.save2(client2);
////	
////} catch (ExceptionPersonnalisee | ExceptionDao | SQLException e1) {
////	e1.printStackTrace();
////}
////
////try {
////	Client client10 = new Client("LesBeauxJours", EnumDomaine.PRIVE,
////			new Adresse("10", "rue des papillons", "57120", "Metz"), 
////			"00332545658745", "eole.la@free.fr", 1112201.3, 1247);
////	//daoClient.save2(client7);
////	daoSociete.save2(client10);
////} catch (ExceptionChampObligatoire | ExceptionPersonnalisee | ExceptionDao | SQLException e1) {
////	e1.printStackTrace();
////}
//       
////// DaoProspect save2
////try {
////	prospect2.setDateProspection("25-05-1985");
////	daoProspect.save2(prospect2);
////	//daoSociete.save2(prospect2);
////} catch (ExceptionPersonnalisee | ExceptionDao | SQLException e1) {
////	e1.printStackTrace();
////}
////
////try {
////	Prospect prospect7 = new Prospect("LesNouvellesVagues", EnumDomaine.PUBLIC,
////			new Adresse("17", "rue du phare", "87100", "Limoges"),"00335421569854",
////			"hygie@free.fr", "23-07-2014", EnumInteret.OUI);
////	daoProspect.save2(prospect7);
////	//daoSociete.save2(prospect7);
////} catch (ExceptionChampObligatoire | ExceptionPersonnalisee | ExceptionDao | SQLException e1) {
////	e1.printStackTrace();
////}
//
//
//
//// INSTANCIATION DE LA JFRAME D'ACCUEIL
//	for (Societe societe :  listeSocietes.getListSocietes()) {
//	System.out.println(societe.getDomaine());
//}  

