package fr.leo.metier;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.time.Month;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import fr.leo.exceptions.ExceptionPersonnalisee;

public class ContratTest {
	
    @Test
    @DisplayName("test de exception levee sur valeur contrat null")        
    	void valuesNullSet() {
        assertThrows(ExceptionPersonnalisee.class, () -> { new Contrat().setDateDebutContrat(null); }); 
    }
        
    @Test  
    @DisplayName("test de non exception levee, sur valeur contrat correcte")  
 	void testNonExceptionFormatDateCorrecte() {
       assertDoesNotThrow(
          	() -> { Contrat c1 = new Contrat();
          			c1.setDateDebutContrat(LocalDate.of(2015, Month.JANUARY, 14));
          	});
    }   
	@Test  
    @DisplayName("test format date")  
	void testFormatDate() throws ExceptionPersonnalisee {
	  	Contrat c1 = new Contrat();
	  	c1.setDateDebutContrat(LocalDate.of(2015, Month.JANUARY, 14));
	  	c1.getDateDebutContrat();	
		assertEquals(c1.getDateDebutContrat(), "14-01-2015");
	}
}


