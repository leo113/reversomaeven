package fr.leo.metier;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import fr.leo.utilitaire.Utilitaire;

public class SocieteTest {
	
	/**
	 * Test la correction de l'adresse email
	 * @param mail
	 */
	@ParameterizedTest
	@ValueSource(strings = { "sylvie.ici@afpa.com", "sylvieici@afpa.com" })
	void testPatternMAil(String mail) {
		assertTrue(Utilitaire.PATTERN_MAIL.matcher(mail).matches());
	}
	
	/**
	 * Test Non Parametre la correction de l'adresse email avec une valeur null ou vide
	 * Valeurs : null ou emptyString
	 * @param mail
	 */
	@Test
	void testValueEmptyOrNullMAil() {
		assertThrows(fr.leo.exceptions.ExceptionChampObligatoire.class, () -> {
			new Client().setEmail(null);
			new Client().setEmail("");
		});
	}

	/**
	 * Test Parametre de la correction de l'adresse email avec une valeur null ou vide
	 * Valeurs : null ou emptyString
	 * @param mail
	 */
	@Test
	@NullSource
	@EmptySource
	void testValueEmptyOrNullMAil2() {
		assertThrows(fr.leo.exceptions.ExceptionChampObligatoire.class, () -> {
			new Client().setEmail("");
		});
	}
}
