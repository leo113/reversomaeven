-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 13 mai 2020 à 17:55
-- Version du serveur :  10.1.32-MariaDB
-- Version de PHP :  7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `reverso2`
--

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

CREATE TABLE `adresse` (
  `id_adresse` int(11) NOT NULL,
  `nom_rue` varchar(255) NOT NULL,
  `num_rue` varchar(14) DEFAULT NULL,
  `code_postal` varchar(255) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `fk_societe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `adresse`
--

INSERT INTO `adresse` (`id_adresse`, `nom_rue`, `num_rue`, `code_postal`, `ville`, `fk_societe`) VALUES
(1, 'impasse de la ville', '15', '25417', ' Avignon', 1),
(2, 'rue des mouettes', '16', '37400', ' Amboise', 2),
(3, 'rue du phare', '17', '87100', ' Limoges', 3),
(4, 'rue des champs de ble', '18', '17200', ' Royan', 4),
(5, 'rue du mais transgenique', '19', '74100', ' Chamonix', 5),
(6, 'rue des jardins', '10', '57120', ' Metz', 6),
(7, 'rue des alouettes', '11', '75002', ' Paris', 7),
(8, 'rue des prairies en fleur', '12', '29800', ' Landernau', 8),
(9, 'rue des vertes collines', '13', '59000', ' Lille', 9),
(10, 'rue des coquelicots', '14', '64200', ' Biarritz', 10);

-- --------------------------------------------------------

--
-- Structure de la table `contrat`
--

CREATE TABLE `contrat` (
  `id_contrat` int(11) NOT NULL,
  `libelle_contrat` varchar(255) NOT NULL,
  `montant_contrat` double DEFAULT NULL,
  `date_debut_contrat` date DEFAULT NULL,
  `date_fin_contrat` date DEFAULT NULL,
  `fk_societe` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `contrat`
--

INSERT INTO `contrat` (`id_contrat`, `libelle_contrat`, `montant_contrat`, `date_debut_contrat`, `date_fin_contrat`, `fk_societe`) VALUES
(1, 'Contrat1', 100.2, '2015-01-14', '2019-01-14', 6),
(2, 'Contrat2', 200.2, '2015-02-14', '2019-02-14', 7),
(3, 'Contrat3', 300.2, '2015-03-14', '2019-03-14', 7),
(4, 'Contrat4', 400.2, '2015-04-14', '2019-04-14', 7),
(5, 'Contrat5', 500.2, '2015-05-14', '2019-05-14', 7),
(6, 'Contrat6', 100.2, '2015-01-14', '2019-01-14', 7),
(7, 'Contrat7', 200.2, '2015-02-14', '2019-02-14', 8),
(8, 'Contrat8', 300.2, '2015-03-14', '2019-03-14', 8),
(9, 'Contrat9', 400.2, '2015-04-14', '2019-04-14', 8),
(10, 'Contrat10', 500.2, '2015-05-14', '2019-05-14', 8),
(11, 'Contrat11', 100.2, '2015-01-14', '2019-01-14', 9),
(12, 'Contrat12', 200.2, '2015-02-14', '2019-02-14', 9),
(13, 'Contrat13', 300.2, '2015-03-14', '2019-03-14', 9),
(14, 'Contrat14', 400.2, '2015-04-14', '2019-04-14', 9),
(15, 'Contrat15', 500.2, '2015-05-14', '2019-05-14', 10),
(16, 'Contrat16', 100.2, '2015-01-14', '2019-01-14', 10),
(17, 'Contrat17', 200.2, '2015-02-14', '2019-02-14', 10),
(18, 'Contrat18', 300.2, '2015-03-14', '2019-03-14', 10),
(19, 'Contrat19', 400.2, '2015-04-14', '2019-04-14', 10),
(20, 'Contrat20', 500.2, '2015-05-14', '2019-05-14', 10),
(21, 'Contrat21', 100.2, '2015-01-14', '2019-01-14', 10),
(22, 'Contrat22', 200.2, '2015-02-14', '2019-02-14', 10),
(23, 'Contrat23', 300.2, '2015-03-14', '2019-03-14', 10),
(24, 'Contrat24', 400.2, '2015-04-14', '2019-04-14', 10),
(25, 'Contrat25', 500.2, '2015-05-14', '2019-05-14', 10);

-- --------------------------------------------------------

--
-- Structure de la table `societe`
--

CREATE TABLE `societe` (
  `id_societe` int(11) NOT NULL,
  `raison_sociale` varchar(255) NOT NULL,
  `domaine` enum('prive','public') NOT NULL,
  `telephone` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `commentaires` text,
  `chiffre_affaire` double DEFAULT NULL,
  `nombre_employes` int(11) DEFAULT NULL,
  `date_prospection` date DEFAULT NULL,
  `interet` enum('oui','non') DEFAULT NULL,
  `type_societe` enum('prospect','client') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `societe`
--

INSERT INTO `societe` (`id_societe`, `raison_sociale`, `domaine`, `telephone`, `email`, `commentaires`, `chiffre_affaire`, `nombre_employes`, `date_prospection`, `interet`, `type_societe`) VALUES
(1, 'Demeter', 'prive', '0332154784523', 'demeter.ese@free.fr', '', NULL, NULL, '2000-12-12', 'non', 'prospect'),
(2, 'Themis', 'public', '00333255145587', 'themis@free.fr', '', NULL, NULL, '2001-11-14', 'oui', 'prospect'),
(3, 'Hygie', 'public', '00335421569854', 'hygie@free.fr', '', NULL, NULL, '2014-07-23', 'oui', 'prospect'),
(4, 'Boree', 'prive', '00335422126985', 'boree@free.fr', '', NULL, NULL, '2010-01-30', 'non', 'prospect'),
(5, 'Vesta', 'public', '00335215488746', 'vesta@free.fr', '', NULL, NULL, '2012-12-12', 'oui', 'prospect'),
(6, 'FleurAuDent', 'prive', '00331254236587', 'bartabac@free.fr', '', 12145121, 100, NULL, NULL, 'client'),
(7, 'Eoles', 'public', '00332545658745', 'eole.la@free.fr', '', 1112201.3, 1247, NULL, NULL, 'client'),
(8, 'Oceanos', 'prive', '00333625148974', 'oceanos.lia@free.fr', '', 11110, 1110, NULL, NULL, 'client'),
(9, 'Maia', 'prive', '00335554736524', 'maia.lea@free.fr', '', 11241110, 14, NULL, NULL, 'client'),
(10, 'Chloris', 'public', '00333521654782', 'chloris.li@free.it', '', 11110, 1110, NULL, NULL, 'client');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD PRIMARY KEY (`id_adresse`),
  ADD KEY `fk_societe` (`fk_societe`);

--
-- Index pour la table `contrat`
--
ALTER TABLE `contrat`
  ADD PRIMARY KEY (`id_contrat`),
  ADD KEY `fk_societe2` (`fk_societe`);

--
-- Index pour la table `societe`
--
ALTER TABLE `societe`
  ADD PRIMARY KEY (`id_societe`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `adresse`
--
ALTER TABLE `adresse`
  MODIFY `id_adresse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `contrat`
--
ALTER TABLE `contrat`
  MODIFY `id_contrat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `societe`
--
ALTER TABLE `societe`
  MODIFY `id_societe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD CONSTRAINT `fk_societe` FOREIGN KEY (`fk_societe`) REFERENCES `societe` (`id_societe`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `contrat`
--
ALTER TABLE `contrat`
  ADD CONSTRAINT `fk_societe2` FOREIGN KEY (`fk_societe`) REFERENCES `societe` (`id_societe`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
